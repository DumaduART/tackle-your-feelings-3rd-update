﻿using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class ToolEditor
{
	[MenuItem("ClearSavedData/ClearSavedData")]
	static void ShowWindow()
	{
		PlayerPrefs.DeleteAll();
		Debug.Log("All Player Prefs Data Deleted");
	}
}
