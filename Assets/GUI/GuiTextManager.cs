using UnityEngine;
using System.Collections;

public class GuiTextManager : MonoBehaviour {

	// Use this for initialization
    public static GuiTextManager instance;
	
	public enum ScreenType
	{
		none, TimeAttackGamePlay,ArcadeGamePlay,TimeAttackGameOver, SkillShotGameOver, SkillShotScreen,SkillShotGamePlay,practiceGameplay,
		ShopScreen,suddenDeathGamePlay,suddenDeathGameOver,ProgressBar,GetMoreTime,GetMoreLife,statsDescription,pause,coinPurchase,maimMenu
	};
	
	public ScreenType _screenType;
	public GUIText[] _guiText;
	public GUITexture[] _uitexture;
	
	BallHandler _ballHandler;
	ScoreManager _scoremanager;
	GameManager _gameManager;
	
	void Awake()
	{
		instance =this;
	}
	
	void Start () {
		
	 _ballHandler = GameObject.Find ("GameManager").GetComponent<BallHandler>();
	 _scoremanager = GameObject.Find ("GameManager").GetComponent<ScoreManager>();
	 _gameManager  = GameObject.Find ("GameManager").GetComponent<GameManager>();
		UpdateText();
	}
	
	// Update is called once per frame
	void Update () {
	 
		switch(_screenType)
		{
			case ScreenType.TimeAttackGamePlay:
			{
				TimeAttack timeAttack = TimeAttack.instance;
				_guiText[1].text = TimeAttack.GetTimeLeft().ToString();
				break;
			}
			

			case ScreenType.SkillShotGamePlay:
			{
				if(LevelHandler.instance != null)
			    {
				    LevelHandler levelHandler = LevelHandler.instance;
					if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						_guiText[1].text = levelHandler.GetTimeLeft().ToString(); 
					}
				}
				break;
			}
		}
		
	}
	
	public void UpdateText()
	{
		switch(_screenType)
		{
			
			case ScreenType.practiceGameplay:
			{
				_guiText[0].text = Practice.instance.GetDistance().ToString() + " " + "mts";
				_guiText[1].text = Mathf.Abs( Practice.instance.GetPracticeWind()).ToString()+ " " + "mph";
				break;
			}
			
			case ScreenType.TimeAttackGamePlay:
			{
				 int i,j=3;
				 
				 TimeAttack timeAttack = TimeAttack.instance;
				PowerUpsManager powerUp = PowerUpsManager.instance;
				_guiText[1].text = TimeAttack.GetTimeLeft().ToString();
				_guiText[0].text = timeAttack.GetScore().ToString();
				_guiText[2].text = Mathf.Abs(timeAttack.GetWind()).ToString();
//				_guiText[4].text =
			    _guiText[3].text = _ballHandler.GetDistOfBall().ToString();
				_guiText[4].text =  timeAttack.GetMultiplier().ToString();
				break;
			}
			
			case ScreenType.SkillShotGamePlay:
			{
			
			 
				 int i,j=3;
				if(LevelHandler.instance != null)
			    {
				    LevelHandler levelHandler = LevelHandler.instance;
				    PowerUpsManager powerUp = PowerUpsManager.instance;
					_guiText[0].text = levelHandler.GetSuccessFulShot().ToString();
					if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
					{
						_guiText[1].text = (levelHandler._noOfBall).ToString();
					}
					if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						_guiText[1].text = levelHandler.GetTimeLeft().ToString(); 
					}
				   _guiText[2].text = Mathf.Abs( levelHandler.GetWind()).ToString();
				   _guiText[3].text = (115-levelHandler._ballPos.z).ToString();
				    
				}
				break;
			}
			
			case ScreenType.SkillShotScreen:
			{
			    if(LevelHandler.instance != null)
			    {
				    LevelHandler levelHandler = LevelHandler.instance;
					_guiText[0].text = ((_gameManager.GetIndex()+1).ToString());
					switch(_gameManager.GetCategory())
					{
						case GameManager.Category.HitTheBar:
						{
							if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
							{
								_guiText[1].text = "Flick ball to hit the gaolpost"+"\n"+" ball is limited ";
							}
							else
							{
								_guiText[1].text = "Flick ball to hit the gaolpost"+"\n"+" time is limited";
							}
							break;
						}
						
						case GameManager.Category.ThroughRings:
						{
							if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
							{
								_guiText[1].text = "Flick ball to pass through rings"+"\n"+" ball is limited ";
							}
							else
							{
								_guiText[1].text = "Flick ball to pass through rings"+"\n"+" time is limited";
							}
							break;
						}
					
						case GameManager.Category.NoSwingStorm:
						{
					
							if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
							{
								_guiText[1].text = "Flick ball without swing"+"\n"+" ball is limited ";
							}
							else
							{
								_guiText[1].text = "Flick ball without swing"+"\n"+" time is limited";
							}
							break;
						}
						
					}
					
					if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
					{
						_guiText[2].text = "BALLS AVAILABLE";
						_guiText[3].text = levelHandler._noOfBall.ToString();
					}
					if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						_guiText[2].text = "TIMES AVAILABLE" ;
						_guiText[3].text = levelHandler._totalTime.ToString(); 
					}
					
//				   _guiText[6].text = "FOR" +" " +levelHandler._shotreqforBronze.ToString()+ " " + "SHOTS";
//				   _guiText[5].text = "FOR" +" " + levelHandler._shotreqforSilver.ToString()+ " " + "SHOTS";
//				   _guiText[4].text = "FOR" +" " + levelHandler._shotreqforGold.ToString()+ " " + "SHOTS";
				    
				}
			 break;
			}
			
			case ScreenType.TimeAttackGameOver:
			{
				TimeAttack timeAttack = TimeAttack.instance;
				_guiText[0].text = ScoreManager.instance.noOfgoal.ToString();
				_guiText[1].text = TimeAttack.instance.goalStreak.ToString();
				_guiText[2].text = (timeAttack.GetMultiplier()-1).ToString();
				_guiText[3].text = TimeAttack.noOfRoundUp.ToString();
				_guiText[4].text = _scoremanager.CurrentTokens().ToString();
			    _guiText[5].text = _scoremanager.GetCurrentXp().ToString();
				_guiText[6].text = ScoreManager.instance.miScoreOfOneShot.ToString();
			    _guiText[7].text = _scoremanager.GetXpScore().ToString();
				_guiText[8].text = _gameManager.GetCoins().ToString();
				_guiText[9].text = _gameManager.GetLevelNo().ToString();
				if(_gameManager._coinUpgrade.currentUpgrade>1)
				{
					_guiText[10].text = "(+"+_gameManager._coinUpgrade.totalUpgrade[_gameManager._coinUpgrade.currentUpgrade-1].UpgradePercentage.ToString()+"%)";
				}
				
				else
				{
					_guiText[10].text = null;
				}
			
				if(_gameManager._xpUpgrade.currentUpgrade>1)
				{
					_guiText[11].text = "(+"+_gameManager._xpUpgrade.totalUpgrade[_gameManager._xpUpgrade.currentUpgrade-1].UpgradePercentage.ToString()+"%)";
				}
				
				else
				{
					_guiText[11].text = null;
				}
			
				_guiText[12].text = (_gameManager.GetLevelNo()*_gameManager.levelUpCuttoff-_scoremanager.GetXpScore()).ToString();
				break;
			}
			
			case ScreenType.SkillShotGameOver:
			{
				LevelHandler levelHandler = LevelHandler.instance;
				_guiText[0].text = levelHandler.GetSuccessFulShot().ToString();
				_guiText[1].text = levelHandler.GetTokens().ToString();
				_guiText[2].text = _scoremanager.GetXpScore().ToString();
				_guiText[3].text = _gameManager.GetLevelNo().ToString();
				_guiText[5].text = _gameManager.GetCoins().ToString();
				_guiText[6].text = (_gameManager.GetLevelNo()*_gameManager.levelUpCuttoff-_scoremanager.GetXpScore()).ToString();
				break;
			}
			
		case ScreenType.ShopScreen:
		{
			
			_guiText[0].text = _gameManager.GetCoins().ToString();
			break;
		}
			
		case ScreenType.suddenDeathGamePlay:
		{
			SuddenDeath suddenDeath = SuddenDeath.instance;
			_guiText[0].text = suddenDeath.GetScore().ToString();
			_guiText[1].text = suddenDeath.GetBall().ToString();
			_guiText[2].text = Mathf.Abs( suddenDeath.GetWind()).ToString();
			_guiText[3].text = suddenDeath.GetDist().ToString();
			_guiText[4].text = suddenDeath.GetMultiPlier().ToString();
			
			break;
		}
			
		case ScreenType.suddenDeathGameOver:
		{
			SuddenDeath suddenDeath = SuddenDeath.instance;
			_guiText[0].text = suddenDeath.GetNoOfGoal().ToString();
			_guiText[1].text = suddenDeath.GetGoalStreak().ToString();
			_guiText[2].text = (suddenDeath.GetMultiPlier()-1).ToString();
			_guiText[3].text = suddenDeath.GetContinuousGoal().ToString();
			_guiText[4].text = _scoremanager.CurrentTokens().ToString();
			_guiText[5].text = _scoremanager.GetCurrentXp().ToString();
			_guiText[6].text = _scoremanager.CurrentScore().ToString();
			_guiText[7].text =  _scoremanager.GetXpScore().ToString();
		    _guiText[8].text = _gameManager.GetCoins().ToString();
			_guiText[9].text = _gameManager.GetLevelNo().ToString();
			if(_gameManager._coinUpgrade.currentUpgrade>1)
				{
					_guiText[10].text = "(+"+_gameManager._coinUpgrade.totalUpgrade[_gameManager._coinUpgrade.currentUpgrade-1].UpgradePercentage.ToString()+"%)";
				}
				
				else
				{
					_guiText[10].text = null;
				}
			
				if(_gameManager._xpUpgrade.currentUpgrade>1)
				{
					_guiText[11].text = "(+"+_gameManager._xpUpgrade.totalUpgrade[_gameManager._xpUpgrade.currentUpgrade-1].UpgradePercentage.ToString()+"%)";
				}
				
				else
				{
					_guiText[11].text = null;
				}
				_guiText[12].text = (_gameManager.GetLevelNo()*_gameManager.levelUpCuttoff-_scoremanager.GetXpScore()).ToString();
			
			break;
		}
			
		case ScreenType.ProgressBar:
		{
			_guiText[0].text = _scoremanager.GetXpScore().ToString();
			_guiText[1].text = _gameManager.GetLevelNo().ToString();
			break;
		}
		
		case ScreenType.GetMoreTime:
		{
			_guiText[0].text = "+"+_gameManager._moreTime[0]._extraTime.ToString();
			_guiText[1].text = "+"+_gameManager._moreTime[1]._extraTime.ToString();
			_guiText[2].text = _gameManager._moreTime[0]._extraTimeCost.ToString();
			_guiText[3].text = _gameManager._moreTime[1]._extraTimeCost.ToString();
			_guiText[4].text = _gameManager.GetCoins().ToString();
			_guiText[5].text = _scoremanager.CurrentScore().ToString();
			break;
		}
		
		case ScreenType.GetMoreLife:
		{
			_guiText[0].text = "+"+_gameManager._moreLife[0]._noOfLife.ToString();
			_guiText[1].text = "+"+_gameManager._moreLife[1]._noOfLife.ToString();
			_guiText[2].text = _gameManager._moreLife[0]._lifeCost.ToString();
			_guiText[3].text = _gameManager._moreLife[1]._lifeCost.ToString();
			_guiText[4].text = _gameManager.GetCoins().ToString();
			_guiText[5].text = _scoremanager.CurrentScore().ToString();
			break;
		}
		
		case ScreenType.statsDescription:
		{
			 StatsData _statsdata = _gameManager.GetStatsData();
			/// trainingData data
			if(_guiText[0] == null)
				return ;
			_guiText[0].text = _statsdata._trainingData._totalTimeplayed.ToString();
			/// clock showDown data
			_guiText[1].text = _statsdata._clockShowDownData._highestScore.ToString();
			_guiText[2].text = _statsdata._clockShowDownData._totalNoofTimesPlayed.ToString();
			_guiText[3].text = _statsdata._clockShowDownData._highestMultiplier.ToString();
			_guiText[4].text = _statsdata._clockShowDownData._longestSession.ToString();
			_guiText[5].text = _statsdata._clockShowDownData._highestRoundsCleared.ToString();
			// Skill Shot Data 
			_guiText[6].text = _statsdata._killShotData._totalAttempts.ToString();
			_guiText[7].text = _statsdata._killShotData._totalChallengedCleared.ToString();
			_guiText[8].text = _statsdata._killShotData._totalBronze.ToString();
			_guiText[9].text = _statsdata._killShotData._totalSilver.ToString();
			_guiText[10].text = _statsdata._killShotData._totalGold.ToString();
			// suddendeath data
			_guiText[11].text = _statsdata._suddenDeathData._highestNoGoal.ToString();
			_guiText[12].text = _statsdata._suddenDeathData._totalNoofTimesPlayed.ToString();
			_guiText[13].text = _statsdata._suddenDeathData._highestMultiplier.ToString();
			// overAll data
			_guiText[14].text = _scoremanager.GetXpScore().ToString();
			_guiText[15].text = _gameManager.GetCoins().ToString();
			int totalSec = _statsdata._overAllData._durationOfTimePlayed;
			
			_guiText[16].text = (int)(totalSec / 3600) + " Hr : "+(int)( (totalSec % 3600) / 60) + " Min : "+(int)((totalSec % 3600)%60) + " Sec";
			break;
		}
		
		case ScreenType.pause:
		{
			if(_gameManager.GetGameMode()==GameManager.GameMode.Training)
			{
				_guiText[0].text = "TRAINING MODE";
			}
			
			else if(_gameManager.GetGameMode()==GameManager.GameMode.ClockShowDown)
			{
				_guiText[0].text = "TIME CHALLENGE MODE";
			}
			
			else if(_gameManager.GetGameMode()==GameManager.GameMode.SuddenDeath)
			{
				_guiText[0].text = "SUDDEN DEATH MODE";
			}
			
			else if(_gameManager.GetGameMode()==GameManager.GameMode.SkillShots)
			{
				if(_gameManager.GetCategory() == GameManager.Category.HitTheBar)
				{
					_guiText[0].text = "HIT THE BAR";
				}
				
				else if(_gameManager.GetCategory() == GameManager.Category.ThroughRings)
				{
					_guiText[0].text = "THROUGH THE RINGS";
				}
				
				else if(_gameManager.GetCategory() == GameManager.Category.NoSwingStorm)
				{
					_guiText[0].text = "NO SWING IN STORM";
				}
			}
			break;
		}
			
		case ScreenType.coinPurchase:
		{
			_guiText[0].text = _gameManager.GetCoins().ToString();
			break;
		}
			
		case ScreenType.maimMenu:
		{	
			if(_guiText[0]==null)
				return ;
			_guiText[0].text = _gameManager.GetLevelNo().ToString();
			_guiText[1].text = _gameManager.GetCoins().ToString();
			_guiText[2].text = _scoremanager.GetXpScore().ToString();
			break;
		}
			
			
		}
		
	}
}
