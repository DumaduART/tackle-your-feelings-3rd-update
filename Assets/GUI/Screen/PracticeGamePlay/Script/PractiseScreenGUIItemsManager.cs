using UnityEngine;
using System.Collections;

public class PractiseScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	
	public GameObject distanceSlider;
	public GameObject windSlider;
	
	bool tutorial_level = false;
	
	
	bool _isAndroidBack = false;
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		distanceSlider.GetComponent<MenuItemEntryAnim>()._EndPos = Practice.instance.distBtnPos;
		windSlider.GetComponent<MenuItemEntryAnim>()._EndPos = Practice.instance.windBtnPos;
	}
	
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				_screenManager.LoadScreen("Pause");
				EventManager.GamePauseTrigger();
				Practice.instance.SetPauseCondition(false);
				_isAndroidBack = false;
			}
		}
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "RandomiseBtn")
			{
				
				Practice.instance.ChangePosInZone();	
			}
			
			if(item.name == "left")
			{
				Practice.instance.ChangeWindType(Practice.WindType.left);
			}
			
			if(item.name == "right")
			{
				Practice.instance.ChangeWindType(Practice.WindType.right);
			}
			
			if(item.name == "Pause")
			{
				_screenManager.LoadScreen("Pause");
				EventManager.GamePauseTrigger();
				Practice.instance.SetPauseCondition(false);
			}
			
			if(item.name == "Store")
			{
				print (item.name);
			}
		}
		
		
		
	}	
}