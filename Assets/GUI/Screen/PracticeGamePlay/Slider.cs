using UnityEngine;
using System.Collections;

public class Slider : MonoBehaviour {

	// Use this for initialization
	private Rect Default ; 
	private bool toucActive;
	public GUITexture upertexture;
	public GUITexture lowerTexture;
	public int distance;
	public static Slider instance;
	public bool iswind;
	public bool isdistance;
	public Vector3 pos;
	InputManager inputManager;
	void Start () {
	    instance = this;
		Default = transform.GetComponent<GUITexture>().pixelInset;
		pos = transform.position;
		inputManager = GameObject.Find("GameManager").GetComponent<InputManager>();
//		if(isdistance)
//		{
//			transform.position = Practice.instance.distBtnPos;
//		}
//		
//		if(iswind)
//		{
//			transform.position = Practice.instance.windBtnPos;
//		}
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetMouseButtonDown(0))
		{
			if (GetComponent<GUITexture>().HitTest(Input.mousePosition, Camera.main))
			{
				print ("test");
				toucActive = true;
				inputManager.SetRotationCondition(false);
			}
			
		}
		if(Input.GetMouseButtonUp(0))
		{
			toucActive = false;
			inputManager.SetRotationCondition(true);
		}
		
		if(toucActive && Input.GetMouseButton(0))
		{
			if(pos.y>=lowerTexture.transform.position.y && pos.y<=upertexture.transform.position.y)
			{
				pos.y = Input.mousePosition.y/Screen.height;
				if(pos.y >= upertexture.transform.position.y)
					pos.y = upertexture.transform.position.y;
				if(pos.y <= lowerTexture.transform.position.y)
					pos.y = lowerTexture.transform.position.y;
				transform.position = pos;
				if(isdistance)
					distance = (int) ((pos.y-lowerTexture.transform.position.y)/(upertexture.transform.position.y-lowerTexture.transform.position.y)*25);
				if(iswind)
					distance = (int) ((pos.y-lowerTexture.transform.position.y)/(upertexture.transform.position.y-lowerTexture.transform.position.y)*10);
				transform.GetComponent<MenuItemExitAnim>()._EndPos = pos;
				if(isdistance)
				{
					if(Practice.instance!=null)
					{
						Practice.instance.Setdistance(distance);
						Practice.instance.distBtnPos = transform.position;
					}
					GuiTextManager.instance.UpdateText();
					
				}
				if(iswind)
				{
					if(Practice.instance!=null)
					{
						Practice.instance.setWindEffect(distance);
						Practice.instance.windBtnPos = transform.position;
					}
					GuiTextManager.instance.UpdateText();
				}
				//print (distance);
			}
		}
			
		 
	
	}
}
