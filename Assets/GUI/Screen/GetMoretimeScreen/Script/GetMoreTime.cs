﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]

public class GetMoreTime{
	
	public string name;
	public int _extraTime;
	public int _extraTimeCost;
}
