using UnityEngine;
using System.Collections;

public class Rotatingring : MonoBehaviour {

	// Use this for initialization
      float initaltime;
	 public float waitingTime;
	public Vector3 _rotateVector = new Vector3(0,0,1);
	void Start () 
	{
		
		CScaleTo scale1 = gameObject.AddComponent<CScaleTo>();
		scale1.actionWith(gameObject,new Vector2(1.0f,1.0f),0.5f);
			
		CScaleTo scale2 = gameObject.AddComponent<CScaleTo>();
		scale2.actionWith(gameObject,new Vector2(0.8f,0.8f),0.5f);
		
		CSequence seq = gameObject.AddComponent<CSequence>();
		seq.actionWithActions(scale1,scale2);
	
		CRepeat rep = gameObject.AddComponent<CRepeat>();
		rep.actionWithAction(seq,-1);
		rep.runAction();
		
		initaltime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		
//		if((Time.time-initaltime) > waitingTime)
//			Destroy(this.gameObject);
		
		if((Time.time-initaltime) <= waitingTime)
	     transform.Rotate(_rotateVector*Time.deltaTime*100);
		else 
		{
			Destroy(this.gameObject);
		}
	}
}
