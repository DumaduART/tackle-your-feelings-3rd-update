using UnityEngine;
using System.Collections;

public class UpgradeShoScroll : ScrollAndPaging
{
	GUIText ShopballText;
	GUIText EnvironmentText;
	GUIText AddupsText;
	//public static ShoScroll instance;
	
	// Use this for initialization
	ScreenManager subscreenitemmanager;	
	ScreenManager screenManager;
	
	GameManager gameManager;
	public GUITexture _coinUpgradeBtn;
	public GUITexture _xpUpgradeBtn;
	public GUITexture _timeUpgradeBtn;
	public GUITexture _lifeUpgradeBtn;
	
	public GUIText _coinUpgradDetail;
	public GUIText _xpUpgradeDetails;
	public GUIText _timeUpgradeDetails;
	public GUIText _lifeUpgradeDetails;
	
	public GUIText _coinUpgradeCost;
	public GUIText _xpUpgradeCost;
	public GUIText _timeUpgradeCost;
	public GUIText _lifeUpgradeCost;
	
	public GUITexture[] _xpEnergy;
	public GUITexture[] _coinEnergy;
	public GUITexture[] _timeEnergy;
	public GUITexture[] _lifeEnergy;
	
	int curcoinUpgrade;
	int curxpUpgrade;
	int curlifeUpgrade;
	int curtimeUpgrade;
			
	void Start () 
	{
		base.InitScroll();		
//		instance = this;
		gameManager = GameManager.GetInstance();
		
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
		screenManager =  GameObject.Find("ScreenManager").GetComponent<ScreenManager>();
		
		 curcoinUpgrade = gameManager._coinUpgrade.currentUpgrade;
		 curxpUpgrade = gameManager._xpUpgrade.currentUpgrade;
		 curlifeUpgrade = gameManager._lifeUpgrade.currentUpgrade;
		 curtimeUpgrade = gameManager._timeUpgrade.currentUpgrade;
		
		for(int i =0;i<gameManager._coinUpgrade.currentUpgrade;i++)
		{
			_coinEnergy[i].gameObject.SetActive(true);
			
		}
		
		for(int i =0;i<gameManager._xpUpgrade.currentUpgrade;i++)
		{
			_xpEnergy[i].gameObject.SetActive(true);
		}
		
		for(int i =0;i<gameManager._timeUpgrade.currentUpgrade;i++)
		{
			_timeEnergy[i].gameObject.SetActive(true);
		}
		
		for(int i =0;i<gameManager._lifeUpgrade.currentUpgrade;i++)
		{
			_lifeEnergy[i].gameObject.SetActive(true);
		}
		if(curcoinUpgrade<gameManager._coinUpgrade.totalUpgrade.Length)
		{
			_coinUpgradeCost.text = "Upgrade For"+ " " + gameManager._coinUpgrade.totalUpgrade[curcoinUpgrade].UpgradeCost.ToString();
			_coinUpgradDetail.text = "Bonus Coins" + "(LV1 +" + " " + gameManager._coinUpgrade.totalUpgrade[curcoinUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
		}
		
		else
		{
			_coinUpgradeCost.text = null;
			_coinUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
			_coinUpgradDetail.text = "Bonus Coins" + "(LV1 +" + " " + gameManager._coinUpgrade.totalUpgrade[curcoinUpgrade-1].UpgradePercentage.ToString() + "%" + " " + "Coin)";
		}
		
		if(curxpUpgrade<gameManager._xpUpgrade.totalUpgrade.Length)
		{
			_xpUpgradeCost.text = "Upgrade For"+ " " + gameManager._xpUpgrade.totalUpgrade[curxpUpgrade].UpgradeCost.ToString();
			_xpUpgradeDetails.text = "XP" + "(LV1 +" + " " + gameManager._xpUpgrade.totalUpgrade[curxpUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
		}
		
		else
		{
			_xpUpgradeCost.text = null;
			_xpUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
			_xpUpgradeDetails.text = "XP" + "(LV1 +" + " " + gameManager._xpUpgrade.totalUpgrade[curxpUpgrade-1].UpgradePercentage.ToString() + "%" + " " + "Coin)";
		}
		
		if(curtimeUpgrade<gameManager._timeUpgrade.totalUpgrade.Length)
		{
			_timeUpgradeCost.text = "Upgrade For"+ " " + gameManager._timeUpgrade.totalUpgrade[curtimeUpgrade].UpgradeCost.ToString();
			_timeUpgradeDetails.text = "TimeDiscount" + "(LV1 +" + " " + gameManager._timeUpgrade.totalUpgrade[curtimeUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
		}
		
		else
		{
			_timeUpgradeCost.text = null;
			_timeUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
			_timeUpgradeDetails.text = "Bonus Coins" + "(LV1 +" + " " + gameManager._timeUpgrade.totalUpgrade[curtimeUpgrade-1].UpgradePercentage.ToString() + "%" + " " + "Coin)";
		}
		
		if(curlifeUpgrade<gameManager._coinUpgrade.totalUpgrade.Length)
		{
			_lifeUpgradeCost.text = "Upgrade For"+ " " + gameManager._lifeUpgrade.totalUpgrade[curlifeUpgrade].UpgradeCost.ToString();
			_lifeUpgradeDetails.text = "Life" + "(LV1 +" + " " + gameManager._lifeUpgrade.totalUpgrade[curlifeUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
			
		}
		
		else
		{
			_lifeUpgradeCost.text = null;
			_lifeUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
			_lifeUpgradeDetails.text = "Life" + "(LV1 +" + " " + gameManager._lifeUpgrade.totalUpgrade[curlifeUpgrade-1].UpgradePercentage.ToString() + "%" + " " + "Coin)";
		}
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{				
		
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			
			if(item.name == "CoinUpgrade")
			{
				if(gameManager.Upgrade(1))
				{
					curcoinUpgrade = gameManager._coinUpgrade.currentUpgrade;
					_coinEnergy[curcoinUpgrade-1].gameObject.SetActive(true);
					if(curcoinUpgrade<gameManager._coinUpgrade.totalUpgrade.Length)
					{
						_coinUpgradeCost.text = "Upgrade For"+ " " + gameManager._coinUpgrade.totalUpgrade[curcoinUpgrade].UpgradeCost.ToString();
						_coinUpgradDetail.text = "Bonus Coin" + "(LV1 +" + " " +
						gameManager._coinUpgrade.totalUpgrade[curcoinUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
					}
					else
					{
						_coinUpgradeCost.text = null;
						_coinUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
						//_coinUpgradeBtn.guiTexture.texture = null;
					}
					gameManager.updateAchievements(16);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			if(item.name == "XPUpgrade")
			{
				if(gameManager.Upgrade(2))
				{
					curxpUpgrade = gameManager._xpUpgrade.currentUpgrade;
					_xpEnergy[curxpUpgrade-1].gameObject.SetActive(true);
					if(curxpUpgrade<gameManager._xpUpgrade.totalUpgrade.Length)
					{
						_xpUpgradeCost.text = "Upgrade For"+ " " + gameManager._xpUpgrade.totalUpgrade[curxpUpgrade].UpgradeCost.ToString();
						_xpUpgradeDetails.text = "XP" + "(LV1 +" + " " +
						gameManager._xpUpgrade.totalUpgrade[curxpUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
					}
					
					else
					{
						_xpUpgradeCost.text = null;
						_xpUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
					}
					
					gameManager.updateAchievements(16);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			if(item.name == "TimeUpgrade")
			{
				if(gameManager.Upgrade(3))
				{
					 curtimeUpgrade = gameManager._timeUpgrade.currentUpgrade;
					_timeEnergy[curtimeUpgrade-1].gameObject.SetActive(true);
					if(curtimeUpgrade<gameManager._timeUpgrade.totalUpgrade.Length)
					{
						_timeUpgradeCost.text = "Upgrade For"+ " " + gameManager._timeUpgrade.totalUpgrade[curtimeUpgrade].UpgradeCost.ToString();
						_timeUpgradeDetails.text = "TimeDiscount" + "(LV1 +" + " " +
						gameManager._timeUpgrade.totalUpgrade[curtimeUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
					}
					
					else
					{
						_timeUpgradeCost.text = null;
						_timeUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
					}
					gameManager.updateAchievements(16);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
				gameManager.UpdateTimeCostInDevice();
			}
			
			if(item.name == "LifeUpgrade")
			{
				if(gameManager.Upgrade(4))
				{
					 curlifeUpgrade = gameManager._lifeUpgrade.currentUpgrade;
					_lifeEnergy[curlifeUpgrade-1].gameObject.SetActive(true);
					if(curlifeUpgrade<gameManager._coinUpgrade.totalUpgrade.Length)
					{
						_lifeUpgradeCost.text = "Upgrade For"+ " " + gameManager._lifeUpgrade.totalUpgrade[curlifeUpgrade].UpgradeCost.ToString();
						_lifeUpgradeDetails.text = "Life" + "(LV1 +" + " " + 
						gameManager._lifeUpgrade.totalUpgrade[curlifeUpgrade].UpgradePercentage.ToString() + "%" + " " + "Coin)";
					}
					
					else
					{
						_lifeUpgradeCost.text = null;
						_lifeUpgradeBtn.GetComponent<GUIItemButton>().setDisabled(true);
					}
					
					 gameManager.updateAchievements(16);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
				
				gameManager.UpdateLifeCostinDevice();
			}
			
			
			
			
			
		}
	}
	
	
	public void CallCoinPurchagePage()
	{
		if(gameManager.isInappNotReq)
		{
			PopUpManager.instance.CreatePopUp(PopUpType.notenoughCoins);
			return ;
		}
		# if UNITY_WEBPLAYER
					
		# else
			screenManager.LoadScreen("CoinPurchase");
			_screenManager.LoadScreen("Empty");
			screenManager.prevPageBeforeCoinPurchage = "ShopScreen";
		#endif
	}
	
}
