using UnityEngine;
using System.Collections;

public class ShopScreenGUIItemsManager : GUIItemsManager
{	
	public static ShopScreenGUIItemsManager instance;
	ScreenManager subscreenitemmanager;	
	public GameObject ShopSubScreenManager,LeftArrow,RightArrow;
	GameObject tempsubscreenmanager;
	public GUIText selectBallText;
	public GUIText selectEnvText;
	public GUIText selectAddupstext;
	BallSelectionManager ballselectionmanager;
	public GUITexture AddCoinBtn;
	//BallHandler ballhandler;
	public ScrollAndPaging _ScrollAndPaging;
	bool _isAndroidBack = false;
 	public GameObject[] _PositionsChangeForCustom;

	void Awake()
	{
		instance = this;
	}
	
	void Start ()
	{
		ballselectionmanager = BallSelectionManager.GetInstance();
		//ballhandler = GameObject.Find("GameManager").GetComponent<BallHandler>();
		base.Init();
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
		subscreenitemmanager.LoadScreen(subscreenitemmanager.defaultScreen);
		switch(subscreenitemmanager.defaultScreen)
		{
			case "BallSelectionScreen":
			{
				selectBallText.GetComponent<GUIText>().color = Color.yellow;
				break;
			}
			
			case "EnvionmentSelectionScreen":
			{
				selectEnvText.GetComponent<GUIText>().color = Color.yellow;
				break;
			}
			
			case "AddupsSelectionScreen":
			{
				selectAddupstext.GetComponent<GUIText>().color = Color.yellow;
				break;
			}
		}
			//if (GameManager.GetInstance ().isInappNotReq) {

		
//		GuiTextManager.instance.UpdateText();
	}	
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				_screenManager.LoadScreen(_screenManager.userdata);
				subscreenitemmanager.LoadScreen("Empty");
				_isAndroidBack = false;
			}
		}
	}
	
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;		
	}	
	
	public override void OnSelectedEvent(GUIItem item)
	{		
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			
			if(item.name == "BackBG" )
			{
					_screenManager.LoadScreen(_screenManager.userdata);
				    subscreenitemmanager.LoadScreen("Empty"); //Empty
			}
			
			if(item.name == "SelectBall" )
			{
				subscreenitemmanager.LoadScreen("BallSelectionScreen");
				selectBallText.GetComponent<GUIText>().color = Color.yellow;
				selectAddupstext.GetComponent<GUIText>().color = Color.white;
				selectEnvText.GetComponent<GUIText>().color = Color.white;
				subscreenitemmanager.defaultScreen = "BallSelectionScreen";
				ButtonList.instance.SetSelectedState(item.name,true);
			}
			
			if(item.name == "SelectUpgrade" )
			{
				subscreenitemmanager.LoadScreen("AddupsSelectionScreen");
				selectAddupstext.GetComponent<GUIText>().color = Color.yellow;
				selectBallText.GetComponent<GUIText>().color = Color.white;
				//selectEnvText.guiText.color = Color.white;
				subscreenitemmanager.defaultScreen = "AddupsSelectionScreen";
				ButtonList.instance.SetSelectedState(item.name,true);
			}
			
			if(item.name == "SelectEnvironment" )
			{
				subscreenitemmanager.LoadScreen("EnvionmentSelectionScreen");
				selectEnvText.GetComponent<GUIText>().color = Color.yellow;
				selectAddupstext.GetComponent<GUIText>().color = Color.white;
				selectBallText.GetComponent<GUIText>().color = Color.white;
				subscreenitemmanager.defaultScreen = "EnvionmentSelectionScreen";
			}
			
			if (item.name == "AddCoin")
			{
				# if UNITY_WEBPLAYER
					
				# else
				_screenManager.LoadScreen("CoinPurchase");
				subscreenitemmanager.LoadScreen("Empty");
				_screenManager.prevPageBeforeCoinPurchage = "ShopScreen";
				#endif
			}
			if(item.name == "LeftArrow")
			{
				LeftArrow.SetActive(false);
				RightArrow.SetActive(false);
				Invoke("Enable",1f);
				if(GameObject.Find("BallSelectionScreen(Clone)")!=null)
				GameObject.Find("BallSelectionScreen(Clone)").GetComponent<BallselectionShoScroll>().movePagePrevious(0.4f);
				if(GameObject.Find("AddupsSelectionScreen(Clone)")!=null)
				GameObject.Find("AddupsSelectionScreen(Clone)").GetComponent<UpgradeShoScroll>().movePagePrevious(0.4f);
				
			}

			if(item.name == "RghtArrow")
			{
				LeftArrow.SetActive(false);
				RightArrow.SetActive(false);
				Invoke("Enable",1f);
				if(GameObject.Find("BallSelectionScreen(Clone)")!=null)
				GameObject.Find("BallSelectionScreen(Clone)").GetComponent<BallselectionShoScroll>().movePageNext(-0.4f);
				if(GameObject.Find("AddupsSelectionScreen(Clone)")!=null)
			GameObject.Find("AddupsSelectionScreen(Clone)").GetComponent<UpgradeShoScroll>().movePageNext(-0.4f);

			}
		}
	}
	void Enable(){
		LeftArrow.SetActive(true);
		RightArrow.SetActive(true);
	}
}