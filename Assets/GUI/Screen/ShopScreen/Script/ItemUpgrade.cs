﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ItemUpgrade  {
	
	public int currentUpgrade;
	public TotalUpgrade[] totalUpgrade;
}
[System.Serializable]
public class TotalUpgrade
{
	public string ItemUpgradeName;
	public int UpgradePercentage;
	public int UpgradeCost;
	public bool isUpgraded;
}