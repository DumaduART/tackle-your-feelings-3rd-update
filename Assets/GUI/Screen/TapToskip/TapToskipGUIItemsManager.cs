using UnityEngine;
using System.Collections;

public class TapToskipGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	public static TapToskipGUIItemsManager instance;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	PopUpManager _popUpManager;
	bool tutorial_level = false;
	
	bool isActivate = false;
	
	bool _isAndroidBack = false;
	// Use this for initialization
	void Awake()
	{
		instance = this;
			
	}
	 
	void Start ()
	{
		base.Init();	
		_gameManager = GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		_popUpManager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		
		switch(_gameManager.GetGameMode())
		{
			case GameManager.GameMode.ClockShowDown:
			{
				TimeAttackScreenGUIItemsManager.instance._isAndroidBack = false;
				break;
			}
			
			case GameManager.GameMode.SuddenDeath:
			{
				SuddenDeathGUIItemsManager.instance._isAndroidBack = false;
				break;
			}
			
			case GameManager.GameMode.SkillShots:
			{
				if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.ball)
				{
					ArcadeGamepalyGUIItemsManager1.instance._isAndroidBack = false;
				}
			
				else if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.time)
				{
					SkillShotGUIItemsManager.instance._isAndroidBack = false;
				}
				break;
			}
		}
	
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				_popUpManager.HidePopUp();
				if(_inputManager.GetRefOfBall() != null)
				{
					_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().Delay(0);
					GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
				}
				
				_isAndroidBack = false;
			//	SetGamePlayAndroidCondition();
			}
		}	
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public override void OnExitAnimationCompleted()
	{
		SetGamePlayAndroidCondition();
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "TaptoSkipBtn")
			{
				_popUpManager.HidePopUp();
				if(_inputManager.GetRefOfBall() != null)
				{
					_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().Delay(0);
					GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
				}
			}
		
		}
		
	}

	public void TapToSkipButton()
    {
        _popUpManager.HidePopUp();
        if (_inputManager.GetRefOfBall() != null)
        {
            _inputManager.GetRefOfBall().GetComponent<CollisionDetection>().Delay(0);
            GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
        }
    }
	
	public void SetGamePlayAndroidCondition()
	{
		
		switch(_gameManager.GetGameMode())
		{
			case GameManager.GameMode.ClockShowDown:
			{
				TimeAttackScreenGUIItemsManager.instance._isAndroidBack = true;
				break;
			}
			
			case GameManager.GameMode.SuddenDeath:
			{
				SuddenDeathGUIItemsManager.instance._isAndroidBack = true;
				break;
			}
			
			case GameManager.GameMode.SkillShots:
			{
				if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.ball)
				{
					ArcadeGamepalyGUIItemsManager1.instance._isAndroidBack = true;
				}
			
				else if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.time)
				{
					SkillShotGUIItemsManager.instance._isAndroidBack = true;
				}
				break;
			}
		}
	}

}