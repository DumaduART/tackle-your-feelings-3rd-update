using UnityEngine;
using System.Collections;

public class LevelUpScreen : GUIItemsManager
{
	public enum AccuracyType
	{
		good,awesome,great
	};
	public AccuracyType accuracyType;
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	PopUpManager _popUpManager;
	
	bool tutorial_level = false;
	public int reWardValue;
	public GUIText[] _guiText;
	
	GameManager gameManager;
	
	// Use this for initialization
	void Start ()
	{
		_popUpManager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		_popUpManager.AddToList(this.gameObject);
		GameObject.Find("ScreenManager").GetComponent<ScreenManager>().DisableInput();
		base.Init();	
		gameManager = GameManager.GetInstance();
		UpdateText();
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
			if(item.name == "CollectBtn")
			{
				gameManager.AddCoin(reWardValue);
				GuiTextManager.instance.UpdateText();
				GameObject.Find("ScreenManager").GetComponent<ScreenManager>().EnableInput();
				switch(gameManager.GetGameMode())
				{
					case GameManager.GameMode.ClockShowDown:
					{
						TimeAttackOverGUIItemsManager.instance._isAndroidBack = true;
						break;
					}
					
					case GameManager.GameMode.SkillShots:
					{
						SkillShotGameOverGUIItemsManager1.instance._isAndroidBack = true;
						break;
					}
					
					case GameManager.GameMode.SuddenDeath:
					{
						SuddenGameOverGUIItemsManager.instance._isAndroidBack = true;
						break;
					}
				}
				Destroy(this.gameObject);
			}
		
		}
		
		
		
		
	}	
	
	public void UpdateText()
	{
		_guiText[0].text = "LEVEL"+" "+gameManager.GetLevelNo().ToString();
		_guiText[1].text = reWardValue.ToString();
	}
	
}