using UnityEngine;
using System.Collections;

public class roundUpScreen : GUIItemsManager
{
	public enum AccuracyType
	{
		good,awesome,great
	};
	public AccuracyType accuracyType;
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	PopUpManager _popUpManager;
	
	bool tutorial_level = false;
	public int reWardValue;
	public GUIText[] _guiText;
	
	GameManager gameManager;
	InputManager inputmanager;
	// Use this for initialization
	void Start ()
	{
		_popUpManager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		_popUpManager.AddToList(this.gameObject);
		base.Init();	
		gameManager = GameManager.GetInstance();
		inputmanager = GameObject.Find("GameManager").GetComponent<InputManager>();
		UpdateText();
	}
	
	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(inputmanager.enabled == true)
			{
				foreach(Transform t in this.gameObject.transform)
				{
					t.gameObject.layer = 13;
				}
			}
		}
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
		
		}
		
		
		
		
	}	
	
	public override void OnEntryAnimationCompleted()
	{
		this.onExitCompleteCallBack = managerExitCallBack;
		this.State = eGUI_ITEMS_MANAGER_STATE.Exiting;
		
	}
	
	void managerExitCallBack()
	{
		_popUpManager.RemoveFromList(this.gameObject);
		Destroy(this.gameObject);
		
	}
	
	public void UpdateText()
	{
//		_guiText[0].text = "ROUND"+" "+TimeAttack.instance.GetRoundUpValue().ToString();
	//	_guiText[1].text = (Mathf.Abs (TimeAttack.instance.GetWind())).ToString();
	}
	
}