using UnityEngine;
using System.Collections;

public class OnePopUpScreen : GUIItemsManager
{
	public enum AccuracyType
	{
		good,awesome,great
	};
	public AccuracyType accuracyType;
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;

	
	bool tutorial_level = false;
	
	float startTime;
	
	//Edited by Ankit
	float _displayTime = 0.2f;
	
	bool isTriger = false;
	// Use this for initialization
	void Start ()
	{
		SoundManager.instance.WhistleSound(0);
		base.Init();	
		
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
		
		}
		
		
		
		
	}	
	
	public override void OnEntryAnimationCompleted()
	{
//		this.onExitCompleteCallBack = managerExitCallBack;
//		this.State = eGUI_ITEMS_MANAGER_STATE.Exiting;
		startTime = Time.time;
//		_screenManager.LoadScreen("2");
		isTriger = true;
	}
	
	void managerExitCallBack()
	{
//		_screenManager.LoadScreen("2");
	}
	
	void Update()
	{
		
		if(isTriger)
		{
			
			if(Time.time-startTime>_displayTime)//Edited by Ankit
			{
				_screenManager.LoadScreen("Go");
				isTriger = false;
			}
		}
	}
	
}