using UnityEngine;
using System.Collections;

public class GoPopUpScreen : GUIItemsManager
{
	public enum AccuracyType
	{
		good,awesome,great
	};
	public AccuracyType accuracyType;
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	
	
	bool tutorial_level = false;
	
	float startTime;
	
	//Edited by Ankit
	float _displayTime = 0.2f;
	
	bool isTriger = false;
	
	InputManager _inputManager;
	GameManager _gameManager;
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		_inputManager = GameObject.Find("GameManager").GetComponent<InputManager>();
		_gameManager =	GameObject.Find("GameManager").GetComponent<GameManager>();
		SoundManager.instance.WhistleSound(2);
		
		//Edited by Ankit
		if(PlayerPrefs.GetInt("PlayTimes") == 0){
			_displayTime = 0.5f;
			
			PlayerPrefs.SetInt("PlayTimes",1);
			
		}
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
		
		}
		
		
		
		
	}	
	
	public override void OnEntryAnimationCompleted()
	{
//		this.onExitCompleteCallBack = managerExitCallBack;
//		this.State = eGUI_ITEMS_MANAGER_STATE.Exiting;
		isTriger = true;
		startTime = Time.time;
//		InstructionManager instructionManager = InstructionManager.GetInstance();
//		instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"FLICK THE BALL TO GET IT INSIDE THE BARS, WHEN BALL IS IN AIR, SWIPE TO SWING",true);
	}
	
	void managerExitCallBack()
	{
//		Destroy(this.gameObject);
//		_inputManager.enabled=true;
		
		
	}
	
	void Update()
	{
		if(isTriger)
		{
			if(Time.time-startTime>_displayTime) //Edited by Ankit
			{
				ScreenManager _screenManager = GameObject.Find("ScreenManager").GetComponent<ScreenManager>();
				if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
				_screenManager.LoadScreen("PracticeGameplay");
				if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
				{
					_screenManager.LoadScreen("TimeAttackGamePlay");
					TimeAttack.instance .SeChkTimeCondition(true);
				}
				if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
				_screenManager.LoadScreen("SuddenDeathGamePlay");
				if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
					LevelHandler levelhandler = LevelHandler.instance;

					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
					{
						_screenManager.LoadScreen("ArcadeGamePlay");
						
					}
					
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						_screenManager.LoadScreen("SkillShot");
						levelhandler.SetIsTimerCondition(true);
						
					}
				}
				Destroy(this.gameObject);
				_inputManager.enabled=true;
				isTriger = false;
				
				
			}
		}
	}
	
}