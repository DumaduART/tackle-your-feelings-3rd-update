using UnityEngine;
using System.Collections;

public class SkillShotGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GUITexture LeftwindDir;
	public GUITexture rightWindDir;
	public GUITexture windBg;
	public GUITexture distanceBg;
	public GUIText windValue;
	public GUIText wind;
	public GUIText mph;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	PopUpManager _popUpManager;
	bool tutorial_level = false;
	public static  SkillShotGUIItemsManager instance;
	public GuiTextManager _guitextmanager;
	
	public GameObject ballSelectionSubScreen;
	
	static GameObject ballselectinClone;
	
	
	Vector3 targetScale = new Vector3(0.2f , 0.15f , 0.0f);
	Vector3 defaltScale = new Vector3(0.15f , 0.1125f , 0.0f);
	
	bool Ischeck = true;
	public bool isActivate = false;
	int	index = 0;
	public bool _isAndroidBack = false;
	// Use this for initialization
	void Awake()
	{
		instance=this;
	}
	
	void Start ()
	{
		
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		_popUpManager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		ChangeWindImage(LevelHandler.instance._windType);
		
		ballselectinClone = Instantiate(ballSelectionSubScreen) as GameObject;
	}
	
	
	public static GameObject GetBallSelectionClone()
	{
		return(ballselectinClone);
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	
	public override void OnEntryAnimationCompleted()
	{
		defaltScale = distanceBg.transform.localScale;
		_isAndroidBack = true;
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "Pause")
			{
				if(_inputManager.GetRefOfBall() == null)
					return;
				if(_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().ballStatus == CollisionDetection.BallStatus.nothitted && LevelHandler.instance.GetTimeLeft()>1)
				{
					ballselectinClone.GetComponent<ScreenManager>().closeScreenManager();
						_screenManager.LoadScreen("Pause");
						EventManager.GamePauseTrigger();
					_popUpManager.HidePopUp();
				}
				
			}
			
			if(item.name == "P1Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[0]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[0]._poweruptype;
					if( _powerManager._SelectedPowerups[0]._count>0)
					 _powerManager._SelectedPowerups[0]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
			
			if(item.name == "P2Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[1]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[1]._poweruptype;
					if( _powerManager._SelectedPowerups[1]._count>0)
					 _powerManager._SelectedPowerups[1]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
			
			if(item.name == "P3Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[2]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[2]._poweruptype;
					if( _powerManager._SelectedPowerups[2]._count>0)
					 _powerManager._SelectedPowerups[2]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
		}
		
		
		
	
	}	
	public void ChangeWindImage(LevelHandler.WindType  windType)
	{
		LevelHandler levelhandler = LevelHandler.instance;
		if(Mathf.Abs(levelhandler.GetWind())>0)
		{
			windBg.gameObject.SetActive(true);
			windValue.gameObject.SetActive(true);
			wind.gameObject.SetActive(true);
			mph.gameObject.SetActive(true);
			switch(windType)
			{
				case LevelHandler.WindType.left:
					LeftwindDir.gameObject.SetActive(true);
					rightWindDir.gameObject.SetActive(false);
					break;
				case LevelHandler.WindType.right:
					rightWindDir.gameObject.SetActive(true);
					LeftwindDir.gameObject.SetActive(false);
					break;
			}
		}
		else
		{
			rightWindDir.gameObject.SetActive(false);
			LeftwindDir.gameObject.SetActive(false);
			windBg.gameObject.SetActive(false);
			windValue.gameObject.SetActive(false);
			wind.gameObject.SetActive(false);
			mph.gameObject.SetActive(false);
		}
	}
	
	void Update()
	{
		if(isActivate)
		{
			if(Ischeck)
			{
				
				//distanceBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , targetScale , 0.4f * Time.deltaTime);
				windBg.transform.localScale = Vector3.MoveTowards(windBg.transform.localScale , targetScale , 0.4f * Time.deltaTime);		
				if(windBg.transform.localScale.Equals(targetScale))
				{
					Ischeck = false;
					index++;
				}
				
			}
			else
			{
				//distanceBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , defaltScale , 0.3f * Time.deltaTime);
				windBg.transform.localScale = Vector3.MoveTowards(windBg.transform.localScale , defaltScale , 0.3f * Time.deltaTime);
				if(windBg.transform.localScale.Equals(defaltScale))
				{
					Ischeck = true;
					if(index == 2)
					{
						isActivate = false;
						index = 0;
					}
				}
			}
		}
		
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				if(_inputManager.GetRefOfBall() == null)
					return;
				if(_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().ballStatus == CollisionDetection.BallStatus.nothitted && LevelHandler.instance.GetTimeLeft()>1)
				{
					ballselectinClone.GetComponent<ScreenManager>().closeScreenManager();
						_screenManager.LoadScreen("Pause");
						EventManager.GamePauseTrigger();
					_popUpManager.HidePopUp();
					_isAndroidBack = false;
				}
				
			}
		}
	}
}