using UnityEngine;
using System.Collections;

public class StoreScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	public GUIText[] guitext;
	bool tutorial_level = false;
	public GuiTextManager _guitextmanager;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
				_screenManager.LoadScreen(_screenManager.userdata);
				//GameObject.Find("SubScreenManager(Clone)").GetComponent<ScreenManager>().closeScreenManager();
				 
			}
			
			if(item.name == "ballsbtn")
			{
				GameObject.Find("SubScreenManager(Clone)").GetComponent<ScreenManager>().LoadScreen("Ball");
			}
			
			if(item.name == "Buybtn1")
			{
				print (item.name);
				PowerUpsManager.instance.PurchaseItem(PowerUpType.FireShot);
				_guitextmanager.UpdateText();
			}
			
			if(item.name == "Buybtn2")
			{
				print (item.name);
				PowerUpsManager.instance.PurchaseItem(PowerUpType.CannonBall);
				_guitextmanager.UpdateText();
			}
			
			if(item.name == "Buybtn3")
			{
				print (item.name);
				PowerUpsManager.instance.PurchaseItem(PowerUpType.PaperSoul);
				_guitextmanager.UpdateText();
			}
			
			if(item.name == "Buybtn4")
			{
				print (item.name);
				PowerUpsManager.instance.PurchaseItem(PowerUpType.Stickiee);
				_guitextmanager.UpdateText();
			}
			
			if(item.name == "Selected1")
			{
				print (item.name);
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.FireShot);
			}
			
			if(item.name == "Deselected1")
			{
				//PowerUpsManager.instance.RemoveSelectedPowerUp(PowerUpType.FireShot);
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.FireShot);
			}
			
			if(item.name == "Selected2")
			{
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.CannonBall);
			}
			
			if(item.name == "Deselected2")
			{
				//PowerUpsManager.instance.RemoveSelectedPowerUp(PowerUpType.CannonBall);
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.CannonBall);
			}
			
			if(item.name == "Selected3")
			{
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.PaperSoul);
				
			}
			
			if(item.name == "Deselected3")
			{
				//PowerUpsManager.instance.RemoveSelectedPowerUp(PowerUpType.PaperSoul);
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.PaperSoul);
			}
			
			if(item.name == "Selected4")
			{
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.Stickiee);
			}
			
			if(item.name == "Deselected4")
			{
				//PowerUpsManager.instance.RemoveSelectedPowerUp(PowerUpType.Stickiee);
				PowerUpsManager.instance.AddSelectedPowerUp(PowerUpType.Stickiee);
			}
			
			if(item.name == "PowerUpbtn")
			{
				 GameObject.Find("SubScreenManager(Clone)").GetComponent<ScreenManager>().LoadScreen("PowerUp");
			}
			
			if(item.name == "GetMoreBtn")
			{
				print (item.name);
			}
		}
		
		
		
	}	
}