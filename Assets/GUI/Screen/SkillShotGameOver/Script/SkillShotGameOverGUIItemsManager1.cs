using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SkillShotGameOverGUIItemsManager1 : GUIItemsManager
{
	public static SkillShotGameOverGUIItemsManager1 instance;
	public GameObject MainBackGround;	
	
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager ;
	bool tutorial_level = false;
	public Texture2D challengeClearedImage;
	public Texture2D challengenotClearedImage;
	public Texture2D goldTrophy;
	public Texture2D silverTrophy;
	public Texture2D bronzeTrophy;
	public Texture2D noTrophy;
	public GUITexture trophyTexture;
	public GUITexture achhievedTexture;
	public GUITexture titleTexture;
	public GameObject Xp_camera;
	private GameObject XpClone;
	public GameObject[] _StoreElement;
	public GUITexture _nextBtn;
	public GUIText _nextBtnText;
	public GUIText _retryBtnText;
	// Use this for initialization
	public bool _isAndroidBack = false;
    public Image MessagePopUp;
    public UnityEngine.Sprite[] _MessageSpries;
    public GameObject RetryButton, NextButton;

	void Awake()
	{
		instance = this;
	}
	
	void Start ()
	{
		base.Init();

		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
		
	//	XpClone = Instantiate(Xp_camera)as GameObject;
		LevelHandler levelhandler = LevelHandler.instance;
		levelhandler.ResetFinishedConsecutive();

		if(levelhandler.GetChallengeCleredCondition())
				{
					if(_gameManager.GetCategory() == GameManager.Category.ThroughRings)
					{
						if(_gameManager.GetIndex()<14)
						{
							_gameManager.SaveThroughRingsCondition(_gameManager.GetIndex()+1);
							_nextBtn.gameObject.SetActive(true);
							_nextBtnText.gameObject.SetActive(true);
							_nextBtnText.text = "RETRY";
							_retryBtnText.text = "NEXT";
						}
					}
					
					if(_gameManager.GetCategory() == GameManager.Category.HitTheBar)
					{
						if(_gameManager.GetIndex()<11)
						{
							_gameManager.SaveHitTheBarCondition(_gameManager.GetIndex()+1);
							_nextBtn.gameObject.SetActive(true);
							_nextBtnText.gameObject.SetActive(true);
							_nextBtnText.text = "RETRY";
							_retryBtnText.text = "NEXT";
						}
					}
			
					if(_gameManager.GetCategory() == GameManager.Category.NoSwingStorm)
					{
						if(_gameManager.GetIndex()<11)
						{
							_gameManager.SaveNoSwingCondition(_gameManager.GetIndex()+1);
							_nextBtn.gameObject.SetActive(true);
							_nextBtnText.gameObject.SetActive(true);
							_nextBtnText.text = "RETRY";
							_retryBtnText.text = "NEXT";
						}
					}
					
				}
				
				else
				{
					
				}
		ChkforTrophy();
		
		if(GameObject.Find("BallSelectionSubScreenManager(Clone)"))
				GameObject.Find("BallSelectionSubScreenManager(Clone)").GetComponent<ScreenManager>().closeScreenManager ();
		GUIManager.instance.GetBgTexture().gameObject.SetActive(false);

        MessagePopUp.sprite = _MessageSpries[Random.Range(0, 9)];
	}
	
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			//if(_isAndroidBack)
			//{

				_inputManager.GetCameraref().transform.parent=null; 
				EventManager.GameHomeTrigger();
				_screenManager.LoadScreen("MainMenu");
				Destroy(XpClone);

			//	_isAndroidBack = false;
			//}
		}
	}
	
	public override void OnEntryAnimationCompleted()
	{
		if(_gameManager.ChkLevelUpCondition())
		{
			PopUpManager.instance.CreatePopUp(PopUpType.levelup);
			_gameManager.SetLevelUpCondition(false);
			_isAndroidBack = false;
		}
		
		else
		_isAndroidBack = true;
		
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	
	public void MenuButtonClicked()
    {
        try
        {
            _inputManager.GetCameraref().transform.parent = null;
            EventManager.GameHomeTrigger();
            _screenManager.LoadScreen("MainMenu");
            Destroy(XpClone);
        }
        catch (System.Exception e)
        {
            
        }
    }
    public void RetryButtonClicked()
    {
        try
        {
            LevelHandler levelhandler = LevelHandler.instance;
            if (levelhandler.GetChallengeCleredCondition())
            {
                switch (_gameManager.GetCategory())
                {
                    case GameManager.Category.ThroughRings:
                        {
                            if (_gameManager.GetIndex() < 14)
                                _gameManager.SetIndex(_gameManager.GetIndex() + 1);
                            break;
                        }

                    case GameManager.Category.HitTheBar:
                        {
                            if (_gameManager.GetIndex() < 11)
                                _gameManager.SetIndex(_gameManager.GetIndex() + 1);
                            break;
                        }

                    case GameManager.Category.NoSwingStorm:
                        {
                            if (_gameManager.GetIndex() < 11)
                                _gameManager.SetIndex(_gameManager.GetIndex() + 1);
                            break;
                        }
                }

                _inputManager.GetCameraref().transform.parent = null;
                EventManager.GameReStartTrigger();
                _screenManager.LoadScreen("Empty");
            }
            else
            {
                _inputManager.GetCameraref().transform.parent = null;
                EventManager.GameReStartTrigger();
                _screenManager.LoadScreen("Empty");
            }
        }
        catch(System.Exception e)
        {
            
        }
    }
    public void OnNextButtonClicked()
    {
        try
        {
                          _gameManager.SetIndex(_gameManager.GetIndex()+1);
                          _inputManager.GetCameraref().transform.parent=null; 
                          EventManager.GameReStartTrigger();  
                          _screenManager.LoadScreen("Empty");
        }
        catch (System.Exception e)
        {

        }
    }
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "MenuBtn")
			{
                try
                {
                    _inputManager.GetCameraref().transform.parent = null;
                    EventManager.GameHomeTrigger();
                    _screenManager.LoadScreen("MainMenu");
                    Destroy(XpClone);
                }
                catch(System.Exception e)
                {
                    
                }
			}
			
			if(item.name == "StoreBtn")
			{
				_screenManager.userdata = "SkillShotGameOver";
				  _screenManager.LoadScreen("ShopScreen");
				subscreenitemmanager.defaultScreen = "BallSelectionScreen";
				 //GameObject subs =	Instantiate(subscreenmanager) as GameObject;
				// subs.GetComponent<ScreenManager>().LoadScreen("Ball");
				Destroy(XpClone);
				GUIManager.instance.GetBgTexture().gameObject.SetActive(true);
			}
			
			
			if(item.name == "FacebookShere")
			{
				
			}
			
			if(item.name == "RetryBtn")
			{
                try
                {
                    LevelHandler levelhandler = LevelHandler.instance;
                    if (levelhandler.GetChallengeCleredCondition())
                    {
                        switch (_gameManager.GetCategory())
                        {
                            case GameManager.Category.ThroughRings:
                                {
                                    if (_gameManager.GetIndex() < 14)
                                        _gameManager.SetIndex(_gameManager.GetIndex() + 1);
                                    break;
                                }

                            case GameManager.Category.HitTheBar:
                                {
                                    if (_gameManager.GetIndex() < 11)
                                        _gameManager.SetIndex(_gameManager.GetIndex() + 1);
                                    break;
                                }

                            case GameManager.Category.NoSwingStorm:
                                {
                                    if (_gameManager.GetIndex() < 11)
                                        _gameManager.SetIndex(_gameManager.GetIndex() + 1);
                                    break;
                                }
                        }

                        _inputManager.GetCameraref().transform.parent = null;
                        EventManager.GameReStartTrigger();
                        _screenManager.LoadScreen("Empty");
                    }
                    else
                    {
                        _inputManager.GetCameraref().transform.parent = null;
                        EventManager.GameReStartTrigger();
                        _screenManager.LoadScreen("Empty");
                    }
                }

                catch(System.Exception e)
                {
                    
                }
            }
			
			if(item.name == "NextBtn")
			{
                try
                {
                    //				_gameManager.SetIndex(_gameManager.GetIndex()+1);
                    //				_inputManager.GetCameraref().transform.parent=null; 
                    //				EventManager.GameReStartTrigger();	
                    //				_screenManager.LoadScreen("Empty");

                    _inputManager.GetCameraref().transform.parent = null;
                    EventManager.GameReStartTrigger();
                    _screenManager.LoadScreen("Empty");
                    //Destroy(XpClone);
                }
                catch(System.Exception e)
                {
                    
                }
			}
		}
		
		
		
		
	}
	public void ChkforTrophy()
	{
        Debug.Log("Working");
		LevelHandler levelhandler = LevelHandler.instance;
		//levelhandler.ChkforTrophy();
		if(levelhandler.GetGoldCondition() )
		{
			trophyTexture.texture = goldTrophy;
			titleTexture.texture = challengeClearedImage;
            RetryButton.SetActive(false);
            NextButton.SetActive(true);
		}
	    else if(levelhandler.GetSilverCondition())
		{
			trophyTexture.texture = silverTrophy;
			titleTexture.texture = challengeClearedImage;
            RetryButton.SetActive(false);
            NextButton.SetActive(true);
		}
		 else if(levelhandler.GetBronzeCondition())
		{
			trophyTexture.texture = bronzeTrophy;
			titleTexture.texture = challengeClearedImage;
            RetryButton.SetActive(false);
            NextButton.SetActive(true);
		}
		
		else
		{
			trophyTexture.texture = noTrophy;
			titleTexture.texture = challengenotClearedImage;
			achhievedTexture.texture = null; 
            RetryButton.SetActive(true);
            NextButton.SetActive(false);
		}

	}
	
	
}