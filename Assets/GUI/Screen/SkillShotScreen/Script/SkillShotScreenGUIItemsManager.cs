using UnityEngine;
using System.Collections;

public class SkillShotScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GameObject subscreenmanager;
	public ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	public GuiTextManager _guitextManager;
	bool tutorial_level = false;
	public static SkillShotScreenGUIItemsManager instance;
	public int ChallengeNo = 0;
	
	public GUIText hittheBarText;
	public GUIText noswingText;
	public GUIText throughtheRings;
	
	bool _isAndroidBack = false;
	// Use this for initialization
	void Start ()
	{
		instance = this;
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
		switch(_gameManager.GetCategory())
		{
			case GameManager.Category.ThroughRings:
			{
				throughtheRings.GetComponent<GUIText>().color = Color.yellow;
				subscreenitemmanager.defaultScreen = "ThroughTheRingsSelection";
				break;
			}
			
			case GameManager.Category.HitTheBar:
			{
				hittheBarText.GetComponent<GUIText>().color = Color.yellow;
				subscreenitemmanager.defaultScreen = "HitTheBarSelection";
				break;
			}
			
			case GameManager.Category.NoSwingStorm:
			{
				noswingText.GetComponent<GUIText>().color = Color.yellow;
				subscreenitemmanager.defaultScreen = "NoSwingInstormSelection";
				break;
			}
		}
		subscreenitemmanager.LoadScreen(subscreenitemmanager.defaultScreen);
		
	
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				_screenManager.LoadScreen("MainMenu");
				subscreenitemmanager.LoadScreen("Empty");
				EventManager.GameHomeTrigger();
				_isAndroidBack = false;
			}
		}
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
				_screenManager.LoadScreen("MainMenu");
				subscreenitemmanager.LoadScreen("Empty");
				EventManager.GameHomeTrigger();
			}
			if(item.name == "LeftButton")
			{
//				print (item.name);
				LoadChallenge(item.name);
				GuiTextManager.instance.UpdateText();
			}
			if(item.name == "RightButton")
			{
//				print (item.name);
//				if(_gameManager.tokens >= 2000)
//				{
//					_screenManager.LoadScreen("NoSwingStorm");
//					
//				}
//				
//				else
//					_screenManager.LoadScreen("NoswingStormLocked");
//				_gameManager.SetCategory(GameManager .Category.NoSwingStorm);
				//_gameManager.LoadChallenge(_gameManager.GetIndex());
				//Destroy(_gameManager.challenge);
				LoadChallenge(item.name);
				GuiTextManager.instance.UpdateText();
			}
			
			if(item.name == "HitTheBarBtn")
			{
				ChallengeNo = 0;
				_gameManager.SetCategory(GameManager .Category.HitTheBar);
//				_gameManager.LoadChallenge(ChallengeNo);
//				_gameManager.SetIndex(ChallengeNo);
				GuiTextManager.instance.UpdateText();
				hittheBarText.GetComponent<GUIText>().color = Color.yellow;
				noswingText.GetComponent<GUIText>().color = Color.white;
				throughtheRings.GetComponent<GUIText>().color = Color.white;
				subscreenitemmanager.LoadScreen("HitTheBarSelection");
				ButtonList.instance.SetSelectedState(item.name,true);
			}
			
			if(item.name == "ThroughRingsBtn")
			{
				ChallengeNo = 0;
				_gameManager.SetCategory(GameManager .Category.ThroughRings);
//				_gameManager.LoadChallenge(ChallengeNo);
//				_gameManager.SetIndex(ChallengeNo);
				GuiTextManager.instance.UpdateText();
				hittheBarText.GetComponent<GUIText>().color = Color.white;
				noswingText.GetComponent<GUIText>().color = Color.white;
				throughtheRings.GetComponent<GUIText>().color = Color.yellow;
				subscreenitemmanager.LoadScreen("ThroughTheRingsSelection");
				ButtonList.instance.SetSelectedState(item.name,true);
			}
			
			if(item.name == "NoSwingBtn")
			{
				ChallengeNo = 0;
				_gameManager.SetCategory(GameManager .Category.NoSwingStorm);
//				_gameManager.LoadChallenge(ChallengeNo);
//				_gameManager.SetIndex(ChallengeNo);
				GuiTextManager.instance.UpdateText();
				hittheBarText.GetComponent<GUIText>().color = Color.white;
				noswingText.GetComponent<GUIText>().color = Color.yellow;
				throughtheRings.GetComponent<GUIText>().color = Color.white;
				subscreenitemmanager.LoadScreen("NoSwingInstormSelection");
				ButtonList.instance.SetSelectedState(item.name,true);
			}
			
			if(item.name == "PlayBtn")
			{
				bool chkCondition = false;
				if(ShoScroll.instance._CurPage > 1)
				{
					if(_gameManager.GetCategory() == GameManager.Category.HitTheBar)
					{
						if(_gameManager._isHitTheBar[ShoScroll.instance._CurPage-1] == 1)
						{
							chkCondition = true;
						}
						
					}
					
					else if(_gameManager.GetCategory() == GameManager.Category.ThroughRings)
					{
						if(_gameManager._isthroughrings[ShoScroll.instance._CurPage-1] == 1)
						{
							chkCondition = true;
						}
					}
					
					else if(_gameManager.GetCategory() == GameManager.Category.NoSwingStorm)
					{
						if(_gameManager._isNoSwing[ShoScroll.instance._CurPage-1] == 1)
						{
							chkCondition = true;
						}
					}
				}
				
				else
				{
					chkCondition = true;
				}
				
				if(chkCondition)
				{
					_gameManager.SetIndex(ShoScroll.instance._CurPage-1);
					
					subscreenitemmanager.LoadScreen("Empty");
					_screenManager.prevPageBeforeSelection = "SkillShotScreen1";
					//_screenManager.LoadScreen("SelectionScreen");
//					if(PlayerPrefs.GetString("tuetorial")=="tuetorialoff")
//					{
						if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
						{
							_gameManager.LoadChallenge(_gameManager.GetIndex());
							PoleMovementRotation polemovement = _gameManager.GetChallenge().GetComponent<PoleMovementRotation>();
							if(polemovement!=null)
							{
								polemovement.Enable();
							}
						}
						
						_screenManager.LoadScreen("Empty");
						EventManager.GameStartTrigger();
						SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
//					}
					
//					else
//					{
//						//_screenManager.LoadScreen("Loading");
//					}
				}
			}
		}
		
		
		
	}	
	
	public void SetActive(bool x)
	{
		if(x)
			//this.enabled = true;
			GameObject.Find("ScreenManager").GetComponent<ScreenManager>().EnableInput ();
		else
			GameObject.Find("ScreenManager").GetComponent<ScreenManager>().DisableInput ();
		
	}
	
	public void LoadChallenge( string button)
	{
		
		if(button == "RightButton")	
		{
			if(ChallengeNo < 11)
			{
				ChallengeNo++;
			}
		}
		if(button == "LeftButton")	
		{
			if(ChallengeNo>0)
			{
				ChallengeNo--;
			}
		}
		
		_gameManager.LoadChallenge(ChallengeNo);
		_gameManager.SetIndex(ChallengeNo);
	}
	
	
	
}