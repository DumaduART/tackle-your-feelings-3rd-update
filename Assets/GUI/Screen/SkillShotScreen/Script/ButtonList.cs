using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonList : MonoBehaviour {

	// Use this for initialization
	public static ButtonList instance;
	public List<GUITexture> _button = new List<GUITexture>();
	//public GUITexture[] _button;
	public Texture2D normalTexture;
	public Texture2D selectedTexture;
	void Start () {
	   instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void SetSelectedState(string name , bool isSelected)
	{
		if(isSelected)
		{
			for(int i=0; i< _button.Count;i++)
			{
				if(_button[i].name == name)
				{
					_button[i].GetComponent<GUIItemButton>()._texNormalState = selectedTexture;
					_button[i].GetComponent<GUITexture>().texture = selectedTexture;//_button[i].GetComponent<GUIItemButton>()._texNormalState;
				}
				
				else 
				{
					_button[i].GetComponent<GUIItemButton>()._texNormalState = normalTexture;
					_button[i].GetComponent<GUITexture>().texture = normalTexture; //_button[i].GetComponent<GUIItemButton>()._texNormalState;
					
				}
				
			}
		}	
		
		else 
		{
			for(int i=0; i< _button.Count;i++)
			{
				_button[i].GetComponent<GUIItemButton>()._texNormalState = normalTexture;
				_button[i].GetComponent<GUITexture>().texture = normalTexture;
			}
		}
	}
}
