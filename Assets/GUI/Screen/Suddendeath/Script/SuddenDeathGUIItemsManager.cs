using UnityEngine;
using System.Collections;

public class SuddenDeathGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GUITexture LeftwindDir;
	public GUITexture rightWindDir;
	public GUITexture windBg;
	public GUITexture distanceBg;
	public GUIText windValue;
	public GUIText wind;
	public GUIText mph;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	PopUpManager _popupmanager;
	public static SuddenDeathGUIItemsManager instance;
	bool tutorial_level = false;
	public GuiTextManager _guitextmanager;
	// Use this for initialization
	public GUIText _scoreText;
	public GUIText _multiplierText;
	public GUIText _timerText;
	Vector3 defaultScale;
	ArrayList AnimationObjects = new ArrayList();
	BallSelectionSubScreenManager _ballSelect;
	SuddenDeath suddenDeath;
	
	public GameObject _gBallSelectionScreenMaanger;
	
	public static GameObject _gBallSelectionClone;
	
	Vector3 targetScale = new Vector3(0.2f , 0.15f , 0.0f);
	Vector3 defaltScale = new Vector3(0.15f , 0.1125f , 0.0f);
	
	bool Ischeck = true;
	public bool isActivate = false;
	int	index = 0;
	
	public static bool _ReviveSuddenDeath;
	public bool _isAndroidBack = false;
	
	void Awake()
	{
		instance = this;
		_gBallSelectionClone = Instantiate (_gBallSelectionScreenMaanger) as GameObject;

//		if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode)
//			Invoke ("CloseballSelect", 0.01f);
	}
	
	void Start ()
	{	
		
		base.Init();
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		_popupmanager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		suddenDeath = SuddenDeath.GetInstance();
		ChangeWindImage(suddenDeath.GetWindType());
		EventManager.GameOver += GameOver ;
        			
	}

	void CloseballSelect()
	{
		_gBallSelectionClone.GetComponent<ScreenManager>().closeScreenManager();
	}

	void OnDestroy()
	{
		EventManager.GameOver -= GameOver ;
	}

	public  static GameObject GetBallSelectionClone()
	{
		return(_gBallSelectionClone);
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}
	
	public override void OnEntryAnimationCompleted()
	{
		defaltScale = distanceBg.transform.localScale;
		_isAndroidBack = true;
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "Pause")
			{
				if(_inputManager.GetRefOfBall() == null)
					return;
				if(_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().ballStatus == CollisionDetection.BallStatus.nothitted)
				{
					StopAllActions();
					_screenManager.LoadScreen("Pause");
						EventManager.GamePauseTrigger();
					_popupmanager.HidePopUp();
					if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode)
					   _gBallSelectionClone.GetComponent<ScreenManager>().closeScreenManager();
				}
			}
			if(item.name == "P1Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[0]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[0]._poweruptype;
					if( _powerManager._SelectedPowerups[0]._count>0)
					 _powerManager._SelectedPowerups[0]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
			
			if(item.name == "P2Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[1]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[1]._poweruptype;
					if( _powerManager._SelectedPowerups[1]._count>0)
					 _powerManager._SelectedPowerups[1]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
			
			if(item.name == "P3Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[2]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[2]._poweruptype;
					if( _powerManager._SelectedPowerups[2]._count>0)
					 _powerManager._SelectedPowerups[2]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
		}
		
		
		
		
		
	}
	
	public void ChangeWindImage(SuddenDeath.WindType  windType)
	{
		SuddenDeath suddendeath = SuddenDeath.instance;
		if(Mathf.Abs(suddendeath.GetWind())>0)
		{
			windBg.gameObject.SetActive(true);
			windValue.gameObject.SetActive(true);
			wind.gameObject.SetActive(true);
			mph.gameObject.SetActive(true);
			switch(windType)
			{
				case SuddenDeath.WindType.left:
					LeftwindDir.gameObject.SetActive(true);
					rightWindDir.gameObject.SetActive(false);
					break;
				case SuddenDeath.WindType.right:
					rightWindDir.gameObject.SetActive(true);
					LeftwindDir.gameObject.SetActive(false);
					break;
			}
			
		}
		else 
		{
			rightWindDir.gameObject.SetActive(false);
			LeftwindDir.gameObject.SetActive(false);
//			windBg.gameObject.SetActive(false);
//			windValue.gameObject.SetActive(false);
//			wind.gameObject.SetActive(false);
//			mph.gameObject.SetActive(false);
		}
	}
	
	GUIText availabaleText = null;
	GameObject mAnimationObject = null;
	void StopActions(GameObject pObj)
	{
		if(pObj != null)
		{
			Destroy(pObj);
			AnimationObjects.Remove(pObj);
		}
		
		if(availabaleText!=null)
		availabaleText.transform.localScale = defaultScale;
	}
	
	void StopAllActions()
	{
		foreach(GameObject tObj in AnimationObjects)
			Destroy(tObj);
		
		AnimationObjects.Clear();
	}
	
	public void StartZoomInZoomOut(TextType textType)
	{
		
		switch(textType)
		{
			case TextType.score:
			{
				availabaleText = _scoreText;
				break;
			}
			
			case TextType.multiplier:
			{
				availabaleText = _multiplierText ;
				break;
			}
			
			case TextType.timer:
			{
				availabaleText = _timerText ;
				break;
			}
		}
		
		defaultScale = availabaleText.gameObject.transform.localScale;
		GameObject tAnimObject = new GameObject();
		tAnimObject.name = "Trial animation";
		Vector3 scale = availabaleText.gameObject.transform.localScale;
		
		CScaleTo tScale = tAnimObject.AddComponent<CScaleTo>();
		tScale.actionWith(availabaleText.gameObject,new Vector2(scale.x*1.2f,scale.y*1.2f),1.0f);
		
		CEaseElastic tease1 = tAnimObject.AddComponent<CEaseElastic>();
		tease1.actionWithAction(tScale,EaseType.EaseOut);
		
		CScaleTo tScale2 = tAnimObject.AddComponent<CScaleTo>();
		tScale2.actionWith(availabaleText.gameObject,new Vector2(scale.x,scale.y),1.0f);
		
		CEaseElastic tease2 = tAnimObject.AddComponent<CEaseElastic>();
		tease2.actionWithAction(tScale2,EaseType.EaseOut);
		
		CSequence seq1 = tAnimObject.AddComponent<CSequence>();
		seq1.actionWithActions(tease1,tease2);
		
		CCallFuncWithObj call = tAnimObject.AddComponent<CCallFuncWithObj>();
		call.actionWithCallBack(StopActions,tAnimObject);
		
		CSequence seq = tAnimObject.AddComponent<CSequence>();
		seq.actionWithActions(seq1,call);
		AnimationObjects.Add(tAnimObject);

		seq.runAction();
	}
	
	
	void Update()
	{
		if(isActivate)
		{
			if(Ischeck)
			{
				
				distanceBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , targetScale , 0.4f * Time.deltaTime);
				windBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , targetScale , 0.4f * Time.deltaTime);		
				if(distanceBg.transform.localScale.Equals(targetScale))
				{
					Ischeck = false;
					index++;
				}
				
			}
			else
			{
				distanceBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , defaltScale , 0.3f * Time.deltaTime);
				windBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , defaltScale , 0.3f * Time.deltaTime);
				if(distanceBg.transform.localScale.Equals(defaltScale))
				{
					Ischeck = true;
					if(index == 2)
					{
						distanceBg.transform.localScale = defaltScale;
						windBg.transform.localScale = defaltScale;
						isActivate = false;
						index = 0;
					}
				}
			}
		}
		
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				if(_inputManager.GetRefOfBall() == null)
					return;
				if(_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().ballStatus == CollisionDetection.BallStatus.nothitted)
				{
					StopAllActions();
					_screenManager.LoadScreen("Pause");
						EventManager.GamePauseTrigger();
					_popupmanager.HidePopUp();
					if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode)	
					   _gBallSelectionClone.GetComponent<ScreenManager>().closeScreenManager();
					
					_isAndroidBack = false;
				}
				
			}
		}
	}
	
	public override void OnExitAnimationCompleted()
	{
		StopAllActions();
	}
	
	private void GameOver()
	{
		StopAllActions();
	}
}