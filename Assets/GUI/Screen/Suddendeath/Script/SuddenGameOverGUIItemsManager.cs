using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FBKit;
using UnityEngine.UI;

public class SuddenGameOverGUIItemsManager : GUIItemsManager
{
	public static SuddenGameOverGUIItemsManager instance;
	public GameObject MainBackGround;	
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	public GameObject Xp_camera;
	private GameObject XpClone;
	bool tutorial_level = false;
	public Texture2D deActivatedFb;
	public Texture2D deActivatedInvite;

	public GUITexture fbShare;
	public GUITexture fbInvite;
	public GUITexture coinimage;
	bool mbRequestSent;
	GameObject mgoActionObj,mgoActionObj1;
	public GameObject[] _StoreElement;
	public bool _isAndroidBack = false;

	public GameObject[] MenuBtn;
	public GameObject[] RetryBtn;
    public Image MessagePopUp;
    public UnityEngine.Sprite[] _MessageSpries;

	
	// Use this for initialization
	void Awake()
	{
		instance = this;
		if(GameManager.GetInstance().isFacebookReq)
		{
			fbShare.gameObject.SetActive(true);
			fbInvite.gameObject.SetActive(true);
		}
		
		else
		{
			//#if (UNITY_STANDALONE_OSX || UNITY_WP8 || UNITY_WEBPLAYER)
			fbShare.gameObject.SetActive(false);
			fbInvite.gameObject.SetActive(false);
			//#endif
		}
	}
	
	void Start ()
	{
		mbRequestSent = false;
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
		//XpClone = Instantiate(Xp_camera) as GameObject;
		base.Init();	
//		if(!OnlineSocialHandler.Instance.GetIsLoggedIn())
//		{
//			fbShare.GetComponent<GUITexture>().texture = deActivatedFb;
//			fbInvite.GetComponent<GUITexture>().texture = deActivatedInvite;
//			fbShare.GetComponent<GUIItemButton>()._texNormalState = deActivatedFb;
//			fbShare.GetComponent<GUIItemButton>()._texSelectedState = deActivatedFb;
//			fbShare.GetComponent<GUIItemButton>()._texHoverState = deActivatedFb;
//			fbShare.GetComponent<GUIItemButton>()._texDisabledState = deActivatedFb;
//			
//			fbInvite.GetComponent<GUIItemButton>()._texNormalState = deActivatedInvite;
//			fbInvite.GetComponent<GUIItemButton>()._texSelectedState = deActivatedInvite;
//			fbInvite.GetComponent<GUIItemButton>()._texHoverState = deActivatedInvite;
//			fbInvite.GetComponent<GUIItemButton>()._texDisabledState = deActivatedInvite;
//			
//			coinimage.gameObject.SetActive(false);
//		}
//		

		if(_gameManager.UpdateIntDataInDevice("noOfInvite",_gameManager.noOfInvite,true)>=15)
		{
			coinimage.gameObject.SetActive(false);
		}
		
		if(GameObject.Find("BallSelectionSubScreenManager(Clone)"))
				GameObject.Find("BallSelectionSubScreenManager(Clone)").GetComponent<ScreenManager>().closeScreenManager ();
		
		GUIManager.instance.GetBgTexture().gameObject.SetActive(false);
		_inputManager.enabled = false;

        if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode) {

			Invoke ("GotoMainMenuFromMsport", 7);
		}

        MessagePopUp.sprite = _MessageSpries[Random.Range(0, 9)];

	}
	
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{

			//if(_isAndroidBack)
			//{
				_inputManager.GetCameraref().transform.parent=null; 
				EventManager.GameHomeTrigger();
				_screenManager.LoadScreen("MainMenu");
				Destroy(XpClone);

			//	_isAndroidBack = false;
			//}
		}
	}

	void GotoMainMenuFromMsport()
	{
		_inputManager.GetCameraref().transform.parent=null; 
		EventManager.GameHomeTrigger();
		_screenManager.LoadScreen("MainMenu");
		Destroy(XpClone);
		SuddenDeathGUIItemsManager._ReviveSuddenDeath = false;
	}
	
	public override void OnEntryAnimationCompleted()
	{
		if(_gameManager.ChkLevelUpCondition())
		{
			PopUpManager.instance.CreatePopUp(PopUpType.levelup);
			_gameManager.SetLevelUpCondition(false);
			
			_isAndroidBack = false;
			SuddenDeathGUIItemsManager._ReviveSuddenDeath = false;

		}
		
		else 
			_isAndroidBack = true;

	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
    public void MainMenuClicked()
    {
        try
        {
            _inputManager.GetCameraref().transform.parent = null;
            EventManager.GameHomeTrigger();
            _screenManager.LoadScreen("MainMenu");
            Destroy(XpClone);
            SuddenDeathGUIItemsManager._ReviveSuddenDeath = false;
        }
        catch(System.Exception e)
        {
            
        }
    }
    public void RetryButtonClicked()
    {
        try
        {
            SuddenDeathGUIItemsManager._ReviveSuddenDeath = false;
            _inputManager.GetCameraref().transform.parent = null;
            EventManager.GameReStartTrigger();
            if (_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
                _screenManager.LoadScreen("Empty");
            Destroy(XpClone);
        }
        catch(System.Exception e)
        {
            
        }
    }
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "MenuBtn")
			{
                try
                {
                    _inputManager.GetCameraref().transform.parent = null;
                    EventManager.GameHomeTrigger();
                    _screenManager.LoadScreen("MainMenu");
                    Destroy(XpClone);
                    SuddenDeathGUIItemsManager._ReviveSuddenDeath = false;
                }
                catch(System.Exception e)
                {
                    
                }

			}
			
			if(item.name == "StoreBtn")
			{

				_screenManager.userdata = "SuddendeathOver";
				 _screenManager.LoadScreen("ShopScreen");
				subscreenitemmanager.defaultScreen = "BallSelectionScreen";
				 //GameObject subs =	Instantiate(subscreenmanager) as GameObject;
				// subs.GetComponent<ScreenManager>().LoadScreen("Ball");
				Destroy(XpClone);
				GUIManager.instance.GetBgTexture().gameObject.SetActive(true);
			}
			
			
			if(item.name == "FacebookShere")
			{
				//if(OnlineSocialHandler.Instance.GetIsLoggedIn())
				//Share(ScoreManager.instance.CurrentScore());
			}
			
			if(item.name == "RetryBtn")
			{
                try
                {
                    SuddenDeathGUIItemsManager._ReviveSuddenDeath = false;

                    _inputManager.GetCameraref().transform.parent = null;
                    EventManager.GameReStartTrigger();
                    if (_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
                        _screenManager.LoadScreen("Empty");
                    Destroy(XpClone);
                }
                catch(System.Exception e)
                {
                    
                }

			}
			
			if(item.name == "InviteBtn")
			{
//				if(OnlineSocialHandler.Instance.GetIsLoggedIn() && !mbRequestSent)
//				{
//					mbRequestSent = true;	
//					if(mgoActionObj != null)
//					{
//						Destroy(mgoActionObj);
//						mgoActionObj = null;
//					}
//					mgoActionObj = new GameObject();					
//					CDelayTime delay = mgoActionObj.AddComponent<CDelayTime>();
//					delay.actionWithDuration(3.0f);					
//					CCallFunc callBack = mgoActionObj.AddComponent<CCallFunc>();
//					callBack.actionWithCallBack(SetRequestVar);					
//					CSequence seq =mgoActionObj.AddComponent<CSequence>();
//					seq.actionWithActions(delay,callBack);
//					seq.runAction();
//					
//					Invite ();
//					
//					if(_gameManager.noOfInvite<17)
//					{
//						_gameManager.noOfInvite++;
//						_gameManager.UpdateIntDataInDevice("noOfInvite",_gameManager.noOfInvite,false);
//					}
//				}
			}
		}	
	}	
	void SetRequestVar()
	{
		mbRequestSent = false;
	}
	
	void Invite()
	{
		string FriendSelectorTitle = "Let's Play Rugby 3D";
   		string FriendSelectorMessage = "Hi! Check out this cool Game!";
    	string FriendSelectorFilters = "[\"app_non_users\"]";
    	string FriendSelectorData = "";
    	string FriendSelectorMax = "150";	
		string FriendSelectorId = "";	
		
		JSONObject jObj = new JSONObject();
		jObj.AddField("message",FriendSelectorMessage);
		jObj.AddField("filters",FriendSelectorFilters);
		jObj.AddField("maxRecipients",FriendSelectorMax);
		jObj.AddField("title",FriendSelectorTitle);
		jObj.AddField("data",FriendSelectorData);
		jObj.AddField("id",FriendSelectorId);
		
		OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.INVITE_FRIENDS,jObj.print(),CallBack);
	}
	void Share(int val)
	{
		string FeedLink = "https://itunes.apple.com/us/app/football-kick-flick-free-rugby/id769338889?ls=1&mt=8";
		#if UNITY_ANDROID 
		FeedLink = "https://play.google.com/store/apps/details?id=com.dumadugames.footballkickflick";
		#endif
		string FeedLinkName = "Football Kick Flick";
		string FeedLinkCaption = "Flick yourself to Glory in Football Kick Flick";
		string FeedLinkDescription = "I have scored " +val +" points in Sudden Death Mode";
		
		
		JSONObject jObj = new JSONObject();
		jObj.AddField("link",FeedLink);
		jObj.AddField("linkName",FeedLinkName);
		jObj.AddField("linkCaption",FeedLinkCaption);
		jObj.AddField("linkDescription",FeedLinkDescription);
		
		OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.SHARE_ON_WALL,jObj.print(),CallBack);
	}
	void CallBack(string strResult ,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null)
	{
		if (eRequestType == eONLINE_REQ_TYPE.INVITE_FRIENDS)
		{
		   if(_gameManager.UpdateIntDataInDevice("noOfInvite",_gameManager.noOfInvite,true)<=15)
			{
				if(mgoActionObj1 != null)
				{
					Destroy(mgoActionObj1);
					mgoActionObj1 = null;
				}
				mgoActionObj1 = new GameObject();					
				CDelayTime delay = mgoActionObj1.AddComponent<CDelayTime>();
				delay.actionWithDuration(2.0f);					
				CCallFunc callBack = mgoActionObj1.AddComponent<CCallFunc>();
				callBack.actionWithCallBack(AddCoins);	
			
				CSequence seq =mgoActionObj1.AddComponent<CSequence>();
				seq.actionWithActions(delay,callBack);
				seq.runAction();
			}	
		}
		else if(eRequestType == eONLINE_REQ_TYPE.SHARE_ON_WALL)
		{
			// add coins	
		}
		else if(eRequestType == eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION)
		{
		}
		
	}
	void AddCoins()
	{
		_gameManager.AddCoin(10);
		GuiTextManager.instance.UpdateText();
	}
	
	void OnDestroy()
	{
		if(mgoActionObj != null)
		{
			Destroy(mgoActionObj);
			mgoActionObj = null;
		}
		if(mgoActionObj1 != null)
		{
			Destroy(mgoActionObj1);
			mgoActionObj1 = null;
		}
	}
	
}