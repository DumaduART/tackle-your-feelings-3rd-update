﻿using UnityEngine;
using System.Collections;

public class AssigningTimeDelayManager : MonoBehaviour 
{
	[System.Serializable]
	public class OBJECTSSERIAL
	{
		public GameObject[] _objectsToAssignDelay;
		public float _cDeltaTime;
	}
	
	public OBJECTSSERIAL[] _OBJECTSSERIAL;
	
	public float _duration = 0.1f;
	
	void Awake ()
	{
		for (int i =0; i < _OBJECTSSERIAL.Length; i++)
		{
			for(int j = 0; j < 	_OBJECTSSERIAL[i]._objectsToAssignDelay.Length; j++)
			{
				_OBJECTSSERIAL[i]._objectsToAssignDelay[j].transform.GetComponent<MenuItemEntryAnim>()._duration = _duration;
				
				if(i ==0)
				_OBJECTSSERIAL[i]._cDeltaTime = 0;
				else
				_OBJECTSSERIAL[i]._cDeltaTime = _OBJECTSSERIAL[i-1]._cDeltaTime + _duration;
				
				_OBJECTSSERIAL[i]._objectsToAssignDelay[j].transform.GetComponent<MenuItemEntryAnim>()._InitialStartDelay = _OBJECTSSERIAL[i]._cDeltaTime;
			}
		}
	}
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
