﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TrainingData
{
  	public 	int _totalTimeplayed;
}

[System.Serializable]
public class ClockShowDownData
{
	public	int _highestScore;
	public int _totalNoofTimesPlayed;
	public int _highestMultiplier;
	public int _longestSession;
	public int _highestRoundsCleared;
}

[System.Serializable]
public class SuddenDeathData
{
	public int _highestNoGoal;
	public int _totalNoofTimesPlayed;
	public int _highestMultiplier;
	
}

[System.Serializable]
public class SkillShotData
{
	public int _totalAttempts;
	public int _totalChallengedCleared;
	public int _totalBronze;
	public int _totalSilver;
	public int _totalGold;
}

[System.Serializable]
public class OverAllData
{
	public int _durationOfTimePlayed;
}

[System.Serializable]
public class StatsData {
	
	public  TrainingData  _trainingData;
	public ClockShowDownData _clockShowDownData;
	public SuddenDeathData _suddenDeathData;
	public SkillShotData _killShotData;
	public OverAllData _overAllData;
	
	public void RetriveDataFromDevice()
	{	
		GameManager gamManager = GameObject.Find("GameManager").GetComponent<GameManager>();	
		
		_trainingData._totalTimeplayed = gamManager.UpdateIntDataInDevice("_trainingData._totalTimeplayed",1,true);
		
		_clockShowDownData._highestScore = gamManager.UpdateIntDataInDevice("_clockShowDownData._highestScore",1,true);
		_clockShowDownData._totalNoofTimesPlayed = gamManager.UpdateIntDataInDevice("_clockShowDownData._totalNoofTimesPlayed",1,true);
		_clockShowDownData._highestMultiplier = gamManager.UpdateIntDataInDevice("_clockShowDownData._highestMultiplier",1,true);
		_clockShowDownData._longestSession = gamManager.UpdateIntDataInDevice("_clockShowDownData._longestSession",1,true);
		_clockShowDownData._highestRoundsCleared = gamManager.UpdateIntDataInDevice("_clockShowDownData._highestRoundsCleared",1,true);
		
		_suddenDeathData._highestNoGoal = gamManager.UpdateIntDataInDevice("_suddenDeathData._highestNoGoal",1,true);
		_suddenDeathData._totalNoofTimesPlayed = gamManager.UpdateIntDataInDevice("_suddenDeathData._totalNoofTimesPlayed",1,true);
		_suddenDeathData._highestMultiplier = gamManager.UpdateIntDataInDevice("_suddenDeathData._highestMultiplier",1,true);
		
		_killShotData._totalAttempts = gamManager.UpdateIntDataInDevice("_killShotData._totalAttempts",1,true);
		_killShotData._totalChallengedCleared = gamManager.UpdateIntDataInDevice("_killShotData._totalChallengedCleared",1,true);
		_killShotData._totalBronze = gamManager.UpdateIntDataInDevice("_killShotData._totalBronze",1,true);
		_killShotData._totalSilver = gamManager.UpdateIntDataInDevice("_killShotData._totalSilver",1,true);
		_killShotData._totalGold = gamManager.UpdateIntDataInDevice("_killShotData._totalGold",1,true);
		
		_overAllData._durationOfTimePlayed = gamManager.UpdateIntDataInDevice("_overAllData._durationOfTimePlayed",1,true);
		
	}
	
	public void UpdateDataInDevice()
	{
		GameManager gamManager = GameObject.Find("GameManager").GetComponent<GameManager>();	
		
		gamManager.UpdateIntDataInDevice("_trainingData._totalTimeplayed",_trainingData._totalTimeplayed,false);
		
		gamManager.UpdateIntDataInDevice("_clockShowDownData._highestScore",_clockShowDownData._highestScore,false);
		gamManager.UpdateIntDataInDevice("_clockShowDownData._totalNoofTimesPlayed",_clockShowDownData._totalNoofTimesPlayed,false);
		gamManager.UpdateIntDataInDevice("_clockShowDownData._highestMultiplier",_clockShowDownData._highestMultiplier,false);
		gamManager.UpdateIntDataInDevice("_clockShowDownData._longestSession",_clockShowDownData._longestSession,false);
		gamManager.UpdateIntDataInDevice("_clockShowDownData._highestRoundsCleared",_clockShowDownData._highestRoundsCleared,false);
		
		gamManager.UpdateIntDataInDevice("_suddenDeathData._highestNoGoal",_suddenDeathData._highestNoGoal,false);
		gamManager.UpdateIntDataInDevice("_suddenDeathData._totalNoofTimesPlayed",_suddenDeathData._totalNoofTimesPlayed,false);
		gamManager.UpdateIntDataInDevice("_suddenDeathData._highestMultiplier",_suddenDeathData._highestMultiplier,false);
		
		gamManager.UpdateIntDataInDevice("_killShotData._totalAttempts",_killShotData._totalAttempts,false);
		gamManager.UpdateIntDataInDevice("_killShotData._totalChallengedCleared",_killShotData._totalChallengedCleared,false);
		gamManager.UpdateIntDataInDevice("_killShotData._totalBronze",_killShotData._totalBronze,false);
		gamManager.UpdateIntDataInDevice("_killShotData._totalSilver",_killShotData._totalSilver,false);
		gamManager.UpdateIntDataInDevice("_killShotData._totalGold",_killShotData._totalGold,false);
		
		gamManager.UpdateIntDataInDevice("_overAllData._durationOfTimePlayed",_overAllData._durationOfTimePlayed,false);
	}
}
