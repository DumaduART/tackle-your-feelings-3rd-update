using UnityEngine;
using System.Collections;
using MonetizationHelper;

public class ModeSelectionforLocalLeatherBoard : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	public GUIText titleText;
	
	bool tutorial_level = false;
	
	public GameObject _LeaderBoardPrefab;
	public GameObject _AchievemntBoardPrefab;
	bool mbIsLeaderBoard;
	GameObject _TableView = null;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		
		//Instantiate(ring);
//		titleText.text = GameManager.instance.LocalLeatherBord;
//		if(titleText.text == "LEADERBOARD" && _LeaderBoardPrefab != null)
//		{
//			mbIsLeaderBoard = true;
//			_TableView = Instantiate(_LeaderBoardPrefab) as GameObject;
//			if(_screenManager.userdata == "SuddenDeath")
//				_TableView.GetComponent<CScoreTableViewDelegate>()._LeaderboardName = "SuddenDeathLB";
//			else
//				_TableView.GetComponent<CScoreTableViewDelegate>()._LeaderboardName = "ClockShowDownLB";
//			_screenManager.userdata = "";
//		}
//		else if(_AchievemntBoardPrefab != null)
//			_TableView = Instantiate(_AchievemntBoardPrefab) as GameObject;
			
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
			
			if(item.name == "BackBtn")
			{
				//DestroyTableView();
					_screenManager.LoadScreen("MainMenu");
			}
			
			if(item.name == "ClockShowDownbtn")
			{
				_screenManager.LoadScreen("LocalLeatherBoard");
				_screenManager.userdata = "ClockShowDown";
			}
			
			if(item.name == "SuddenDeathbtn")
			{
				_screenManager.LoadScreen("LocalLeatherBoard");
				_screenManager.userdata = "SuddenDeath";
			}
		
		}		
	}	
	
	
	void DestroyTableView()
	{
		if(_TableView != null)
			Destroy(_TableView);
	}
}