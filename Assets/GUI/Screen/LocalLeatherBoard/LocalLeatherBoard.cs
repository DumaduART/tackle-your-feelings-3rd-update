using UnityEngine;
using System.Collections;
using MonetizationHelper;

public class LocalLeatherBoard : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	public GUIText titleText;
	
	bool tutorial_level = false;
	
	public GameObject _CamPrefab = null;
	public GameObject _LeaderBoardPrefab;
	public GameObject _AchievemntBoardPrefab;
	bool mbIsLeaderBoard;
	GameObject mTableView = null;
	GameObject mCamera = null;
	
	public GUIText suddenDeathText;
	public GUIText clockShowdownText;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		mCamera = Instantiate(_CamPrefab) as GameObject;
		//Instantiate(ring);
		titleText.text = GameManager.instance.LocalLeatherBord;
//		if(titleText.text == "LEADERBOARD" && _LeaderBoardPrefab != null)
//		{
//			mbIsLeaderBoard = true;
//			mTableView = Instantiate(_LeaderBoardPrefab) as GameObject;
//			if(_screenManager.userdata == "SuddenDeath")
//				mTableView.GetComponent<CScoreTableViewDelegate>()._LeaderboardName = "SuddenDeathLB";
//			else
//				mTableView.GetComponent<CScoreTableViewDelegate>()._LeaderboardName = "ClockShowDownLB";
//			_screenManager.userdata = "";
//		}
//		else if(_AchievemntBoardPrefab != null)
//			mTableView = Instantiate(_AchievemntBoardPrefab) as GameObject;
		_screenManager.userdata = "SuddenDeath";
		suddenDeathText.GetComponent<GUIText>().color = Color.yellow;
		CreateTableView();
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "ResetBtn" && mTableView != null)
			{
				if(mbIsLeaderBoard)
				{
					mTableView.GetComponent<CScoreTableViewDelegate>().Clear();
					ScoreHandler.Instance.ClearAllScores();
				}
			}
			
			if(item.name == "BackBtn")
			{
				DestroyTableView();
					_screenManager.LoadScreen("MainMenu");
			}
			
			if(item.name == "ClockShowdownBtn")
			{
				_screenManager.userdata = "ClockshowDown";
				suddenDeathText.GetComponent<GUIText>().color = Color.white;
				clockShowdownText.GetComponent<GUIText>().color = Color.yellow;
				CreateTableView();
			}
			
			if(item.name == "SuddenDeathBtn")
			{
				_screenManager.userdata = "SuddenDeath";
				suddenDeathText.GetComponent<GUIText>().color = Color.yellow;
				clockShowdownText.GetComponent<GUIText>().color = Color.white;
				CreateTableView();
			}
		
		}		
	}	
	
	
	void DestroyTableView()
	{
		if(mTableView != null)
			Destroy(mTableView);
		
		if(mCamera != null)
			Destroy(mCamera);
	}
	
	
	void CreateTableView()
	{
		if(mTableView!=null)
			Destroy(mTableView);
		if(titleText.text == "LEADERBOARD" && _LeaderBoardPrefab != null)
		{
			mbIsLeaderBoard = true;
			//if(mTableView == null)
				mTableView = Instantiate(_LeaderBoardPrefab) as GameObject;
			if(_screenManager.userdata == "SuddenDeath")
				mTableView.GetComponent<CScoreTableViewDelegate>()._LeaderboardName = "SuddenDeathLB";
			else
				mTableView.GetComponent<CScoreTableViewDelegate>()._LeaderboardName = "ClockShowDownLB";
			_screenManager.userdata = "";
		}
		else if(_AchievemntBoardPrefab != null)
			mTableView = Instantiate(_AchievemntBoardPrefab) as GameObject;
	}
	
}