﻿using UnityEngine;
using System.Collections;

public class CoinPurchaseScreenManager :  GUIItemsManager
{
	public static CoinPurchaseScreenManager instance;
	BallSelectionSubScreenManager _sBallSelectionSubScreenManager;
	// Use this for initialization
	public GUIText totalCoin;
	GameManager _gamemanager;
	public GUIText _removeAdsText;
	public GUIText _totalCoinsText;
	public bool isTrue ;
	
	bool _isAndroidBack = false;
	
	void Start () 
	{
		instance = this;
		base.Init ();
		
		_sBallSelectionSubScreenManager = GameObject.Find("BallSelectionSubScreen(Clone)").GetComponent<BallSelectionSubScreenManager>();
		_gamemanager = GameManager.GetInstance();
	//	totalCoin.text = _gamemanager.GetCoins().ToString();
		if(_gamemanager.checkingAdd)
		{
			_removeAdsText.text = "REMOVE ADS WITH ANY PURCHASE";
		}
		else
		{
			_removeAdsText.text = "Checkout for more games from Game++";
		}
	}
	
	
	void Update()
	{
				if(_removeAdsText.color.a.Equals(1))
				{
					isTrue = true;
				}
				else if(_removeAdsText.color.a.Equals(0))
				{
					isTrue = false;
				}
				
				if(isTrue)
				{
					Color c = _removeAdsText.color;
					c.a = Mathf.MoveTowards(c.a , 0 , 2 * Time.deltaTime);
					_removeAdsText.color = c;
			
				}
				else
				{
					Color c = _removeAdsText.color;
					c.a = Mathf.MoveTowards(c.a , 1 , 3 * Time.deltaTime);
					_removeAdsText.color = c;
					
				}
		
				if(Input.GetKeyDown(KeyCode.Escape))
				{
					if(_isAndroidBack)
					{
						_screenManager.LoadScreen("BallSelectionPopupScreen");
						_isAndroidBack = false;
					}
			   }
			
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{		
			if(item.name == "BackBG" )
			{
				_screenManager.LoadScreen("BallSelectionPopupScreen");
				  //  subscreenitemmanager.LoadScreen("Empty"); //Empty
			}
			
			if(item.name == "BarBack1")
			{
				_gamemanager.InAppCalling(1);
				
			}
			
			if(item.name == "BarBack2")
			{
				_gamemanager.InAppCalling(2);
			}
			
			if(item.name == "BarBack3")
			{
				_gamemanager.InAppCalling(3);
			}
			
			if(item.name == "BarBack4")
			{
				_gamemanager.InAppCalling(4);
			}
			
			if(item.name == "BarBack5")
			{
				_gamemanager.InAppCalling(5);
			}
			
			if(item.name == "BarBack6")
			{
				_gamemanager.InAppCalling(6);
			}
			
			if(item.name == "GetNowRemoveAdds")
			{
				
			}
			
	}
}
