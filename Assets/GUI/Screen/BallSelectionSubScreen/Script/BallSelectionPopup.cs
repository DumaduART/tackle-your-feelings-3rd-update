﻿using UnityEngine;
using System.Collections;

public class BallSelectionPopup : GUIItemsManager 
{
	public enum POPUPSTATUS
	{
		CoinsAvailable, CoinsNeeded	
	}
	public POPUPSTATUS _ePOPUPSTATUS;
	
	public GUIText Heading, SubHeading, Coins, YesText, CancelText, BallCoinText, BallPackText, SubHeading1,BallHeading,cancelTextWitoutInapp,DollorText,Dollor;
	
	int CoinsHad, CoinsNeeded, BallsToBeAdded, ClickedBallIndex;
	BallSelectionSubScreenManager _sBallSelectionSubScreenManager;
	public GUITexture BallTextureinPopup;
	Transform[] BallObjects;
	public GUITexture yesBtn,CancelBtn,cancelBtnwithoutInapp;
	public string[] _ballHeandings;
	GameManager gameManager;
	public static bool VideoClicked;
	bool _isAndroidBack = false;
	// Use this for initialization
	void Start () 
	{
		gameManager = GameManager.GetInstance();
		GUIManager.instance.Bgtexture.gameObject.SetActive(false);
		base.Init ();
		getTextsneeded ();
		DisableGamplayButton(false);
		
		if(gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
		{
			TimeAttack.instance.ChangeTimerCondition(true);
		}
		
		if(gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
		{
				if(LevelHandler.instance.GetGameOverParameter() == LevelHandler.GameoverParameter.time)
				LevelHandler.instance.ChangeTimerCondition(true);
		}
		
		SetBallHeading(_ballHeandings[ClickedBallIndex-2]);
		
		switch(gameManager.GetGameMode())
		{
			case GameManager.GameMode.ClockShowDown:
			{
				TimeAttackScreenGUIItemsManager.instance._isAndroidBack = false;
				break;
			}
			
			case GameManager.GameMode.SuddenDeath:
			{
				SuddenDeathGUIItemsManager.instance._isAndroidBack = false;
				break;
			}
			
			case GameManager.GameMode.SkillShots:
			{
				if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.ball)
				{
					ArcadeGamepalyGUIItemsManager1.instance._isAndroidBack = false;
				}
			
				else if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.time)
				{
					SkillShotGUIItemsManager.instance._isAndroidBack = false;
				}
				break;
			}
		}
	
	}
	
	void getTextsneeded ()
	{
		_sBallSelectionSubScreenManager = GameObject.Find("BallSelectionSubScreen(Clone)").GetComponent<BallSelectionSubScreenManager>();
		
		BallObjects = new Transform[_sBallSelectionSubScreenManager.transform.childCount];
		
		for(int i = 0; i < _sBallSelectionSubScreenManager.transform.childCount; i++)
		{
			BallObjects[i] = _sBallSelectionSubScreenManager.transform.GetChild(i);
			if(BallObjects[i].GetComponent<GUITexture>() != null)
			BallObjects[i].GetComponent<GUITexture>().enabled = false;
			else 
			BallObjects[i].GetComponent<GUIText>().enabled = false;
		}
		
		CoinsHad = _sBallSelectionSubScreenManager.selectedBallCoinStatus (1);
		CoinsNeeded = _sBallSelectionSubScreenManager.selectedBallCoinStatus (2);
		BallsToBeAdded = _sBallSelectionSubScreenManager.selectedBallCoinStatus (3);
		ClickedBallIndex = _sBallSelectionSubScreenManager.selectedBallCoinStatus (4);
		//print (ClickedBallIndex);
		if(ClickedBallIndex == 1)
			BallTextureinPopup.texture = _sBallSelectionSubScreenManager._cButton1Elements.BallActivatedTexture;
		else if(ClickedBallIndex == 2)
			BallTextureinPopup.texture = _sBallSelectionSubScreenManager._cButton2Elements.BallActivatedTexture;
		else if(ClickedBallIndex == 3)
			BallTextureinPopup.texture = _sBallSelectionSubScreenManager._cButton3Elements.BallActivatedTexture;
		else if(ClickedBallIndex == 4)
			BallTextureinPopup.texture = _sBallSelectionSubScreenManager._cButton4Elements.BallActivatedTexture;
		
		BallCoinText.text = "Cost: "+CoinsNeeded;
		BallPackText.text = "(pack of " +BallsToBeAdded +" Balls)";
//		print (CoinsHad +" "+ CoinsNeeded +" "+ BallsToBeAdded +" "+ ClickedBallIndex);
		
		if(CoinsHad < CoinsNeeded)
			_ePOPUPSTATUS = POPUPSTATUS.CoinsNeeded;	
		else if(CoinsHad >= CoinsNeeded)
			_ePOPUPSTATUS = POPUPSTATUS.CoinsAvailable;
		
		switch (_ePOPUPSTATUS)
		{
			case POPUPSTATUS.CoinsAvailable:
				Heading.text = "You Have Enough Coins";
				SubHeading.text = "Cost: "+CoinsNeeded +"coins";
				Coins.text = "Available: "+CoinsHad +"coins";
				SubHeading1.text = "Buy " +BallsToBeAdded +" Of These?";
				YesText.text = "Buy";
				CancelText.text = "Cancel";
			break;
			
			case POPUPSTATUS.CoinsNeeded:
				
				Heading.text = "Not Enough Coins";
				SubHeading.text = "Cost: "+CoinsNeeded +"coins";
				Coins.text = "Available: "+CoinsHad +"coins";
				
			if(gameManager.isInappNotReq)
				{
					SubHeading1.text = "Play more to get coins.";
					//yesBtn.gameObject.SetActive(false);
					//CancelBtn.gameObject.SetActive(false);
					//YesText.gameObject.SetActive(false);
					//CancelText.gameObject.SetActive(false);
					//cancelBtnwithoutInapp.gameObject.SetActive(true);
					//cancelTextWitoutInapp.gameObject.SetActive(true);
				}
				else
				{
					SubHeading1.text = "Buy More Coins?";
				}
				Coins.transform.GetComponent<GUIText>().color = new Color(1,0,0,1);
				Coins.transform.GetComponent<MenuItemEntryAnim>()._textColor = new Color(1,0,0,1);
				YesText.text = "Store";
				CancelText.text = "Cancel";
			break;
		}
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				_sBallSelectionSubScreenManager.enableScreenClosingPopup ();
				_screenManager.closeScreenManager ();
				_sBallSelectionSubScreenManager.checkRugbyCoinsPublic ();
				for(int i = 0; i < _sBallSelectionSubScreenManager.transform.childCount; i++)
				{
					if(BallObjects[i].GetComponent<GUITexture>() != null)
					BallObjects[i].GetComponent<GUITexture>().enabled = true;
					else 
					BallObjects[i].GetComponent<GUIText>().enabled = true;
				}
				
				DisableGamplayButton(true);
				
				if(gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
				{
					TimeAttack.instance.ChangeTimerCondition(false);
				}
				
				if(gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
						if(LevelHandler.instance.GetGameOverParameter() == LevelHandler.GameoverParameter.time)
						LevelHandler.instance.ChangeTimerCondition(false);
				}
			}
			SetGamePlayAndroidCondition();
		}	
	}
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
			if(item.name == "NoButton")
			{

				_sBallSelectionSubScreenManager.enableScreenClosingPopup ();
				_screenManager.closeScreenManager ();
				_sBallSelectionSubScreenManager.checkRugbyCoinsPublic ();
				for(int i = 0; i < _sBallSelectionSubScreenManager.transform.childCount; i++)
				{
					if(BallObjects[i].GetComponent<GUITexture>() != null)
					BallObjects[i].GetComponent<GUITexture>().enabled = true;
					else 
					BallObjects[i].GetComponent<GUIText>().enabled = true;
				}
				
				DisableGamplayButton(true);
				
				if(gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
				{
					TimeAttack.instance.ChangeTimerCondition(false);
				}
				
				if(gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
						if(LevelHandler.instance.GetGameOverParameter() == LevelHandler.GameoverParameter.time)
						LevelHandler.instance.ChangeTimerCondition(false);
				}
				
				SetGamePlayAndroidCondition();
			}
			
			if(item.name == "CancelWithoutInapp")
			{

				_sBallSelectionSubScreenManager.enableScreenClosingPopup ();
				_screenManager.closeScreenManager ();
				_sBallSelectionSubScreenManager.checkRugbyCoinsPublic ();
				for(int i = 0; i < _sBallSelectionSubScreenManager.transform.childCount; i++)
				{
					if(BallObjects[i].GetComponent<GUITexture>() != null)
					BallObjects[i].GetComponent<GUITexture>().enabled = true;
					else 
					BallObjects[i].GetComponent<GUIText>().enabled = true;
				}
				
				DisableGamplayButton(true);
				
				if(gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
				{
					TimeAttack.instance.ChangeTimerCondition(false);
				}
				
				if(gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
						if(LevelHandler.instance.GetGameOverParameter() == LevelHandler.GameoverParameter.time)
						LevelHandler.instance.ChangeTimerCondition(false);
				}
				
				SetGamePlayAndroidCondition();
			}
			else if(item.name == "Video Button")
			{

			}
			else if(item.name == "YesButton")
			{
				switch (_ePOPUPSTATUS)
				{
					case POPUPSTATUS.CoinsAvailable:
						_sBallSelectionSubScreenManager.ActivateSelectedBall (ClickedBallIndex);
						_sBallSelectionSubScreenManager.enableScreenClosingPopup ();
						_screenManager.closeScreenManager ();
						_sBallSelectionSubScreenManager.checkRugbyCoinsPublic ();
						for(int i = 0; i < _sBallSelectionSubScreenManager.transform.childCount; i++)
						{
							if(BallObjects[i].GetComponent<GUITexture>() != null)
							BallObjects[i].GetComponent<GUITexture>().enabled = true;
							else 
							BallObjects[i].GetComponent<GUIText>().enabled = true;
						}
						DisableGamplayButton(true);
						if(gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
						{
							TimeAttack.instance.ChangeTimerCondition(false);
						}
					
						if(gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
						{
								if(LevelHandler.instance.GetGameOverParameter() == LevelHandler.GameoverParameter.time)
								LevelHandler.instance.ChangeTimerCondition(false);
						}
						BallSelectionManager.instance._ballType[ClickedBallIndex - 1].noOfBallAvailable += BallsToBeAdded;
						SetGamePlayAndroidCondition();
					
					break;
					
					case POPUPSTATUS.CoinsNeeded:
						# if UNITY_WEBPLAYER
					
						# else
						_screenManager.prevPageBeforeCoinPurchage = "BallSelectionPopupScreen";
						_screenManager.LoadScreen ("CoinPurchase");
						_sBallSelectionSubScreenManager.checkRugbyCoinsPublic ();
						#endif
					break;
				}
			}
			
		}	
	}
	void ShowVideoAds()
	{
				VideoClicked = true;
			
					_sBallSelectionSubScreenManager.ActivateSelectedBall (ClickedBallIndex);
					_sBallSelectionSubScreenManager.enableScreenClosingPopup ();
					_screenManager.closeScreenManager ();
					_sBallSelectionSubScreenManager.checkRugbyCoinsPublic ();
					for(int i = 0; i < _sBallSelectionSubScreenManager.transform.childCount; i++)
					{
						if(BallObjects[i].GetComponent<GUITexture>() != null)
							BallObjects[i].GetComponent<GUITexture>().enabled = true;
						else 
							BallObjects[i].GetComponent<GUIText>().enabled = true;
					}
					DisableGamplayButton(true);
					if(gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
					{
						TimeAttack.instance.ChangeTimerCondition(false);
					}

					if(gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
					{
						if(LevelHandler.instance.GetGameOverParameter() == LevelHandler.GameoverParameter.time)
							LevelHandler.instance.ChangeTimerCondition(false);
					}
					BallSelectionManager.instance._ballType[ClickedBallIndex - 1].noOfBallAvailable += BallsToBeAdded;
					SetGamePlayAndroidCondition();
	}
	public void DisableGamplayButton(bool isEnable)
	{
		if(isEnable)
		{
			InputManager.GetInstance().enabled = true;
			GameObject.Find("ScreenManager").GetComponent<ScreenManager>().EnableInput();	
		}
		else
		{
			GameObject.Find("ScreenManager").GetComponent<ScreenManager>().DisableInput();
			InputManager.GetInstance().enabled = false;
		}
	}
    
	public void SetBallHeading(string mBallHeading)
	{
		BallHeading.text = mBallHeading;
		
		
		
	}
	
	public void SetGamePlayAndroidCondition()
	{
		switch(gameManager.GetGameMode())
		{
			case GameManager.GameMode.ClockShowDown:
			{
				TimeAttackScreenGUIItemsManager.instance._isAndroidBack = true;
				break;
			}
			
			case GameManager.GameMode.SuddenDeath:
			{
				SuddenDeathGUIItemsManager.instance._isAndroidBack = true;
				break;
			}
			
			case GameManager.GameMode.SkillShots:
			{
				if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.ball)
				{
					ArcadeGamepalyGUIItemsManager1.instance._isAndroidBack = true;
				}
			
				else if(LevelHandler.instance._gameOverParameter == LevelHandler.GameoverParameter.time)
				{
					SkillShotGUIItemsManager.instance._isAndroidBack = true;
				}
				break;
			}
		}
	}

}