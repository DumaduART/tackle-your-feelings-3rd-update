using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class MainScreenGUIItemsManager : GUIItemsManager
{
	public GameObject ModeButton;
	public GameObject MainBackGround;	
	public static MainScreenGUIItemsManager instance;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	public GameObject _ImageField1,_TableviewCam;
	GameObject mImageField1,mImageField2,mTableviewCam;
	public GUITexture removeads;
	public GUIText removeAdstext;
	public GUITexture removeadsValue;
	public GUIItemButton TrainigBtn, ClockBtn, SkillBnt, SDeath;
	private GameManager _gameManager;
	public GameObject _LeatherBoard, BackToModes;
	public GameObject _Achievements;
	public GameObject[] _Store;
	public GameObject _MoreGame;
	public GameObject _loginButton;

	public GameObject BackToMemuBtn,BackToMenuTxt,StoreBtn,StoreImage,StoreText,Training,ClockShowDown,SkillShots,SuddenDeath,AreuSurePopUp;
	public GameObject _logoutButton;
	string debugStr="" ;
	bool tutorial_level = false;
	int miTopScorerCall = 0;
	bool mbStillInMainMenu;
	public bool _isAndroidBack = false;

	public GUITexture _PlayBtn;
	public Texture2D _PlayTexture;
	public GameObject   _BackButton;
	public GameObject[] _Exit;
	public GameObject _PlayBtnMspo,_PlayNormal;
	void Awake()
	{
		instance = this;

		if(BackToMemuBtn != null)
			BackToMemuBtn.SetActive(false);
		if(BackToMenuTxt != null)
			BackToMenuTxt.SetActive(false);
		if(GameManager.GetInstance().isFacebookReq)
		{
		
			//#if UNITY_STANDALONE_OSX || UNITY_WP8 || UNITY_WEBPLAYER
			_loginButton.SetActive(true);
			_logoutButton.SetActive(true);
			//#endif
		}
		else
		{
			//#if UNITY_STANDALONE_OSX || UNITY_WP8 || UNITY_WEBPLAYER
			_loginButton.SetActive(false);
			_logoutButton.SetActive(false);
			//#endif
		}
		//#if UNITY_STANDALONE_OSX || UNITY_WEBPLAYER
//		if(GameManager.GetInstance().isRemoveAddReq)
//		{
//			removeads.gameObject.SetActive(true);
//			removeAdstext.gameObject.SetActive(true);
//			removeadsValue.gameObject.SetActive(true);
//		}
//		else
//		{
//			removeads.gameObject.SetActive(false);
//			removeAdstext.gameObject.SetActive(false);
//			removeadsValue.gameObject.SetActive(false);
//		}
		//#endif
		if (Application.loadedLevel == 3)
		{
			_PlayBtnMspo.SetActive(true);
			_PlayNormal.SetActive(false);
		}
		if (Application.loadedLevel == 2)
		{
			_PlayBtnMspo.SetActive(false);
			_PlayNormal.SetActive(true);
		}
	}
	void Start ()
	{		
		mbStillInMainMenu = true;
		Application.targetFrameRate = 50;
		base.Init ();	
		miTopScorerCall = 0;
		_gameManager = GameObject.Find ("GameManager").GetComponent ("GameManager") as GameManager;
		subscreenitemmanager = GameObject.Find ("ShopSubScreenManager").GetComponent<ScreenManager> ();
		if (GameManager.GetInstance ().isFacebookReq) {
			//#if (!UNITY_STANDALONE_OSX  && !UNITY_WP8  && !UNITY_WEBPLAYER)
			CheckLoginStatus ();
			//#else
		}
//		if(iPhone.generation == iPhoneGeneration.iPad3Gen || iPhone.generation == iPhoneGeneration.iPhone5 || iPhone.generation == iPhoneGeneration.iPodTouch5Gen)
//		{
//			if(!_gameManager._callLogin && !OnlineSocialHandler.Instance.GetIsLoggedIn())
//			{
//				OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGIN,"",CallBack);
//				_gameManager._callLogin = true;
//			}
//		}
		
//		#if UNITY_ANDROID
//			if(!_gameManager._callLogin && !OnlineSocialHandler.Instance.GetIsLoggedIn() && PlayerPrefs.GetInt("LoggedIn") == 1)
//			{
//				OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGIN,"",CallBack);
//				_gameManager._callLogin = true;
//			}
//		#endif
		
		
		if (GameObject.Find ("BallSelectionSubScreenManager(Clone)"))
			GameObject.Find ("BallSelectionSubScreenManager(Clone)").GetComponent<ScreenManager> ().closeScreenManager ();

			if (!_gameManager.checkingAdd) {
//				removeads.gameObject.SetActive (false);
//				removeAdstext.gameObject.SetActive (false);
//				removeadsValue.gameObject.SetActive (false);
			}

		else {
//			removeads.gameObject.SetActive (false);
//			removeAdstext.gameObject.SetActive (false);
//			removeadsValue.gameObject.SetActive (false);
			
		}

		CheckGameScene ();
		CheckMsportCondition ();



        if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
		{
			Invoke("BackButttonEnable",2f);
			Invoke("DisableLoadingIndicator",10f);
		}
		else
		{
			//_BallCount = mGameManager._BallNo;
			//PlayerPrefs.SetInt("BallCountMSposrts",_BallCount);
		}
        if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
		{
			Invoke("PlayButtonDisable",0.03f);

			//#if UNITY_IPHONE
			//if(MSportsConstants._MSportsMenuLoadCount != 0)
			//{
			//Invoke("PlayButtonEnable",0.04f);
			////_PlayBtn.texture = _PlayTexture;
			//}
			//#elif UNITY_ANDROID
			//Invoke("PlayButtonEnable",0.04f);
		
			//#endif




		}
		
	}


	public void PlayButtonEnable()
	{
		ClockShowDown.SetActive (true);
		//SuddenDeath.SetActive (true);
		ClockShowDown.GetComponent<GUITexture>().enabled = true;

		ClockShowDown.GetComponent<GUIItemButton>().enabled = true;
		//SuddenDeath.GetComponent<GUITexture>().enabled = true;
	//	SuddenDeath.GetComponent<GUIItemButton>().enabled = true;
		//_PlayBtn.texture = _PlayTexture;
	}
	public void PlayButtonDisable()
	{
		ClockShowDown.gameObject.SetActive(false);
		ClockShowDown.GetComponent<GUITexture>().enabled = false;

	//	SuddenDeath.gameObject.SetActive(false);
	//	SuddenDeath.GetComponent<GUITexture>().enabled = false;
	}
	public void BackButttonEnable()
	{
		_BackButton.GetComponent<GUIItemButton>().enabled = true;
	}
	public void EnableObject(bool pValue)
	{
		for(int i=0; i < _Exit.Length; i++)
		{
			_Exit[i].SetActive(pValue);
		}
		_BackButton.SetActive(!pValue);
	}




	void CheckGameScene()
	{
		if (Application.loadedLevel == 1)
			GameSceneHandler.Instance._eGameStates = GameSceneHandler.eGameStates.None;
		else if (Application.loadedLevel == 2)
			GameSceneHandler.Instance._eGameStates = GameSceneHandler.eGameStates.NormalMode;
		else if (Application.loadedLevel == 3)
            GameSceneHandler.Instance._eGameStates = GameSceneHandler.eGameStates.Sports_Mode;

	}

	void CheckMsportCondition()
	{
		if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.None) {
			ClockBtn.gameObject.SetActive (false);
			SDeath.gameObject.SetActive (false);
			BackToModes.SetActive (false);
		} 
		else if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
			ModeButton.SetActive (false);
            _LeatherBoard.SetActive (false);
            _Achievements.SetActive (false);
            StoreBtn.SetActive (false);
			_MoreGame.SetActive (false);
			StoreImage.SetActive (false);
			StoreText.SetActive (false);
			BackToModes.SetActive (false);
			_loginButton.SetActive (false);
			_logoutButton.SetActive (false);
		}

        else if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode) {

//			ClockShowDown.SetActive (false);
			SuddenDeath.SetActive (false);
			TrainigBtn.gameObject.SetActive (false);
			SkillBnt.gameObject.SetActive (false);
            _LeatherBoard.SetActive (false);
            _Achievements.SetActive (false);
            StoreBtn.SetActive (false);
			_MoreGame.SetActive (false);
			StoreImage.SetActive (false);
			StoreText.SetActive (false);
			BackToModes.SetActive (false);
			_loginButton.SetActive (false);
			_logoutButton.SetActive (false);
		}
	}




	public void ShowNo1List()
	{
		if(mImageField1 != null)
		{
			Destroy(mImageField1);
			mImageField1=null;
		}
		if(mImageField2 != null)
		{
			Destroy(mImageField2);
			mImageField2=null;
		}
		miTopScorerCall = 0;
		//Debug.Log("ShowNo1List");
		//OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_RANKER,"1",CallBack,"TimeScore");
		//Debug.Log("==Inmainmenu==");
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
            Application.Quit();
	    }
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	

	public enum eBackButtonStates
	{
		None,
		MainMenu,
		Arcade,
		QuitPlace
	};

	public eBackButtonStates _eBackButtonStates;

    public void TrainingBtnClick()
    {
        if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.None)
            Application.LoadLevel(2);
        else
        {
            if (PlayerPrefs.GetString("tuetorial") == "tuetorialoff")
                _screenManager.LoadScreen("Empty");
            else
                _screenManager.LoadScreen("Loading");
            _screenManager.prevPageBeforeSelection = "MainMenu";
            _screenManager.userdata = "TM";
        }
    }
    public void ClockShowDownClick()
    {
        if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
            MSportsHandler.Instance.mSportsGameStarted();
        _screenManager.prevPageBeforeSelection = "MainMenu";
        _screenManager.userdata = "CM";
        _gameManager.SetGameMode(GameManager.GameMode.ClockShowDown);
        if (_gameManager.isFacebookReq)
        {
            _gameManager.FaceBookSubScrren.LoadScreen("FaceBookPopUp");
            TableviewCamEnable(false);
        }
        else
        {
            if (PlayerPrefs.GetString("tuetorial") == "tuetorialoff")
                _screenManager.LoadScreen("Empty");
            else
                _screenManager.LoadScreen("Loading");
        }
    }
    public void SkillShotClick()
    {
        _screenManager.userdata = "";
        DestroyImageFb();
        _gameManager.SetGameMode(GameManager.GameMode.SkillShots);
        _gameManager.LoadGamePrefab();
        _screenManager.LoadScreen("SkillShotScreen1");
        _gameManager.SetCategory(GameManager.Category.ThroughRings);
    }
    public void SuddenDeathClick()
    {
        _screenManager.prevPageBeforeSelection = "MainMenu";
        _screenManager.userdata = "SM";
        _gameManager.SetGameMode(GameManager.GameMode.SuddenDeath);
        if (_gameManager.isFacebookReq)
        {
            _gameManager.FaceBookSubScrren.LoadScreen("FaceBookPopUp");
            TableviewCamEnable(false);
        }

        else
        {
            if (PlayerPrefs.GetString("tuetorial") == "tuetorialoff")
                _screenManager.LoadScreen("Empty");
            else
                _screenManager.LoadScreen("Loading");
        }
    }
    public void SettingsBtnClick()
    {
        DestroyImageFb();
        _screenManager.LoadScreen("Setting");
    }
    public void StatesButtonClick()
    {
        DestroyImageFb();
        _screenManager.LoadScreen("StatsScreen");
        subscreenitemmanager.defaultScreen = "StatsDescription";
    }
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);

			if(item.name == "Trainingbtn")
			{
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.None)
					Application.LoadLevel (2);
				else {
					if (PlayerPrefs.GetString ("tuetorial") == "tuetorialoff")
						_screenManager.LoadScreen ("Empty");
					else
						_screenManager.LoadScreen ("Loading");
					_screenManager.prevPageBeforeSelection = "MainMenu";
					_screenManager.userdata = "TM";
				}
				
				
			}

			if(item.name == "Tournament")
			{
					Application.LoadLevel (3);
			}
			
			if(item.name == "ClockShowDownbtn")
			{
                if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
				MSportsHandler.Instance.mSportsGameStarted();
				_screenManager.prevPageBeforeSelection = "MainMenu";
				_screenManager.userdata = "CM";
				_gameManager.SetGameMode(GameManager.GameMode.ClockShowDown);
				if(_gameManager.isFacebookReq)
				{
					_gameManager.FaceBookSubScrren.LoadScreen("FaceBookPopUp");
					TableviewCamEnable(false);
				}
				
				else
				{
					if(PlayerPrefs.GetString("tuetorial")=="tuetorialoff")
						_screenManager.LoadScreen("Empty");
					else
					_screenManager.LoadScreen("Loading");
				}
			}
			
			if(item.name == "ClockShowDownbtnN")
			{
                if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
					MSportsHandler.Instance.mSportsGameStarted();
				_screenManager.prevPageBeforeSelection = "MainMenu";
				_screenManager.userdata = "CM";
				_gameManager.SetGameMode(GameManager.GameMode.ClockShowDown);
				if(_gameManager.isFacebookReq)
				{
					_gameManager.FaceBookSubScrren.LoadScreen("FaceBookPopUp");
					TableviewCamEnable(false);
				}
				
				else
				{
					if(PlayerPrefs.GetString("tuetorial")=="tuetorialoff")
						_screenManager.LoadScreen("Empty");
					else
						_screenManager.LoadScreen("Loading");
				}
			}
			if(item.name == "SkillShotbtn")
			{
					_screenManager.userdata = "";
					DestroyImageFb ();
					_gameManager.SetGameMode (GameManager.GameMode.SkillShots);
					_gameManager.LoadGamePrefab ();
					_screenManager.LoadScreen ("SkillShotScreen1");
					_gameManager.SetCategory (GameManager.Category.ThroughRings);
            }

			if(item.name == "Arcade")
			{
					Application.LoadLevel (2);
			}
			
			if(item.name == "SuddenDeathbtn")
			{
				_screenManager.prevPageBeforeSelection = "MainMenu";
				_screenManager.userdata = "SM";
				_gameManager.SetGameMode(GameManager.GameMode.SuddenDeath);
				if(_gameManager.isFacebookReq)
				{
					_gameManager.FaceBookSubScrren.LoadScreen("FaceBookPopUp");
					TableviewCamEnable(false);
				}
				
				else
				{
					if(PlayerPrefs.GetString("tuetorial")=="tuetorialoff")
						_screenManager.LoadScreen("Empty");
					else
					_screenManager.LoadScreen("Loading");
				}
			}
			
			if(item.name == "Settingbtn")
			{
				DestroyImageFb();
			_screenManager.LoadScreen("Setting");
			}
			
			if(item.name == "Storebtn")
			{
				DestroyImageFb();
				_screenManager.userdata = "MainMenu";
				 _screenManager.LoadScreen("ShopScreen");
				subscreenitemmanager.defaultScreen = "BallSelectionScreen";
	//			 GameObject subs =	Instantiate(subscreenmanager) as GameObject;
	//			 subs.GetComponent<ScreenManager>().LoadScreen("Ball");
			}
			
			if(item.name == "Statsbtn")
			{
				DestroyImageFb();
				 _screenManager.LoadScreen("StatsScreen");
				subscreenitemmanager.defaultScreen = "StatsDescription";
			}	
			
			if(item.name == "FaceBook_Login"){
				
				//Debug.Log("Login Clicked");
				OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGIN,"",CallBack);
			}
			
			if(item.name == "FaceBook_Logout"){
				
				OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGOUT,"",CallBack);
			}

			if(item.name == "BackToModes"){

				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode)
					Application.LoadLevel (1);
                else if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode) 
				{
					
					Instantiate (AreuSurePopUp);
					ClockShowDown.SetActive (false);
					SuddenDeath.SetActive (false);
					ModeButton.SetActive (false);

				}
			}
			if(item.name == "ModeButton"){
				ClockShowDown.SetActive (true);
			//	SuddenDeath.SetActive (true);


			}

			if(item.name == "LeaderBoard")
			{
				if(_gameManager.isNativeCallReq)
                {
                    
                }
				else
				{
					_gameManager.LocalLeatherBord = "LEADERBOARD";
					_screenManager.LoadScreen("LocalLeatherBoard");
					
				}
			}
			
			if(item.name == "AchievementsButton")
			{
				if(_gameManager.isNativeCallReq)
                {
                    
                }
				else
				{
					_gameManager.LocalLeatherBord = "ACHIEVEMENT";
					_screenManager.LoadScreen("ModeSelctionForLocalLeatherBoard");
					
				}
			}
			
			if(item.name == "MoreGamesbtn")
			{
//				print ("MoreGamesbtn");
				if(_gameManager.isNativeCallReq)
                {
                    
                }
				else
					Application.OpenURL ("http://www.dumadu.com/");
				//for updating score in gamecenter
				
				//for updating achievement in gamecenter
			}
			
			if(item.name == "RemoveAdsbtn")
			{
                _gameManager.Receiver(7);
			}
			if(item.name == "BackToMenuBtn")
			{
                Application.Quit();
			}
		}
	}	
	
	void CallBack(string strResult ,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null)
	{
		#if (!UNITY_STANDALONE_OSX )
		if(!mbStillInMainMenu)
		{
			DestroyImageFb();
			return;
		}
		//Debug.Log("===Login CallBack===");		
		if(eRequestType == eONLINE_REQ_TYPE.LOGIN)
		{
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.UPDATE_VALUE_TO_SERVER,"0",null,"TimeScore");//TimeScore , SuddenDeathScore
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.UPDATE_VALUE_TO_SERVER,"0",null,"SuddenDeathScore");			
			
			_loginButton.SetActive(false);
			_logoutButton.SetActive(true);
			
			//Debug.Log("<----Login Successfull---->");
			ShowNo1List();
			//Change Login/Logout Image
		}
		else if(eRequestType == eONLINE_REQ_TYPE.LOGOUT)
		{
			//Change Login/Logout Image
			
			_loginButton.SetActive(true);
			_logoutButton.SetActive(false);
			
			//Debug.Log("<----Logout Successfull---->");
		}
		else if(eRequestType == eONLINE_REQ_TYPE.LOAD_USER_DETAILS)
		{
			ShowNo1List();
		}
		else if(miTopScorerCall == 0 && eRequestType == eONLINE_REQ_TYPE.GET_TOP_RANKER)
		{
			miTopScorerCall +=1;
			if(data.Count>0)
				SetTimeScoreImage(data[0]._UserImageURL);
			//else
				//Debug.Log("TimeScore No top ranker");
			//OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_RANKER,"1",CallBack,"SuddenDeathScore");
			
		}
		else if(miTopScorerCall == 1 && eRequestType == eONLINE_REQ_TYPE.GET_TOP_RANKER)
		{
			miTopScorerCall += 1;
			if(data.Count>0)
				SetSuddenDeathScoreImage(data[0]._UserImageURL);
			//else
				//Debug.Log("SuddenDeathScore No top ranker");
		}
		else if(eRequestType == eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION)
		{
			// connect to net first popup
		}
		#endif
	}
	void SetTimeScoreImage(string strUrl)
	{
		
		if(!mbStillInMainMenu)
		{
			DestroyImageFb();
			return;
		}
		mImageField1 = Instantiate(_ImageField1) as GameObject;
		mImageField1.transform.position= new Vector3(1.75f,1.67f,0);
		StartCoroutine(LoadImageFromURL(strUrl,mImageField1));
	}
	void SetSuddenDeathScoreImage(string strUrl)
	{
		if(!mbStillInMainMenu)
		{
			DestroyImageFb();
			return;
		}
		mImageField2 = Instantiate(_ImageField1) as GameObject;
		mImageField2.transform.position= new Vector3(1.75f,-0.23f,0);
		StartCoroutine(LoadImageFromURL(strUrl,mImageField2));
	}
	IEnumerator LoadImageFromURL(string url, GameObject goImage)
	{
			
			//Debug.Log("initializeWithJSONString=="+url);
			WWW www = new WWW(url);
			yield return www;
			if(www.error == null)			
			{
				//Debug.Log("initializeWithJSONString22");
				Texture2D tex = new Texture2D(256, 256, TextureFormat.RGB24, false);
				//Debug.Log("Got texture");
				try
				{
					 www.LoadImageIntoTexture(tex);
					//Texture2D tex=www.texture;
					//tex.Compress(true);
					tex.wrapMode = TextureWrapMode.Clamp;
					tex.anisoLevel = 9;
					tex.filterMode = FilterMode.Bilinear;
					
					goImage.GetComponent<Renderer>().material.mainTexture = tex;
				}
				catch(Exception e)
				{
					//Debug.Log(e.ToString());
				}	
			
			}
			else
			{
				//Debug.Log("initializeWithJSONString33"+www.error);
			}	
		
			
		
	}
	
	public void TableviewCamEnable(bool val)
	{
		if(mTableviewCam != null)
			mTableviewCam.SetActive(val);
	}
	
	
	public void CheckLoginStatus()
	{
		#if (!UNITY_STANDALONE_OSX  && !UNITY_WP8 )
		//Debug.Log("I am entering here");
//		SetTimeScoreImage("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash1/371729_100002300375711_500081994_n.jpg");
//		SetSuddenDeathScoreImage("https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash1/371729_100002300375711_500081994_n.jpg");
		if(mTableviewCam == null)
		{
			mTableviewCam = Instantiate(_TableviewCam) as GameObject;
		}
//		if(OnlineSocialHandler.Instance.GetIsLoggedIn())
//		{
//			debugStr = "Fb.loggedIn true";
//			if(OnlineSocialHandler.Instance.GetIsLoginDetails())
//			{
//				Debug.Log("<----Alredey LoggedIn---->");
//				debugStr = "Fb.LoginDetails true";
//				_loginButton.SetActive(false);
//				_logoutButton.SetActive(true);
//				ShowNo1List();
//			}
//			else
//			{
//				debugStr = "Fb.LoginDetails false";
//				Debug.Log("<----Alredey LoggedIn----> But no Details==");
//				OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOAD_USER_DETAILS,"",CallBack);
//			}
//		}
//		else
//		{
//			debugStr = "Fb.loggedIn false";
////			Debug.Log("<----Not LoggedIn----> ");
//			_loginButton.SetActive(true);
//			_logoutButton.SetActive(false);
//		}
	#endif
	}
	
//	void OnGUI()
//	{
//		GUI.contentColor = Color.white;
//		GUI.Label(new Rect(10,10,500,200),debugStr);
//	}
	void DestroyImageFb()
	{
		mbStillInMainMenu = false;
		if(mTableviewCam!=null)
		{
			Destroy(mTableviewCam);
			mTableviewCam=null;
		}
		if(mImageField1 != null)
		{
			Destroy(mImageField1);
			mImageField1=null;
		}
		if(mImageField2 != null)
		{
			Destroy(mImageField2);
			mImageField2=null;
		}
	}
	void OnDestroy()
	{
		mbStillInMainMenu = false;
	}
	

	public override void OnExitAnimationCompleted()
	{
		if(PlayerPrefs.GetString("tuetorial")=="tuetorialoff")
		{
			switch(_screenManager.userdata)
			{
				case "TM":
					_gameManager.SetGameMode(GameManager.GameMode.Training);
					EventManager.GameStartTrigger();
					SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
					_screenManager.userdata = "";
					break;
				case "CM":
					//print ("brajesh");
					_gameManager.SetGameMode(GameManager.GameMode.ClockShowDown);
					EventManager.GameStartTrigger();
					SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
					_screenManager.userdata = "";
					break;
			    case "SM":
					_gameManager.SetGameMode(GameManager.GameMode.SuddenDeath);
					EventManager.GameStartTrigger();
					SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
					_screenManager.userdata = "";
					break;
			}
		}
		
		else
		{
			switch(_screenManager.userdata)
			{
				case "TM":
					_gameManager.SetGameMode(GameManager.GameMode.Training);
					_screenManager.userdata = "";
					break;
				case "CM":
					_gameManager.SetGameMode(GameManager.GameMode.ClockShowDown);
					_screenManager.userdata = "";
					break;
			    case "SM":
					_gameManager.SetGameMode(GameManager.GameMode.SuddenDeath);
					_screenManager.userdata = "";
					break;
			}
		}
		DestroyImageFb();
		
	}
	
	public void SetData()
	{
		_screenManager.userdata = "";
		_gameManager.SetGameMode(GameManager.GameMode.None);
	}
}