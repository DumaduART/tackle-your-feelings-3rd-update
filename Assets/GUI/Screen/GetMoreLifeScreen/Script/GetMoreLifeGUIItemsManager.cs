using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FBKit;

public class GetMoreLifeGUIItemsManager : GUIItemsManager
{
	public GameObject _TableView,_LoadingText;
	GameObject mTableview = null;

	
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	public GameObject _GetCoinBtn;
	bool tutorial_level = false;
	public GUITexture Addcoin;
	private ScoreManager _scoreManager;
	public static bool mbGameOverScreen;
	public GameObject[] _GetNow;
	public GameObject[] _disableForCustom;
	public GameObject[] _PositionsChangeForCustom;

	// Use this for initialization
	void Awake()
	{
		if(GameManager.GetInstance().isFacebookReq)
		{
			_LoadingText.gameObject.SetActive(true);
		}
		
		else
		{
			//#if (UNITY_STANDALONE_OSX  || UNITY_WP8 || UNITY_WEBPLAYER )
			_LoadingText.gameObject.SetActive(false);
			//#endif
		}
	}
	void Start ()
	{
		GUIManager.instance.Bgtexture.gameObject.SetActive(false);
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager;
		_scoreManager =  GameObject.Find("GameManager").GetComponent("ScoreManager") as ScoreManager;
		//Edited By Ankit
		//_scoreManager.TokensCalculation();
		_LoadingText.GetComponent<GUIText>().text = "Loading friends data...";
		mbGameOverScreen = true;
		
		
		
		if(GameObject.Find("BallSelectionPopupScreenManager(Clone)") != null)
			GameObject.Find("BallSelectionPopupScreenManager(Clone)").GetComponent<ScreenManager>().closeScreenManager ();
		
//		if(_inputManager.GetRefOfBall() != null)
//		{
//			_inputManager.GetRefOfBall().transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
//		}
		//if(_gameManager.isInappNotReq)
		//	Addcoin.gameObject.SetActive(false);
	}

	void Update()
	{
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			mbGameOverScreen = false;
			EventManager.GameOverTrigger();
		}
	}


	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	void CallBack(string strResult ,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null)
	{
		if(!mbGameOverScreen)
		{
			if(mTableview != null)
			{
				Destroy(mTableview);
				mTableview = null;
			}
			return;
		}
		if(eRequestType == eONLINE_REQ_TYPE.GET_TOP_RANKING_LIST || eRequestType == eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST)
		{
			//Debug.Log("==Create Table View==");
			if(mTableview != null)
			{
				Destroy(mTableview);
				mTableview = null;
			}
			if(mTableview == null)
				mTableview =(GameObject)Instantiate(_TableView);
			JSONObject tJSONObj = new JSONObject();
			tJSONObj.type = JSONObject.Type.ARRAY;
			for(int i=0; i<data.Count;i++)
			{
				JSONObject tJSONObj2 = new JSONObject();
				tJSONObj2.AddField("Name",data[i]._UserName);
				tJSONObj2.AddField("Image",(data[i]._UserImageURL).ToString());
				tJSONObj2.AddField("UserId",data[i]._UserId);
				tJSONObj2.AddField("Score",data[i]._UserScore.ToString());
				tJSONObj2.AddField("Rank",data[i]._UserRank.ToString());
				tJSONObj2.AddField("Installed",true);
				tJSONObj.list.Add(tJSONObj2);
			}
			mTableview.GetComponent<FBTableViewDelegate>().FillTableViewWithData(tJSONObj.print(),0);
			_LoadingText.GetComponent<GUIText>().text = "";
			
		}
		else if(eRequestType == eONLINE_REQ_TYPE.UPDATE_VALUE_TO_SERVER)
		{
			//OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK,"",CallBack,"Score");
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST,"5",CallBack,"SuddenDeathScore");
		}
		if(eRequestType == eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION)
		{
			_LoadingText.GetComponent<GUIText>().text = "Connect to Facebook...";
		}
//		if(eRequestType == eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK)
//		{
//			//Show the strResult in the rank text field....
//		}
	}
	public override void OnEntryAnimationCompleted ()
	{
		OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.UPDATE_VALUE_TO_SERVER,_scoreManager.currentScore.ToString(),CallBack,"SuddenDeathScore");	
	}
	void OnDestroy()
	{
		if(mTableview != null)
		{
			Destroy(mTableview);
			mTableview = null;
		}
	}

	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
			
			if(item.name == "GetCoin")
			{
				
				# if UNITY_WEBPLAYER
					
				# else
				mbGameOverScreen = false;
				_screenManager.LoadScreen("CoinPurchase");
				_screenManager.prevPageBeforeCoinPurchage = "GetMoreLifeScreen";
					
				#endif
			}
			if(item.name == "GetNowVideo")
			{

			}
			if(item.name == "GetNowBG1")
			{
				mbGameOverScreen = false;
				SuddenDeathGUIItemsManager._ReviveSuddenDeath = true;
                if (_gameManager.GetCoins() >= _gameManager._moreLife[0]._lifeCost)
                {
                    _screenManager.LoadScreen("SuddenDeathGamePlay");
                    SuddenDeath.instance.IncreaseBall(_gameManager._moreLife[0]._noOfLife);
                    _inputManager.enabled = true;
                    _gameManager.AddCoin(-_gameManager._moreLife[0]._lifeCost);
                }
//					if(_inputManager.GetRefOfBall() != null)
//					_inputManager.GetRefOfBall().transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				
				else
				{
					if(_gameManager.isInappNotReq)
					{
						PopUpManager.instance.CreatePopUp(PopUpType.notenoughCoins);
						return;
					}
					# if UNITY_WEBPLAYER
					
					# else
					_screenManager.LoadScreen("CoinPurchase");
					_screenManager.prevPageBeforeCoinPurchage = "GetMoreLifeScreen";
					#endif
				}
			}
			
			if(item.name == "GetNowBG2")
			{
				mbGameOverScreen = false;
				if(_gameManager.GetCoins()>= _gameManager._moreLife[1]._lifeCost )
				{
					_screenManager.LoadScreen("SuddenDeathGamePlay");
					SuddenDeath.instance.IncreaseBall(_gameManager._moreLife[1]._noOfLife);
					_inputManager.enabled = true;
					_gameManager.AddCoin(-_gameManager._moreLife[1]._lifeCost);
					
//					if(_inputManager.GetRefOfBall() != null)
//					_inputManager.GetRefOfBall().transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				}
				
				else
				{
					if(_gameManager.isInappNotReq)
					{
						PopUpManager.instance.CreatePopUp(PopUpType.notenoughCoins);
						return;
					}
					# if UNITY_WEBPLAYER
					
					# else
					_screenManager.LoadScreen("CoinPurchase");
					_screenManager.prevPageBeforeCoinPurchage = "GetMoreLifeScreen";
					#endif
				}
			}
			
			if(item.name == "NoThankBt")
			{
				mbGameOverScreen = false;
				EventManager.GameOverTrigger();
				//_gameManager.CallGameOverScreen();
			}
		
		}
		
		
		
	}	
	void ShowVideoAds()
	{
				mbGameOverScreen = false;
				SuddenDeathGUIItemsManager._ReviveSuddenDeath = true;

					_screenManager.LoadScreen("SuddenDeathGamePlay");
					SuddenDeath.instance.IncreaseBall(_gameManager._moreLife[0]._noOfLife);
					_inputManager.enabled = true;

	}
}