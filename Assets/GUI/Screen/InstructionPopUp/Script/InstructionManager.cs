﻿using UnityEngine;
using System.Collections;

public class InstructionManager : MonoBehaviour {

	// Use this for initialization
	static InstructionManager instance;
	public ScreenManager InstructionSubscreen;
	
	public enum ImotionType
	{
		happy,sad,angry
	}
	
	Texture2D AvatarTexture;
	public Texture2D sadImage;
	public Texture2D happyImage;
	public Texture2D angryImage;
	string content;
	
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void GenerateInstruction(ImotionType imotionType , string displayText, bool isForce)
	{
		if(PlayerPrefs.GetString("instructor") == "instructoroff")
			return ;
			
		switch (imotionType)
		{
			case ImotionType.happy:
			{
				AvatarTexture = happyImage;
				break;
			}
			
			case ImotionType.sad:
			{
				AvatarTexture = sadImage;
				break;
			}
			
			case ImotionType.angry:
			{
				AvatarTexture = angryImage;
				break;
			}
		}
		content = displayText;
		InstructionSubscreen.LoadScreen("Instruction");	
	}
	
	public string GetContent()
	{
		return(content);
	}
	
	public Texture2D GetAvatarImage()
	{
		return(AvatarTexture);
	}
	
	public static InstructionManager GetInstance()
	{
		return(instance);
	}
}
