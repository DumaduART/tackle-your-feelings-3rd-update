using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InstructionScreen : GUIItemsManager
{
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	InstructionManager instructionManager;

	bool tutorial_level = false;
	
	
	public GUITexture _AvatarImage;
    public Text _ContentTxt;
	public int firstLineLenth;
	
	public bool updateText;
	
	InputManager inputmanager;
	
	// Use this for initialization
	void Start ()
	{
		
		string content1 = "",content2 = "";
		base.Init();	
		instructionManager = InstructionManager.GetInstance();
		if(updateText){
		_AvatarImage.texture = instructionManager.GetAvatarImage();
		
		  _ContentTxt.text = instructionManager.GetContent();//content1+"\n"+ content2;
		}
		inputmanager = GameObject.Find("GameManager").GetComponent<InputManager>();

        Invoke("managerExitCallBack",2.8f);
	}
	void Update ()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if(inputmanager.enabled == true)
			{
				foreach(Transform t in this.gameObject.transform)
				{
					t.gameObject.layer = 13;
				}
			}
		}
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{			
		
		}	
	}	
	
	public override void OnEntryAnimationCompleted()
	{
		this.State = eGUI_ITEMS_MANAGER_STATE.Exiting;
	}
	
	void managerExitCallBack()
	{
		Destroy(this.gameObject);
	}
	
}