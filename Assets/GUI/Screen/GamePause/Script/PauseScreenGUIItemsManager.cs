using UnityEngine;
using System.Collections;

public class PauseScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	
	bool tutorial_level = false;
	
	
	bool _isAndroidBack = false;
	public GameObject _Resume,_ResumeText; 
	// Use this for initialization
   
	void Awake(){
		

	}
	void Start ()
	{
		base.Init();	
//		if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode)
//		{
//			_Resume.SetActive(false);
//			_ResumeText.SetActive(false);
//		}

		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		if(GameObject.Find("BallSelectionSubScreenManager(Clone)"))
				GameObject.Find("BallSelectionSubScreenManager(Clone)").GetComponent<ScreenManager>().closeScreenManager ();
	}
	
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{

			//if(_isAndroidBack)
			//{
                //Debug.Log("Working");
				EventManager.GameResumeTrigger();
				if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
				{
					_screenManager.LoadScreen("PracticeGameplay");
					Practice.instance.SetPauseCondition(true);
				}
				if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
				_screenManager.LoadScreen("TimeAttackGamePlay");
				
				if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
					
				{
					_screenManager.LoadScreen("SuddenDeathGamePlay");
				}
					
				
				if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
					LevelHandler levelhandler = LevelHandler.instance;
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
					{
						_screenManager.LoadScreen("ArcadeGamePlay");
					}
					
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						_screenManager.LoadScreen("SkillShot");
					}
					
				}
				//_isAndroidBack = false;
			//}
		}
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}
    public void MainButtonClicked()
    {
        if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
        {
            MSportsHandler.Instance.DeInit();
            SportsConstants._GUILoad = false;
            Application.LoadLevel("StartScene");

        }
        else
        {
            if (GameObject.Find("TapToskip(Clone)"))
                GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
            _inputManager.GetCameraref().transform.parent = null;
            EventManager.GameHomeTrigger();
            _screenManager.LoadScreen("MainMenu");

            if (_gameManager.GetGameMode() == GameManager.GameMode.Training)
            {
                _gameManager.updateAchievements(1);
            }
        }
    }
    public void RetryBtnClicked()
    {
        if (GameObject.Find("TapToskip(Clone)"))
            GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
        _inputManager.GetCameraref().transform.parent = null;
        EventManager.GameReStartTrigger();
        _screenManager.LoadScreen("Empty");
    }
    public void ResumeBtnclicked()
    {
        if (_gameManager.GetGameMode() == GameManager.GameMode.Training)
        {
            _screenManager.LoadScreen("PracticeGameplay");
            Practice.instance.SetPauseCondition(true);
        }
        if (_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
            _screenManager.LoadScreen("TimeAttackGamePlay");

        if (_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)

        {
            _screenManager.LoadScreen("SuddenDeathGamePlay");
        }


        if (_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
        {
            LevelHandler levelhandler = LevelHandler.instance;
            if (levelhandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
            {
                _screenManager.LoadScreen("ArcadeGamePlay");
            }

            if (levelhandler._gameOverParameter == LevelHandler.GameoverParameter.time)
            {
                _screenManager.LoadScreen("SkillShot");
            }

        }
        EventManager.GameResumeTrigger();
    }
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "Menu")
			{
				//_screenManager.LoadScreen("Loading");
			



                if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
				{
					MSportsHandler.Instance.DeInit();
					SportsConstants._GUILoad = false;
					Application.LoadLevel("StartScene");

				}
				else
				{
					if(GameObject.Find("TapToskip(Clone)"))
						GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
					_inputManager.GetCameraref().transform.parent=null; 
					EventManager.GameHomeTrigger();
					_screenManager.LoadScreen("MainMenu");

					if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
					{
						_gameManager.updateAchievements (1);
					}
				}
			}
			
			if(item.name == "Resume")
			{
				//_screenManager.LoadScreen("Loading");
				//_inputManager.SetInputStatus()

				if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
				{
					_screenManager.LoadScreen("PracticeGameplay");
					Practice.instance.SetPauseCondition(true);
				}
				if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
				_screenManager.LoadScreen("TimeAttackGamePlay");
				
				if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
					
				{
					_screenManager.LoadScreen("SuddenDeathGamePlay");
				}
					

				if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
					LevelHandler levelhandler = LevelHandler.instance;
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
					{
						_screenManager.LoadScreen("ArcadeGamePlay");
					}
					
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						_screenManager.LoadScreen("SkillShot");
					}
					
				}
				EventManager.GameResumeTrigger();
			}
			
			if(item.name == "Retry")
			{
				if(GameObject.Find("TapToskip(Clone)"))
					GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
				_inputManager.GetCameraref().transform.parent=null; 
				EventManager.GameReStartTrigger();	
				_screenManager.LoadScreen("Empty");
//				if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
//				{
//					_screenManager.LoadScreen("PracticeGameplay");
//				}
//				if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
//				{
//					_screenManager.LoadScreen("TimeAttackGamePlay");
//				}
//				
//				if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
//				{
//					
//					_screenManager.LoadScreen("SuddenDeathGamePlay");
//				}
//				
//				if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
//				{
//					LevelHandler levelhandler = LevelHandler.instance;
//					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
//					{
//						_screenManager.LoadScreen("ArcadeGamePlay");
//					}
//					
//					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.time)
//					{
//						_screenManager.LoadScreen("SkillShot");
//					}
//				}
			
				
			}
			
			if(item.name == "on")
			{
				SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
			}
			if(item.name == "off")
			{
				SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
			}
		}
		
		
		
	}	
}