using UnityEngine;
using System.Collections;

public class HorozontalSlider : MonoBehaviour {

	// Use this for initialization
	private GameManager gameManager;
	
	public enum SliderType
	{
		music,sfx,flick,swing
	};
	public SliderType sliderType;
	private Rect Default ; 
	private bool toucActive;
	public GUITexture rightTexture;
	public GUITexture leftTexture;
	private Vector3 pos;
	public static HorozontalSlider instance;
	public float slidervalue;
	private float slideLenth;
	private float currentSlideLenth;
	public float xcmpTransformPos;
	Transform mTransform ;
	void Awake()
	{
		instance = this;
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		mTransform  = transform;
	}
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		switch(sliderType)
		{
			
			case SliderType.flick:
			{
				Vector3 temp = transform.position;
				xcmpTransformPos = gameManager.UpdateFloatDataInDevice("flickSensityvity",transform.position.x,true);
				if(xcmpTransformPos>temp.x)
				temp.x = xcmpTransformPos;
				transform.position = temp;
				break;
			}
			
			case SliderType.music:
			{
				Vector3 temp = transform.position;
				xcmpTransformPos = gameManager.UpdateFloatDataInDevice("musicvalue",transform.position.x,true);
				if(xcmpTransformPos>temp.x)
				temp.x = xcmpTransformPos;
				transform.position = temp;
				break;
			}
			
			case SliderType.sfx:
			{
				Vector3 temp = transform.position;
				xcmpTransformPos = gameManager.UpdateFloatDataInDevice("sfxvalue",transform.position.x,true);
				if(xcmpTransformPos>temp.x)
				temp.x = xcmpTransformPos;
				transform.position = temp;
				break;
			}
			
			case SliderType.swing:
			{
				Vector3 temp = transform.position;
				xcmpTransformPos = gameManager.UpdateFloatDataInDevice("swingSensityvity",transform.position.x,true);
				if(xcmpTransformPos>temp.x)
				temp.x = xcmpTransformPos;
				transform.position = temp;
				break;
			}
		}
		Default = transform.GetComponent<GUITexture>().pixelInset;
		pos = transform.position;
		slideLenth = (rightTexture.transform.position-leftTexture.transform.position).magnitude;
		currentSlideLenth = (transform.position-leftTexture.transform.position).magnitude;
		slidervalue = (currentSlideLenth/slideLenth);
	}
	Vector3 mPos;
	// Update is called once per frame
	void Update () {




		if(mTransform.position.x > 0.9f)
		{
			mPos.Set(0.9f,mTransform.position.y,mTransform.position.z);
			mTransform.position = mPos;
		}
		if(slidervalue > 0.9830917f)
		{
			slidervalue = 0.9830917f;
		}
		if(xcmpTransformPos > 0.9f)
		{
			xcmpTransformPos = 0.9f;
		}
	//	else
		{






			if(Input.GetMouseButtonDown(0))
			{
				if (GetComponent<GUITexture>().HitTest(Input.mousePosition, Camera.main))
				{
					toucActive = true;
				}

			}

			if(Input.GetMouseButtonUp(0))
			{
				toucActive = false;
				switch(sliderType)
				{
				case SliderType.flick:
					{
						gameManager.UpdateFloatDataInDevice("flickSensityvity",xcmpTransformPos,false);
						break;

					}

				case SliderType.music:
					{
						gameManager.UpdateFloatDataInDevice("musicvalue",xcmpTransformPos,false);
						SoundManager.instance.SetSond();
						break;

					}

				case SliderType.sfx:
					{
						gameManager.UpdateFloatDataInDevice("sfxvalue",xcmpTransformPos,false);
						SoundManager.instance.SetSond();
						break;

					}

				case SliderType.swing:
					{
						gameManager.UpdateFloatDataInDevice("swingSensityvity",xcmpTransformPos,false);
						break;

					}
				}

			}

			if(toucActive && Input.GetMouseButton(0))
			{
				if(pos.x>=leftTexture.transform.position.x && pos.x<= rightTexture.transform.position.x)
				{
					pos.x = Input.mousePosition.x/Screen.width;
					if(pos.x >= rightTexture.transform.position.x)
						pos.x = rightTexture.transform.position.x;
					if(pos.x <= leftTexture.transform.position.x)
						pos.x = leftTexture.transform.position.x;
					transform.position = pos;
					xcmpTransformPos = transform.position.x;
					transform.GetComponent<MenuItemExitAnim>()._EndPos = pos;
					currentSlideLenth = (transform.position-leftTexture.transform.position).magnitude;
					slidervalue = (currentSlideLenth/slideLenth);
				}
			}


		}

	
	}
}
