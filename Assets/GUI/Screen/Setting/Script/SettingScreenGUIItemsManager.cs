using UnityEngine;
using System.Collections;

public class SettingScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips,_OptOutoff,_OptOutOn;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager inputmanager;
	bool tutorial_level = false;
	
	bool _isAndroidBack = false;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		_gameManager = GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		inputmanager =  GameObject.Find("GameManager").GetComponent("InputManager") as InputManager;
	}
	
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				_screenManager.LoadScreen("MainMenu");
				_isAndroidBack = false;
			}
		}
	}
	
	public override void OnEntryAnimationCompleted()
	{
		_isAndroidBack = true;
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
//		print ("test21");
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			
			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
//				print (item.name);
				_screenManager.LoadScreen("MainMenu");
				
//				HorozontalSlider flick = GameObject.Find("flickSliderbtn").GetComponent<HorozontalSlider>();
//				HorozontalSlider swing = GameObject.Find("swingSliderbtn").GetComponent<HorozontalSlider>();
				//inputmanager.Setsensityvity(flick.slidervalue,swing.slidervalue);
			}
			if(item.name == "HowTopalyBtn")
			{
				_screenManager.LoadScreen("HowToplay");
			}
			if(item.name == "ResetDataBtn")
			{
				print (item.name);
			}
			if(item.name == "on")
			{
				SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gui);
			}
			if(item.name == "off")
			{
				SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gui);
			}
			
			if(item.name=="OptOutOn"){
			}
			else if(item.name=="OptOutOff"){
			}
			else if(item.name=="PrivacyPolicy"){
				Application.OpenURL("http://dumadu.com/privacy-policy/");			
            }

		}
		
	}
}