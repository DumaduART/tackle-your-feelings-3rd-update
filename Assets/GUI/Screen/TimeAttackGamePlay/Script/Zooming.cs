using UnityEngine;
using System.Collections;

public class Zooming : MonoBehaviour 
{
	Vector3 targetValue;
	Vector3 currentValue;
	float speed = .1f;

	// Use this for initialization
	void Start () 
	{
		currentValue = transform.localScale;
		StartCoroutine(StartAction(1));
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.localScale = new Vector3(Mathf.MoveTowards(transform.localScale.x , targetValue.x , speed * Time.deltaTime) , Mathf.MoveTowards(transform.localScale.y , targetValue.y , speed * Time.deltaTime) , transform.localScale.z);
		
		if(transform.localScale.x == targetValue.x)
		{
			if(targetValue.x > currentValue.x)
			{
				ZoomOut();
			}
			
			else
			{			
				ZoomIn();
			}
		}
		
	}
	
	void ZoomIn()
	{
		targetValue = currentValue + new Vector3(currentValue.x/(3.4f * 5.0f) ,currentValue.y/(1.5f * 5.0f), currentValue.z);
//		print (targetValue);
	}
	
	void ZoomOut()
	{
		targetValue = currentValue - new Vector3(currentValue.x/(3.4f * 2.5f) ,currentValue.y/(1.5f * 2.5f) , currentValue.z);
	}
	
	IEnumerator StartAction(float time)
	{
		yield return new WaitForSeconds(time);
		ZoomIn();
	}
}