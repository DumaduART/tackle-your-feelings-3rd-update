using UnityEngine;
using System.Collections;

public class ShoScroll : ScrollAndPaging
{
	
	public enum SelectionType
	{
		hitTheBar,throughRings,noSwing
	};
	public SelectionType _selectionType;
	GUIText ShopballText;
	GUIText EnvironmentText;
	GUIText AddupsText;
	public static ShoScroll instance;
	public GUIText[] _UnlockDescription;
	GameManager gameManager ;
	public GameObject[] _lockComponent;
	ScreenManager screenManager;
	// Use this for initialization
	void Start () 
	{
		base.InitScroll();		
		instance = this;
		//UpdateText();
//		//Debug.Log("Position = "+transform.localScale);
		gameManager = GameManager.GetInstance();
		ChkUnLockSkillShot();
		SetUnlockDescription();
		screenManager = GameObject.Find("ScreenManager").GetComponent<ScreenManager>();
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{				
		
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			if(item.name == "UnlockEnvironment1")
//			{
//				//Debug.Log("Environment1 Unlocked");
//			}			
//			else if(item.name == "UnlockEnvironment2")
//			{
//				//Debug.Log("Environment2 Unlocked");
//			}			
//			else if(item.name == "UnlockEnvironment3")
//			{
//				//Debug.Log("Environment3 Unlocked");
//			}			
//			else if(item.name == "UnlockEnvironment4")
//			{
//				//Debug.Log("Environment4 Unlocked");
//			}
//			
//			else if(item.name == "UnlockBall11")
//			{
//				//Debug.Log("Ball1 Unlocked");
//			}			
//			else if(item.name == "UnlockBall12")
//			{
//				//Debug.Log("Ball2 Unlocked");
//			}			
//			else if(item.name == "UnlockBall13")
//			{
//				//Debug.Log("Ball3 Unlocked");
//			}			
//			else if(item.name == "UnlockBall14")
//			{
//				//Debug.Log("Ball4 Unlocked");
//			}	
			if(item.name == "TUnlock1")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[0])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[0] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[0]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock2")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[1])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[1] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[1]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock3")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[2])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[2] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[2]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock4")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[3])
				{
		
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[3] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[3]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock5")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[4])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[4] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[4]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock6")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[5])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[5] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[5]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			
			else if(item.name == "TUnlock7")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[6])
				{
					//Debug.Log("tunlock7");
					gameManager._isthroughrings[6] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[6]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			
			
			else if(item.name == "TUnlock8")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[7])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[7] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[7]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock9")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[8])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[8] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[8]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock10")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[9])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[9] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[9]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock11")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[10])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[10] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[10]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock12")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[11])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[11] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[11]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock13")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[12])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[12] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[12]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock14")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[13])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[13] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[13]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "TUnlock15")
			{
				if(gameManager.GetCoins()>=gameManager._throughringsUnlockingCost[14])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isthroughrings[14] = 1;
					gameManager.RetriveThroughRingsCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._throughringsUnlockingCost[14]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			
			
			else if(item.name == "HUnlock1")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[0])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[0] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[0]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock2")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[1])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[1] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[1]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock3")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[2])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[2] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[2]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock4")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[3])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[3] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[3]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock5")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[4])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[4] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[4]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock6")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[5])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[5] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[5]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "HUnlock7")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[6])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[6] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[6]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock8")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[7])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[7] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[7]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock9")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[8])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[8] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[8]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock10")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[9])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[9] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[9]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock11")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[10])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[10] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[10]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "HUnlock12")
			{
				if(gameManager.GetCoins()>=gameManager._hitthebarUnlockingCost[11])
				{
					//Debug.Log("Environment1 Unlocked");
					gameManager._isHitTheBar[11] = 1;
					gameManager.RetriveHittheBarCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._hitthebarUnlockingCost[11]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			
			
			
			else if(item.name == "NUnlock1")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[0])
				{
					gameManager._isNoSwing[0] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[0]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}	
			
			else if(item.name == "NUnlock2")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[1])
				{
					gameManager._isNoSwing[1] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[1]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
				
			}
			
			else if(item.name == "NUnlock3")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[2])
				{
					gameManager._isNoSwing[2] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[2]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock4")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[3])
				{
					gameManager._isNoSwing[3] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[3]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock5")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[4])
				{
					gameManager._isNoSwing[4] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[4]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock6")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[5])
				{
					gameManager._isNoSwing[5] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[5]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock7")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[6])
				{
					gameManager._isNoSwing[6] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[6]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock8")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[7])
				{
					gameManager._isNoSwing[7] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[7]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock9")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[8])
				{
					gameManager._isNoSwing[8] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[8]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock10")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[9])
				{
					gameManager._isNoSwing[9] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[9]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock11")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[10])
				{
					gameManager._isNoSwing[10] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[10]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			else if(item.name == "NUnlock12")
			{
				if(gameManager.GetCoins()>=gameManager._noswingUnlockingCost[11])
				{
					gameManager._isNoSwing[11] = 1;
					gameManager.RetriveNoswingCondition(true);
					ChkUnLockSkillShot();
					gameManager.AddCoin(-gameManager._noswingUnlockingCost[11]);
				}
				
				else
				{
					CallCoinPurchagePage();
				}
			}
			
			
			
		}
	}
	
	public void SetUnlockDescription()
	{
		
		GameManager gamManager = GameManager.GetInstance();
		switch(gamManager.GetCategory())
		{
			case GameManager.Category.ThroughRings:
			{
				for(int i= 0 ; i< _UnlockDescription.Length;i++)
				{
					_UnlockDescription[i].text = "UNLOCK USING" + " " + gameManager._throughringsUnlockingCost[i].ToString() + " "+ "COINS";
				}
				break;
			}
			
			case GameManager.Category.HitTheBar:
			{
				for(int i= 0 ; i< _UnlockDescription.Length;i++)
				{
					_UnlockDescription[i].text = "UNLOCK USING" + " " + gameManager._hitthebarUnlockingCost[i].ToString() + " "+ "COINS";
				}
				break;
			}
			
			case GameManager.Category.NoSwingStorm:
			{
				for(int i= 0 ; i< _UnlockDescription.Length;i++)
				{
					_UnlockDescription[i].text = "UNLOCK USING" + " " + gameManager._noswingUnlockingCost[i].ToString() + " "+ "COINS";
					//Debug.Log("Count: "+i);
				}
				break;
			}
		}
	}
	
	public void ChkUnLockSkillShot()
	{
		GameManager gamManager = GameManager.GetInstance();
		switch(gamManager.GetCategory())
		{
			case GameManager.Category.ThroughRings:
			{
				for(int i = 0;i<gamManager._isthroughrings.Length;i++)
				{
					if(gamManager._isthroughrings[i] == 1)
					{
						_lockComponent[i].SetActive(false);
					}
				}
				break;
			}
			
			case GameManager.Category.HitTheBar:
			{
				for(int i = 0;i<gamManager._isHitTheBar.Length;i++)
				{
					if(gamManager._isHitTheBar[i] == 1)
					{
						_lockComponent[i].SetActive(false);
					}
				}
				break;
			}
			
			case GameManager.Category.NoSwingStorm:
			{
				for(int i = 0;i<gamManager._isNoSwing.Length;i++)
				{
					if(gamManager._isNoSwing[i] == 1)
					{
						_lockComponent[i].SetActive(false);
					}
				}
				break;
			}
		}
	}
	
	public void CallCoinPurchagePage()
	{
			screenManager.LoadScreen("CoinPurchase");
			_screenManager.LoadScreen("Empty");
			screenManager.prevPageBeforeCoinPurchage = "SkillShotScreen1";
	}

    void ActivateControl()
	{
		ControlStatus(true);
	}
	void ControlStatus(bool isActivate)
	{
		GUIItemButton[] btns = transform.GetComponentsInChildren<GUIItemButton>();
		foreach(GUIItemButton btn in btns)
		{
			if(isActivate)
			{
				btn.enabled = true;
			}
			else
			{
				btn.enabled = false;
			}
		}
	}
}
