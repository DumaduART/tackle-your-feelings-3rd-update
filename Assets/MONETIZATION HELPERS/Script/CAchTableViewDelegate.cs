﻿using UnityEngine;
using System.Collections;
using FBKit;
using MonetizationHelper;

namespace MonetizationHelper
{
public class CAchTableViewDelegate : MonetizationHelper.CTableViewDelegate 
{
	// Use this for initialization
	void Start () 
	{
		base.Init();
		FillTableViewWithData(AchHandler.Instance.AchievementData);
	}
	
	public override void FillTableViewWithData(string pString)
	{
		if(pString == null || pString == "")
			return;
		
		if(_TableView != null)
		{
			Clear();
			_TableViewData = pString;
			Save();
			
			//WRITE SCRIPT IN DERIVED CLASS TO LOAD DATA
			JSONObject tObjectMain = new JSONObject(_TableViewData);
			JSONObject tObjectMain2 = tObjectMain.GetField("AchievementList");
			
			if(tObjectMain2.type == JSONObject.Type.ARRAY)
			{
				ArrayList tObjArray = tObjectMain2.list;
				foreach(JSONObject tObj in tObjArray)
				{
					JSONObject tActualObj = tObjectMain.GetField(tObj.GetField("AchievementName").str);
					_TableView.addCellOfType(0,tActualObj.print());
				}
			}			
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetMouseButtonDown(0))
		{
			AchHandler.Instance.AddPercentageAchieved("Roland",10);
			AchHandler.Instance.AddPercentageAchieved("Rishi",5);
			FillTableViewWithData(AchHandler.Instance.AchievementData);
		}
		if(Input.GetMouseButtonDown(1))
		{
			AchHandler.Instance.ClearAchievements();
			FillTableViewWithData(AchHandler.Instance.AchievementData);
		}
	}
}
}