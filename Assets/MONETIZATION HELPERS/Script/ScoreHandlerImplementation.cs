﻿using UnityEngine;
using System.Collections;
using FBKit;
using System;
using MonetizationHelper;

namespace MonetizationHelper
{
public class ScoreHandlerImplementation : MonoBehaviour 
{
	string mScoreData = null;
	JSONObject mJSONObject = null;
	string mFileName = "ScoreHandlerData";
	
	// Use this for initialization
	void Awake () 
	{
		string mScoreData = "";
		if(PlayerPrefs.HasKey("ScoreHandlerData"))
			mScoreData = PlayerPrefs.GetString("ScoreHandlerData");
		
		if(mScoreData != "")
			InitializeLeaderboards(mScoreData);
		else
		{
			//INITIALIZE FROM FILE
			TextAsset txt = (TextAsset)Resources.Load(mFileName, typeof(TextAsset));
			if(txt != null)
			{
   				mScoreData = txt.text;
				InitializeLeaderboards(mScoreData);
			}
			//else
				//Debug.Log("Missing leaderboard initializer file");
		}
	}
	
	public void InitializeLeaderboards(string pLeaderboardData)
	{
		mScoreData = pLeaderboardData;
		mJSONObject = new JSONObject(mScoreData);
		SaveData();
	}
	
	public string ScoreData
	{
		//ALL SCORE DATA
		get
		{
			return mScoreData;
		}
	}
	
	public string GetScoreDataFor(string pLeaderBoard)
	{
		//GET SCORE DATA FOR PARTICULAR LEADER BOARD
		JSONObject tObj = mJSONObject.GetField(pLeaderBoard);
		if(tObj != null)
			return tObj.print();
		return "";
	}
	
	public void PostScore(int pScore,string pLeaderBoardName)
	{
			
			
		//ASSIGN APPROPRIATE POSITION IN THE LEADER BOARD ALSO SAVE DATE OF SCORE
		JSONObject leaderBoard = mJSONObject.GetField(pLeaderBoardName);
		if(leaderBoard != null)
		{				
			JSONObject leaderBoardData = leaderBoard.GetField("Data");
			int tEntryCount = (int)leaderBoard.GetField("EntryCount").n;
			
			JSONObject scoreObject = new JSONObject(JSONObject.Type.OBJECT);
			scoreObject.AddField("Score",pScore);
			scoreObject.AddField("Rank",0);
			string date = DateTime.Now.ToString("HH:mm yyyy-MM-dd");
			scoreObject.AddField("Date",date);
			
			bool tbDescending = leaderBoard.GetField("Descending").b;
			int insertIndex = 0;
			foreach(JSONObject tObj in leaderBoardData.list)
			{
				int tscore = (int)tObj.GetField("Score").n;
				if(tbDescending)
				{
					if(pScore < tscore)
						insertIndex++;
				}
				else if(!tbDescending)
				{
					if(pScore > tscore)
						insertIndex++;
				}
			}
			if(leaderBoardData.list.Count > 0)
				leaderBoardData.list.Insert(insertIndex,scoreObject);
			else
				leaderBoardData.Add(scoreObject);
			
			if(leaderBoardData.list.Count > tEntryCount)
				leaderBoardData.list.RemoveAt(tEntryCount);
			
			SaveData();
		}
		//else
			//Debug.Log("This leaderBoard has yet to be registered");
	}
	
	void SaveData()
	{
		if(mJSONObject != null)
			mScoreData = mJSONObject.print();
		
		PlayerPrefs.SetString("ScoreHandlerData",mScoreData);
	}
	
	public void ClearAllScores()
	{
		PlayerPrefs.DeleteKey("ScoreHandlerData");
		mJSONObject = null;
		string mScoreData = "";
		if(PlayerPrefs.HasKey("ScoreHandlerData"))
			mScoreData = PlayerPrefs.GetString("ScoreHandlerData");
		
		if(mScoreData != "")
			InitializeLeaderboards(mScoreData);
		else
		{
			//INITIALIZE FROM FILE
			TextAsset txt = (TextAsset)Resources.Load(mFileName, typeof(TextAsset));
			if(txt != null)
			{
   				mScoreData = txt.text;
				InitializeLeaderboards(mScoreData);
			}
			//else
				//Debug.Log("Missing leaderboard initializer file");
		}
	}
	
	void OnDestroy()
	{
		 ScoreHandler.InstanceDestroyed();
	}
}
}