﻿using UnityEngine;
using System.Collections;
using FBKit;
using MonetizationHelper;

namespace MonetizationHelper
{
	
public class CScoreTableViewDelegate : MonetizationHelper.CTableViewDelegate
{
	// Use this for initialization
	public string _LeaderboardName = "Roland LB";
	void Start () 
	{
		base.Init();
		FillTableViewWithData(ScoreHandler.Instance.GetScoreDataFor(_LeaderboardName));
		_TableView = GetComponent<CTableView>();
		if(_TableView != null)
			_TableView.InitTableView();
	}
	
	public override void FillTableViewWithData(string pString)
	{
		if(pString == null || pString == "")
			return;
		
		if(_TableView != null)
		{
			Clear();
			_TableViewData = pString;
			Save();
			
			//WRITE SCRIPT IN DERIVED CLASS TO LOAD DATA
			JSONObject tMainObj = new JSONObject(pString);
			JSONObject tObjectMain2 = tMainObj.GetField("Data");
			
			if(tObjectMain2.type == JSONObject.Type.ARRAY)
			{
				if(tObjectMain2.list.Count <= 0)
					Debug.Log("No Data available");
				else
				{
					ArrayList tObjArray = tObjectMain2.list;
					int rank = 1;
					foreach(JSONObject tObj in tObjArray)
					{
						tObj.GetField("Rank").n = rank++;
						_TableView.addCellOfType(0,tObj.print());
					}
				}
			}	
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		//MAHESH
//		if(Input.GetMouseButtonDown(0))
//		{
//			ScoreHandler.Instance.PostScore(Random.Range(10,100), _LeaderboardName);
//			FillTableViewWithData(ScoreHandler.Instance.GetScoreDataFor(_LeaderboardName));
//		}
		//ScoreHandler.Instance.ClearAllScores();
	}
}
}