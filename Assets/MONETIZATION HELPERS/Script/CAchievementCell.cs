﻿using UnityEngine;
using System.Collections;
using FBKit;
using MonetizationHelper;

namespace MonetizationHelper
{
public class CAchievementCell : MonetizationHelper.CCell
{
	public Texture _TrophyTexture;
	public Color _AchievementTextColor = Color.white;
	public Color _DescriptionTextColor = Color.white;
	JSONObject mJSONObject;
	
	float mfPercentageCompletion = 0;
	// Use this for initialization
	void Start () 
	{
		if(_Textfield1 != null)
			_Textfield1.GetComponent<Renderer>().material.color = _AchievementTextColor;
		
		if(_Textfield2 != null)
			_Textfield2.GetComponent<Renderer>().material.color = _DescriptionTextColor;
	}
	
	public override void initializeWithJSONString(string pJSONString)
	{
		_UserData = pJSONString;
		refreshData();
	}
	
	public override void refreshData()
	{
		mJSONObject = new JSONObject(_UserData);
		mJSONObject.type = JSONObject.Type.OBJECT;
		
		//REFRESH UR DATA HERE
		if(_Textfield1 != null)
			_Textfield1.text = mJSONObject.GetField("AchievementName").str;
		
		if(_Textfield2 != null)
			_Textfield2.text = mJSONObject.GetField("AchievementDescription").str;
		
		mfPercentageCompletion = (float) mJSONObject.GetField("Percentage").n;
		if(_Textfield3 != null && mfPercentageCompletion > 0 && mfPercentageCompletion < 100)
			_Textfield3.text = ""+mJSONObject.GetField("Percentage").n+"%";
		
		if(mfPercentageCompletion >= 100)
			_ImageField1.GetComponent<Renderer>().material.mainTexture = _TrophyTexture;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//NOT REQUIRED UNLESS TOUCH IS INVOLVED
	}
}
}