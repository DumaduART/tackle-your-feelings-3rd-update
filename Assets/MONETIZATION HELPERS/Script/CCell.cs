using UnityEngine;
using System.Collections;
using MonetizationHelper;

namespace MonetizationHelper
{


[System.Serializable]
public class CellTextFieldData
{
	public GameObject _TextFieldPrefab;
	public Vector3 _LocalPosition;
	public Vector3 _LocalScale;
	public Color _TextFieldColor;
	public string _TextFieldString;
	public TextAlignment _TextFieldAlignment;
	public TextAnchor _TextFieldAnchor;
}

[System.Serializable]
public class CellImageData
{
	public GameObject _CellImagePrefab;
	public string _DownloadUrl;
	public Texture _PredefinedTexture;
	public bool _IsButton;
}

public class CCell : MonoBehaviour
{	
	public TextMesh _Textfield1;
	public TextMesh _Textfield2;	
	public TextMesh _Textfield3;
	public TextMesh _Textfield4;
	public GameObject _ImageField1;
	public GameObject _ImageField2;
	public string _UserData;
	
	void Start () 
	{
		
	}
	
	public virtual void initializeWithJSONString(string pJSONString)
	{
		_UserData = pJSONString;
	}
	
	public virtual void refreshData()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
}