﻿using UnityEngine;
using System.Collections;
using System.IO;
using FBKit;
using MonetizationHelper;

namespace MonetizationHelper
{
public class AchHandlerImplementation : MonoBehaviour {
	
	string mAchievementData = null;
	JSONObject mJSONObject = null;
	string mFileName = "AchievementData";
	
	// Use this for initialization
	void Awake () 
	{
		string tAchData = "";
		if(PlayerPrefs.HasKey("AchievementHandlerData"))
			tAchData = PlayerPrefs.GetString("AchievementHandlerData");
		
		if(tAchData != "")
			InitializeAllAchievements(tAchData);
		else
		{
			//INITIALIZE FROM FILE
			TextAsset txt = (TextAsset)Resources.Load(mFileName, typeof(TextAsset));
			if(txt != null)
			{
   				tAchData = txt.text;
				//Debug.Log("File loaded with content "+tAchData);
				InitializeAllAchievements(tAchData);
			}
			//else
			//	Debug.Log("Missing achievement initializer file");
		}
	}
	
	public void InitializeAllAchievements(string pAchievementData)
	{
		mAchievementData = pAchievementData;
		mJSONObject = new JSONObject(mAchievementData);
		SaveData();
	}
	
	public string AchievementData
	{
		get
		{
			return mAchievementData;
		}
	}
	
	public void SetPercentageAchieved(string pAchievementName,float pfPercentage)
	{
		JSONObject achievement = mJSONObject.GetField(pAchievementName);
		if(achievement != null)
		{
			if(pfPercentage >= 100)
				pfPercentage = 100;
			else if(pfPercentage <= 0)
				pfPercentage = 0;
			
			achievement.GetField("Percentage").n = pfPercentage;
			mAchievementData = mJSONObject.print();
			
			SaveData();
		}
		//else
			//Debug.Log("This achievement has yet to be registered");
	}
	
	public void AddPercentageAchieved(string pAchievementName,float pfPercentage)
	{
		JSONObject achievement = mJSONObject.GetField(pAchievementName);
		if(achievement != null)
		{
			float percentageCompleted = (float)achievement.GetField("Percentage").n;
			if(percentageCompleted+pfPercentage >= 100)
				percentageCompleted = 100;
			else if(percentageCompleted+pfPercentage <= 0)
				percentageCompleted = 0;
			else
				percentageCompleted += pfPercentage;
			
			achievement.GetField("Percentage").n = percentageCompleted;
			SaveData();
		}
		//else
			//Debug.Log("This achievement has yet to be registered");
	}
	
	void SaveData()
	{
		if(mJSONObject != null)
			mAchievementData = mJSONObject.print();
		PlayerPrefs.SetString("AchievementHandlerData",mAchievementData);
	}
	
	public void ClearAchievements()
	{
		PlayerPrefs.DeleteKey("AchievementHandlerData");
		
		string tAchData = "";
		if(PlayerPrefs.HasKey("AchievementHandlerData"))
			tAchData = PlayerPrefs.GetString("AchievementHandlerData");
		
		if(tAchData != "")
			InitializeAllAchievements(tAchData);
		else
		{
			//INITIALIZE FROM FILE
			TextAsset txt = (TextAsset)Resources.Load(mFileName, typeof(TextAsset));
			if(txt != null)
			{
   				tAchData = txt.text;
				//Debug.Log("File loaded with content "+tAchData);
				InitializeAllAchievements(tAchData);
			}
			//else
				//Debug.Log("Missing achievement initializer file");
		}
	}
	
	void OnDestroy()
	{
		AchHandler.InstanceDestroyed();
	}
}
}