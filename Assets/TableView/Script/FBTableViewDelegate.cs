using UnityEngine;
using System.Collections;
using FBKit;

public class FBTableViewDelegate : CTableViewDelegate 
{
	// Use this for initialization
	public string JSONString = "";
	void Awake () 
	{
		base.Init();	
	//	OnReceivedData(JSONString,0);
	}
	
	public void OnReceivedData(string pString,int val)
	{
		if(pString==null || pString == "")
			pString=JSONString;
		//Debug.Log("Received Data");
		//Debug.Log(pString);
		//CONVERT THIS STRING TO ARRAY OF JSON OBJECTS
		//CONVERT IT BACK TO STRING FORMAT AND FILL THE TABLE
//		JSONObject JSONData = new JSONObject(pString);
//		JSONData = JSONData.GetField("app42");
//		JSONData = JSONData.GetField("response");
//		
//		bool success = JSONData.GetField("success").b;
//		if(success)
//		{
//			JSONData = JSONData.GetField("social");
//			JSONData = JSONData.GetField("friends");			
			FillTableViewWithData(pString,val);
//		}
	}
	
	public override void FillTableViewWithData(string pString,int val)
	{
		//Debug.Log("FillTableViewWithData---");
		//Debug.Log(pString);
		if(pString == null || pString == "")
			return;
		
		if(_TableView != null)
		{
			//Debug.Log(pString);
			Clear();
			_TableViewData = pString;
			Save();
			
			//WRITING SCRIPT IN DERIVED CLASS TO LOAD DATA
			JSONObject JSONData = new JSONObject(pString);
			ArrayList tArray = JSONData.list;
			//Debug.Log("=======1");
			if(tArray.Count > 0)
			{
				//Debug.Log("=======2");
				if(val==0)
				{
					//Debug.Log("=======3");
					foreach(JSONObject jObj in tArray)
					{
						JSONObject tJSONObj = new JSONObject();
						tJSONObj.type = JSONObject.Type.OBJECT;					
						tJSONObj.AddField("Name",jObj.GetField("Name").str);
						tJSONObj.AddField("Score",jObj.GetField("Score").str);
						tJSONObj.AddField("UserId",jObj.GetField("UserId").str);
						tJSONObj.AddField("Picture",jObj.GetField("Image").str);
						tJSONObj.AddField("Rank",jObj.GetField("Rank").str);					
						_TableView.addCellOfType(0,tJSONObj.print());
					}
				}
				else if(val==1)
				{
					//Debug.Log("=======4");
					foreach(JSONObject jObj in tArray)
					{
						//Debug.Log("=======5");
						JSONObject tJSONObj = new JSONObject();
						tJSONObj.type = JSONObject.Type.OBJECT;					
						tJSONObj.AddField("Name",jObj.GetField("Name").str);
						tJSONObj.AddField("Message",jObj.GetField("Score").str);
						tJSONObj.AddField("UserId",jObj.GetField("UserId").str);
						tJSONObj.AddField("Picture",jObj.GetField("Image").str);
						//Debug.Log("======="+tJSONObj.print());
						_TableView.addCellOfType(1,tJSONObj.print());
					}
				}
			}
			else
			{
				//IN CASE NO ENTRIES
				JSONObject tJSONObj = new JSONObject();
				tJSONObj.type = JSONObject.Type.OBJECT;
				tJSONObj.AddField("textField1","NO STATS TODAY");
				tJSONObj.AddField("Rounds",0);
				tJSONObj.AddField("textField2","");
				tJSONObj.AddField("textField4","");	
				tJSONObj.AddField("Scored",0);
				tJSONObj.AddField("textField3","");
				_TableView.addCellOfType(0,tJSONObj.print());
				_TableView.addCellOfType(0,tJSONObj.print());
				_TableView.addCellOfType(0,tJSONObj.print());
				_TableView.addCellOfType(0,tJSONObj.print());
				_TableView.addCellOfType(0,tJSONObj.print());
				_TableView.addCellOfType(0,tJSONObj.print());
			}								
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
