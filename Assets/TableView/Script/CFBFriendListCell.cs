﻿using UnityEngine;
using System.Collections;
using FBKit;
public class CFBFriendListCell : CCell 
{
	JSONObject _jsonObject;
	string mImageURL = "";
	// Use this for initialization
	void Start () 
	{
		if(_Textfield1 != null)
			_Textfield1.GetComponent<Renderer>().material.color = _TextField1Color;
		
		if(_Textfield2 != null)
			_Textfield2.GetComponent<Renderer>().material.color = _TextField2Color;
				
		if(_ImageField2 != null && _ImageFieldTexture2 != null)
			_ImageField2.GetComponent<Renderer>().material.mainTexture = _ImageFieldTexture2;
		
		if(_Textfield3 != null)
			_Textfield3.GetComponent<Renderer>().material.color = _TextField3Color;
		
		if(_Textfield4 != null)
			_Textfield4.GetComponent<Renderer>().material.color = _TextField4Color;
		
		refreshData();
	}
	
	public override void initializeWithJSONString(string pJSONString,GameObject cam)
	{
		
		_TableViewCam=cam;
		//Debug.Log(pJSONString);
		
		_jsonObject = new JSONObject(pJSONString);
		mImageURL = _jsonObject.GetField("Picture").str;
		refreshData();
		if(mImageURL != "")
			StartCoroutine(LoadImageFromURL(mImageURL));
	}
	
	IEnumerator LoadImageFromURL(string url)
	{
		//Debug.Log("initializeWithJSONString=="+url);
		
		WWW www = new WWW(url);
		yield return www;
		if(www.error == null)			
		{
			
			//Debug.Log("initializeWithJSONString22");
			Texture2D tex = new Texture2D(256, 256, TextureFormat.RGB24, false);
			//Debug.Log("Got texture");
			 www.LoadImageIntoTexture(tex);
			//Texture2D tex=www.texture;
			//tex.Compress(true);
			tex.wrapMode = TextureWrapMode.Clamp;
			tex.anisoLevel = 9;
			tex.filterMode = FilterMode.Bilinear;
			_ImageField1.GetComponent<Renderer>().material.mainTexture = tex;
		}
		else
		{
			//Debug.Log("initializeWithJSONString33"+www.error);
		}
	}
	
	public override void refreshData()
	{		
		
		if(_jsonObject != null)
		{
			
			if(_jsonObject.HasField("Name"))
				if(_Textfield1!= null)
					_Textfield1.text = _jsonObject.GetField("Name").str;
			
			if(_jsonObject.HasField("Message"))
				if(_Textfield2!= null)
					_Textfield2.text = _jsonObject.GetField("Message").str;
			
			if(_jsonObject.HasField("UserId"))
				_CellUserId= _jsonObject.GetField("UserId").str;	
			
			if(_jsonObject.HasField("Picture"))
				_CellUserImgUrl=_jsonObject.GetField("Picture").str;
		
//			if(_jsonObject.GetField("Installed").b)
//				if(_Textfield3!= null)
//					_Textfield3.text = "Installed";
//			else
//				if(_Textfield3!= null)
//					_Textfield3.text = "Not Installed";
			
		}
	}
	
	void loadPhotoFromNet()
	{
		
	}
	
void LateUpdate ()
	{
		RaycastHit hit;
 		Ray ray = _TableViewCam.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
  		if(Physics.Raycast(ray,out hit,1000.0f) && Input.GetMouseButtonDown(0)) 
		{
			
			Vector3 pos= _TableViewCam.GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
			if(pos.x>(_TableViewCam.GetComponent<Camera>().transform.position.x-(_TableViewCam.GetComponent<Camera>().orthographicSize)) && pos.x<(_TableViewCam.GetComponent<Camera>().transform.position.x+(_TableViewCam.GetComponent<Camera>().orthographicSize)) && pos.y>(_TableViewCam.GetComponent<Camera>().transform.position.y-(_TableViewCam.GetComponent<Camera>().orthographicSize)) && pos.y<(_TableViewCam.GetComponent<Camera>().transform.position.y+(_TableViewCam.GetComponent<Camera>().orthographicSize)))
			{		
				if(hit.collider.name=="AcceptBtn")
				{
					GameObject currGameObj=hit.collider.gameObject;
					currGameObj=currGameObj.transform.parent.gameObject;
					if(currGameObj.GetComponent<CFBFriendListCell>()._CellUserId== _CellUserId)
					{	
						print(currGameObj.GetComponent<CFBFriendListCell>()._CellUserId);
						//GameObject.Find("AppWarp(Clone)").GetComponent<appwarp>().sendFBRequestAccepted(currGameObj.GetComponent<CFBFriendListCell>()._CellUserId,GameObject.Find("FacebookFramework").GetComponent<FacebookFramework>().GetNameofCurrentUser(),GameObject.Find("FacebookFramework").GetComponent<FacebookFramework>().GetImageUrlofCurrentUser());
					}	
				}
				else if(hit.collider.name=="InviteBtn")
				{
					GameObject currGameObj=hit.collider.gameObject;
					currGameObj=currGameObj.transform.parent.gameObject;
					if(currGameObj.GetComponent<CFBFriendListCell>()._CellUserId== _CellUserId)
					{	
						print(currGameObj.GetComponent<CFBFriendListCell>()._CellUserId);
						//GameObject.Find("AppWarp(Clone)").GetComponent<appwarp>().sendFBConnectionRequest(currGameObj.GetComponent<CFBFriendListCell>()._CellUserId,GameObject.Find("FacebookFramework").GetComponent<FacebookFramework>().GetNameofCurrentUser(),GameObject.Find("FacebookFramework").GetComponent<FacebookFramework>().GetImageUrlofCurrentUser());//"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-prn2/1076525_100002718755914_797702763_q.jpg");
					}
				}
			}
		}
	}
}
