using UnityEngine;
using System;
using System.Collections;
using FBKit;

public class CTableViewDelegate : MonoBehaviour {
	
	public CTableView _TableView = null;	
	public string _StorageName = "";
	public bool _OfflineCapability = false;
	
	protected string _TableViewData = null;
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	public void Init()
	{
		if(_TableView == null)
			_TableView = GetComponent<CTableView>();
		
		if(_TableView == null)
			Debug.Log("TableView Component Missing");
		
		bool tHasPreviousStorage = false;
		
		if(_StorageName != "")
			PlayerPrefs.HasKey(_StorageName);
		
		if(_OfflineCapability && tHasPreviousStorage)
		{
			_TableViewData = PlayerPrefs.GetString(_StorageName);
			if(_TableViewData != null)
				FillTableViewWithData(_TableViewData,0);
		}
	}
	
	public void Clear()
	{
		//CLEARING THE TABLE VIEW
		_TableView.Clear();
	}
	
	public virtual void FillTableViewWithData(string pString,int val)
	{
		if(pString == null || pString == "")
			return;
		
		if(_TableView != null)
		{
			Clear();
			_TableViewData = pString;
			Save();
			//WRITE SCRIPT IN DERIVED CLASS TO LOAD DATA
		}
	}
	
	public void Save()
	{
		//SAVE THE CURRENT TABLE VIEW DATA IF OFFLINE CAPABILITY IS ON
		if(_StorageName != "" && _OfflineCapability)
			PlayerPrefs.SetString(_StorageName,_TableViewData);
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
		
}
