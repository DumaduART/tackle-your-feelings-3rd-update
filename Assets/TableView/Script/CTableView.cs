using UnityEngine;
using System.Collections;

[System.Serializable]
public class CellDataStruct
{
	public GameObject _CellPrefab;
	public float _CellSpacingTop;
	public float _CellSpacingBottom;
	public Vector2 _CellSize;
	public Vector3 _Offset;
}

public class CTableView : CScrollView 
{
	public CellDataStruct[] _CellStructures;
	public Vector3 _ContentOffset;
	
	Vector3 mvAddCellPosition;
	ArrayList _CellArray = new ArrayList();

	public GameObject _TableViewCam;
	GameObject mgoTableViewCam;
	//public GameObject _goFacebookObj;
	// Use this for initialization
	void Start () 
	{
		//CREATE THE TABLE USING ITS TABLEVIEW DELEGATE SCRIPT
		GameObject objCam=GameObject.Find("TableViewCam(Clone)");
		if(objCam==null)
		{
			mgoTableViewCam= Instantiate(_TableViewCam) as GameObject;
		}
		else
		{
			mgoTableViewCam=objCam;		
		}
	}
	
	public void InitTableView()
	{
		_Bounds = new Rect(_Bounds.x,_Bounds.y,0,0);
		mvAddCellPosition = _ContentOffset;
		base.InitScroll();
	}
	
	public void addCellOfType(int pType,string pCellData)
	{
		
		if(_CellStructures.Length > pType)
		{
			
			//CREATE CELL OF THIS TYPE
			GameObject cell = Instantiate(_CellStructures[pType]._CellPrefab) as GameObject;
			cell.transform.parent = gameObject.transform;
			GameObject objCam=GameObject.Find("TableViewCam(Clone)");
			if(objCam==null)
			{
				mgoTableViewCam= Instantiate(_TableViewCam) as GameObject;
			}
			else
			{
				mgoTableViewCam=objCam;				
			}
			mvAddCellPosition += new Vector3(0,-1 * _CellStructures[pType]._CellSpacingTop,0);
			_Bounds = new Rect(_Bounds.x,_Bounds.y,_Bounds.width,_Bounds.height + _CellStructures[pType]._CellSpacingTop);
			
			cell.transform.localPosition = mvAddCellPosition;
			mvAddCellPosition += new Vector3(0,-1 * _CellStructures[pType]._CellSpacingBottom,0);

			float boundsWidth = mvAddCellPosition.x - _ContentOffset.x;
			float boundsHeight = (-1*mvAddCellPosition.y) - _ContentOffset.y;
			mvAddCellPosition += new Vector3(_CellStructures[pType]._CellSize.x/2.0f,-1 * (_CellStructures[pType]._CellSize.y/2.0f) ,0);
			
			if(boundsWidth < 0)
				boundsWidth = 0;
			if(boundsHeight < 0)
				boundsHeight = 0;
			
			_Bounds = new Rect(_Bounds.x,_Bounds.y,boundsWidth,boundsHeight);
			
			CCell tCellScr = cell.GetComponent<CCell>();
			if(tCellScr != null)
				tCellScr.initializeWithJSONString(pCellData,mgoTableViewCam);	
			
			_CellArray.Add(cell);
		}
	}
	
//	public void refresh()
//	{
//		foreach(GameObject obj in _CellArray)
//			Destroy(obj);
//		
//		_CellArray.RemoveRange(0,_CellArray.Count);
//		_CellArray = new ArrayList();
//		
//		_Bounds = new Rect(_Bounds.x,_Bounds.y,0,0);
//		mvAddCellPosition = _ContentOffset;		
//	}
	
	public void Clear()
	{
		foreach(GameObject obj in _CellArray)
			Destroy(obj);
		
		_CellArray.RemoveRange(0,_CellArray.Count);
		_CellArray = new ArrayList();
		
		_Bounds = new Rect(_Bounds.x,_Bounds.y,0,0);
		mvAddCellPosition = _ContentOffset;		
	}
	
	void OnDestroy()
	{
		//Debug.Log("Table view destroyed");
		
		foreach(GameObject tObj in _MaskArray)
			Destroy(tObj);
		_MaskArray.Clear();
		
		if(mgoTableViewCam != null)
		{
			Destroy(mgoTableViewCam);
			mgoTableViewCam = null;
		}
	}
	
//	void LateUpdate ()
//	{
//		RaycastHit hit;
// 		Ray ray = _TableViewCam.camera.ScreenPointToRay(Input.mousePosition);
//  		if(Physics.Raycast(ray,out hit,1000.0f) && Input.GetMouseButtonDown(0)) 
//		{
//			print(hit.collider.gameObject.GetComponent<CFBCell>()._CellUserId);
//			string strUserID=hit.collider.gameObject.GetComponent<CFBCell>()._CellUserId;
//			string strUserName=null;
//			foreach(Transform trs in hit.collider.gameObject.transform)
//			{
//				if(trs.gameObject.name=="TextField1")
//				{
//					GameObject child=trs.gameObject;
//					Debug.Log(child.GetComponent<TextMesh>().text); 
//					strUserName=child.GetComponent<TextMesh>().text;
//				}
//			}
//			_goFacebookObj.GetComponent<FacebookFramework>().ChallengeUser(strUserID,strUserName);
//		}
//	}
}
