using UnityEngine;
using System.Collections;
using FBKit;

[System.Serializable]
public class CellTextFieldData
{
	public GameObject _TextFieldPrefab;
	public Vector3 _LocalPosition;
	public Vector3 _LocalScale;
	public Color _TextFieldColor;
	public string _TextFieldString;
	public TextAlignment _TextFieldAlignment;
	public TextAnchor _TextFieldAnchor;
}

[System.Serializable]
public class CellImageData
{
	public GameObject _CellImagePrefab;
	public string _DownloadUrl;
	public Texture _PredefinedTexture;
	public bool _IsButton;
}

public class CCell : MonoBehaviour
{	
	public string _CellUserId;
	public string _CellUserImgUrl;
	
	public CellTextFieldData[] _CellTextFields;
	public CellImageData[] _CellImageFields;

	public TextMesh _Textfield1;
	public Color _TextField1Color;
	
	public TextMesh _Textfield2;
	public Color _TextField2Color;
	
	public TextMesh _Textfield3;
	public Color _TextField3Color;
	public Color _TextField3AlternateColor;

	public TextMesh _Textfield4;
	public Color _TextField4Color;
	
	public GameObject _ImageField1;
	public Texture[] _ImageFieldTexture1;
	
	public GameObject _ImageField2;
	public Texture _ImageFieldTexture2;

	public string _UserData;
	
	public GameObject _TableViewCam;
	
	//bool _CellIsSelectable = false;
	JSONObject _jsonObject;
	// Use this for initialization
	void Start () 
	{
		if(_Textfield1 != null)
			_Textfield1.GetComponent<Renderer>().material.color = _TextField1Color;
		
		if(_Textfield2 != null)
			_Textfield2.GetComponent<Renderer>().material.color = _TextField2Color;
		
		if(_Textfield3 != null)
			_Textfield3.GetComponent<Renderer>().material.color = _TextField3Color;
		
		if(_Textfield4 != null)
			_Textfield4.GetComponent<Renderer>().material.color = _TextField4Color;
				
		if(_ImageField2 != null && _ImageFieldTexture2 != null)
			_ImageField2.GetComponent<Renderer>().material.mainTexture = _ImageFieldTexture2;
		
		refreshData();
	}
	
	public virtual void initializeWithJSONString(string pJSONString,GameObject cam)
	{

	}
	
	public virtual void refreshData()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
