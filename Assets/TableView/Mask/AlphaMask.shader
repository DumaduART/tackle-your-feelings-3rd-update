Shader "Custom/AlphaMask" 
{
    Properties 
    {
        _Alpha1 ("Alpha Mask1 (A)", 2D) = "white" {}
        _MainTex ("Base Texture (Alpha)", 2D) = "white" {}
        _Alpha2 ("Alpha Mask2 (A)", 2D) = "white" {}
    }

    SubShader 
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
        ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB 
        
		Pass 
        {
        	SetTexture[_Alpha1] 
            {
                Combine texture
            } 
                        
            SetTexture[_MainTex] 
            {
                Combine texture*previous,previous
            }
            
            SetTexture[_Alpha2] 
            {
                Combine texture + previous,texture// * previous
            }            
        }
    }
}