﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectHandler : MonoBehaviour {
	public Material _mSportsBanner;
	public Texture2D  _OnTexture;
	public Texture2D  _OffTexture;
	float mTime;
	public float _Timeinterval = 0.2f;
	bool mStates;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.time - mTime > _Timeinterval)
		{
			mTime = Time.time;
			ChangeTexture();
		}
	}
	void ChangeTexture()
	{
		switch(mStates)
		{
		case true:
			mStates = false;
			_mSportsBanner.mainTexture = _OnTexture;
			return;
		case false:
			mStates = true;
			_mSportsBanner.mainTexture = _OffTexture;
			return;
		}

	}
}
