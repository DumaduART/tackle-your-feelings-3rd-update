﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreuSure : GUIItemsManager {
	public MainScreenGUIItemsManager _MainScreenGUIItemsManager;
	public GameObject _Reference;
	// Use this for initialization
	void Start () {
     
		base.Init ();	
		_Reference =  GameObject.Find ("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>().ClockBtn.gameObject;
		if(GameObject.Find ("MainMenu(Clone)/BackToModes") != null)
		  GameObject.Find ("MainMenu(Clone)/BackToModes").GetComponent<GUIItemButton> ().enabled = false;
		
		_MainScreenGUIItemsManager =  GameObject.Find("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(GameObject.Find ("MainMenu(Clone)/BackToModes") != null)
				GameObject.Find ("MainMenu(Clone)/BackToModes").GetComponent<GUIItemButton> ().enabled = true;
			
			if(SportsConstants._GUILoad)
			{
				//if(GameObject.Find ("MainMenu(Clone)/ClockShowDownbtn") != null)
				//				GameObject.Find ("MainMenu(Clone)/ClockShowDownbtn").SetActive(true);
				if(GameObject.Find ("MainMenu(Clone)") != null)
					GameObject.Find ("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>().ClockBtn.gameObject.SetActive(true);
			}
            if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode) {
				//MainScreenGUIItemsManager.instance.ModeButton.SetActive (true);
			}
			_MainScreenGUIItemsManager._eBackButtonStates = MainScreenGUIItemsManager.eBackButtonStates.None;
			Destroy (this.gameObject);
		}
	}

	public override void OnSelectedEvent(GUIItem item)
	{
		if (item.name == "Yes") {
			
			MSportsHandler.Instance.DeInit();
			SportsConstants._GUILoad = false;
			Application.LoadLevel("StartScene");
		}
		if (item.name == "No") {
			
			if(GameObject.Find ("MainMenu(Clone)/BackToModes") != null)
			   GameObject.Find ("MainMenu(Clone)/BackToModes").GetComponent<GUIItemButton> ().enabled = true;

			if(SportsConstants._GUILoad)
			{
				//if(GameObject.Find ("MainMenu(Clone)/ClockShowDownbtn") != null)
//				GameObject.Find ("MainMenu(Clone)/ClockShowDownbtn").SetActive(true);
				if(GameObject.Find ("MainMenu(Clone)") != null)
				GameObject.Find ("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>().ClockBtn.gameObject.SetActive(true);
			}
            if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode) {
				//MainScreenGUIItemsManager.instance.ModeButton.SetActive (true);
			}
			_MainScreenGUIItemsManager._eBackButtonStates = MainScreenGUIItemsManager.eBackButtonStates.None;
			Destroy (this.gameObject);
		}
	}
}
