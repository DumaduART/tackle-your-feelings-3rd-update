using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public enum PopUpType
	{
		awesome,good,great,go,scoreincreaser,multiplierIncreaser,timeIncreaser,levelup,roundUp,notenoughCoins
	}
public class PopUpManager : MonoBehaviour {

	// Use this for initialization
     public static PopUpManager instance;
	
	public ScreenManager _screenmanager;
	public ScreenManager _shopscreen;
	public PopUpType _popUpType;
	public ScreenManager _powerUpsubScreenManager;
	public ScreenManager _ScoresubScreenManager;
	public ScreenManager _multiplierScreenManager;
	public ScreenManager _timerScreenManager;
	public ScreenManager _roundUpScreenManager;
	public ScreenManager _NotenoughCoins;
	public List<GameObject> currentPopUps = new List<GameObject>();
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void CreatePopUp(PopUpType powerUpType)
	{
		switch (powerUpType)
		{
			case PopUpType.great:
			{
				_powerUpsubScreenManager.LoadScreen("great");
				break;
			}
			
			case PopUpType.good:
			{
				_powerUpsubScreenManager.LoadScreen("good");
				break;
			}
			
			case PopUpType.notenoughCoins:
			{
				if (GameObject.Find ("NotEnoughCoins(Clone)") == null) 
				_NotenoughCoins.LoadScreen("NotEnoughCoins");
				break;
			}
			
			case PopUpType.awesome:
			{
				_powerUpsubScreenManager.LoadScreen("Awesome");
				break;
			}
			case PopUpType.go:
			{
				_powerUpsubScreenManager.LoadScreen("3"); //Edited by Ankit
				break;
			}
			
			case PopUpType.scoreincreaser:
			{
				_ScoresubScreenManager.LoadScreen("ScoreIncreaser");
				break;
			}
			
			case PopUpType.multiplierIncreaser:
			{
				_multiplierScreenManager.LoadScreen("MultiplierIncreaser");
				break;
			}
			
			case PopUpType.timeIncreaser:
			{
				_timerScreenManager.LoadScreen("TimeIncreaser");
				break;
			}
			
			case PopUpType.levelup:
			{
				_powerUpsubScreenManager.LoadScreen("LevelUp");
				break;
			}
			
			case PopUpType.roundUp:
			{
				
				_roundUpScreenManager.LoadScreen("RoundUpPopUp");
				break;
			}
		}
	}
	
	
	public void AddToList(GameObject obj )
	{
		currentPopUps.Add(obj);
	}
	
	public void RemoveFromList(GameObject obj)
	{
		currentPopUps.Remove(obj);
	}
	
	public void HidePopUp()
	{
		
		int _lenth = currentPopUps.Count;
		for(int i=0;i<_lenth;i++)
		{
			if(currentPopUps[i]!=null)
			{
				foreach(Transform t in currentPopUps[i].transform)
				{
					t.gameObject.layer = LayerMask.NameToLayer("Hide");
				}
			}
		}
	}
	
	public void EnableInput(bool istrue)
	{
		
		if(istrue)
		{
			_screenmanager.EnableInput();
			_shopscreen.EnableInput();
		}
		
		else 
		{
			_screenmanager.DisableInput();
			_shopscreen.DisableInput();
		}
	}
}
