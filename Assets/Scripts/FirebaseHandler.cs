﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseHandler : MonoBehaviour {
	public enum eFireBaseStates
	{
		GameLaunch,
		DumaduMainMenu,
		MsportsMainMenu
	};
	public eFireBaseStates _eFireBaseStates;
	public static FirebaseHandler Instance;
	// Use this for initialization
	void Start () {
		
		Instance = this;
		FireBaseAnalytics();

	}
	
	void FireBaseAnalytics()
	{

		switch(_eFireBaseStates)
		{

		    case eFireBaseStates.GameLaunch:
			if(PlayerPrefs.GetInt("FirstGameLaunch") == 0)
			{
				PlayerPrefs.SetInt("FirstGameLaunch",1);
			}
			break;
		    case eFireBaseStates.DumaduMainMenu:
			if(PlayerPrefs.GetInt("FirstDumaduMainMenu") == 0)
			{
				PlayerPrefs.SetInt("FirstDumaduMainMenu",1);
			}
			break;
		    case eFireBaseStates.MsportsMainMenu:



			if(PlayerPrefs.GetInt("FirstMsportsMainMenu") == 0)
			{
				PlayerPrefs.SetInt("FirstMsportsMainMenu",1);
			}
			break;


		}
	}
}