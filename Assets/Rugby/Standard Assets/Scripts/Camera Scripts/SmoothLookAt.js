var target : Transform;
var damping = 5.0;
var smooth = true;
var rotation:Quaternion;
@script AddComponentMenu("Camera-Control/Smooth Look At")

function LateUpdate () {
	if (target) {
		if (smooth)
		{
			// Look at and dampen the rotation
//			if(target.transform.position.y>transform.position.y)
			  rotation = Quaternion.LookRotation(Vector3(0,transform.position.y,target.transform.position.z)- Vector3(0,transform.position.y,transform.position.z));
//			if(target.transform.position.y<transform.position.y)
//			  rotation = Quaternion.LookRotation(Vector3(0,target.transform.position.y,target.transform.position.z)- Vector3(0,transform.position.y,transform.position.z));
			 transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
		}
		else
		{
			// Just lookat
		    transform.LookAt(target);
		}
	}
}

function Start () {
	// Make the rigid body not change rotation
   	if (GetComponent.<Rigidbody>())
		GetComponent.<Rigidbody>().freezeRotation = true;
}