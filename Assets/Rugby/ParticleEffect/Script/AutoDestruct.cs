using UnityEngine;
using System.Collections;

public class AutoDestruct : MonoBehaviour {
	
	public static AutoDestruct instance = null;
	
	public float time;
	
	public bool isAlpha;
	
	
	void Awake()
	{
		instance = this;
	}
	
	// Use this for initialization
	void Start () 
	{
		//SetColor(0.5f , 0.5f , 0.5f , 1);
		
		
		
		StartCoroutine(AutoDestroy());
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(isAlpha)
		{
			Color tempcolor = this.GetComponent<Renderer>().material.color;
			tempcolor.a--;
			this.GetComponent<Renderer>().material.color = tempcolor;
		}
	
	}
	
	IEnumerator AutoDestroy()
	{
		yield return new WaitForSeconds(time);
		Destroy(this.gameObject);
	}
	
	public void SetColor(float r , float g , float b , float a)
	{
		Color c = GetComponent<Renderer>().material.GetColor("_TintColor");
		c = new Color(r , g ,b, a);
		GetComponent<Renderer>().material.SetColor("_TintColor", c);
	}
}
