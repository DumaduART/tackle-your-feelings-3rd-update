using UnityEngine;
using System.Collections;

public class BallFollowParticle : MonoBehaviour {

	// Use this for initialization
	GameObject target;
	
	public Vector3 localPos;
	Vector3 refDir;
	InputManager _inputManager;
	void Start () {
			_inputManager = GameObject.Find("GameManager").GetComponent<InputManager>();
			target = _inputManager.GetRefOfBall();
		transform.rotation = target.transform.rotation;
		transform.parent = target.transform;
		//transform.localPosition = localPos;
		transform.parent = null;
		refDir = target.transform.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(target!=null)
		{
			transform.position = target.transform.position - refDir;
			//transform.rotation = target.transform.rotation;
		}
		else
		{
			Destroy(this.gameObject);
		}
		
	}
}
