using UnityEngine;
using System.Collections;

public class ScoreIncreaser : MonoBehaviour {

	// Use this for initialization
    GUIText scoringtext; 
	GameManager gameManager;
	public Vector3 targetPos;
	float tempSize;
	public int targetfontsize = 46;
	public float initialTargetFontSize = 100.0f;
	public bool isTriger;
	private float startTime;
	void Start () {
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		scoringtext = gameObject.GetComponent<GUIText>();
		switch (gameManager.GetGameMode())
		{
			case GameManager.GameMode.ClockShowDown:
			{
				scoringtext.text = "+" + ScoreManager.instance.GetCurrentShotScore().ToString();
				break;
			}
			
			case GameManager.GameMode.SuddenDeath:
			{
				scoringtext.text = "+" + ScoreManager.instance.GetCurrentShotScore().ToString();
				break;
			}
			
			case GameManager.GameMode.SkillShots:
			{
				break;
			}
		}
		
		StartCoroutine(InitialAnimation());
	}
	
	// Update is called once per frame
	void Update () {
	
		if(isTriger)
		{
			if(Time.time-startTime>.5f)
			{
				StartCoroutine( TextAnimation());
				isTriger = false;
			}
		}
	}
	
	private IEnumerator TextAnimation()
	{
		while(transform.position!=targetPos)
		{
			transform.position = Vector3.MoveTowards(transform.position,targetPos,1*Time.deltaTime);
			tempSize += 2;
			if(scoringtext.fontSize > targetfontsize)
			scoringtext.fontSize -= (int)(tempSize);
			yield return new WaitForEndOfFrame() ;
			
		}
		Destroy(this.gameObject);
		GuiTextManager.instance.UpdateText();
	}
	
	private IEnumerator InitialAnimation()
	{
		 float temp = scoringtext.fontSize;
		while(temp<initialTargetFontSize)
		{
			temp =  Mathf.MoveTowards(temp,initialTargetFontSize,10);
			scoringtext.fontSize = (int)temp;
			yield return new WaitForEndOfFrame() ;
		}
		
		isTriger = true;
		startTime = Time.time;
	}
}
