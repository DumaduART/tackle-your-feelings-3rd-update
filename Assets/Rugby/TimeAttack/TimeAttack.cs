using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class TimeAttack : MonoBehaviour {

	// Use this for initialization
	public enum HittedStatus
	{
		notHitted=0,
		hitted
	};
	
	public enum FlashingStatus
	{
		flashing=0,
		notflashing
	};
	public FlashingStatus flashingStatus;
	public HittedStatus _hittedStatus;
	public static TimeAttack instance;
	private float countdown;
    private float timer;
    private bool checkTime = true;
	private float count;
	private float countdowntime;
	public int mitotalTime = 0;
	private static int mitimeLeft=1;
	public float mistartTime;
	private bool mbCheckTime=false;
	public int miTimer;
	public GameObject[] bars; 
	private List<GameObject> barscopy= new List<GameObject>();
	public List<GameObject> generationPattern= new List<GameObject>();
	public List<GameObject> hittedPattern= new List<GameObject>();
	private int nOfHittedBar=0;
	private int miwindeffect;
	private float mistopedTime;
	private bool isRoundUp=false;
	public bool isFlashingZone = false;
	private int mitimeBonus;
	private int score;
	public int blinkNo = 0;
	private int generationIndex = 0;
	private BallHandler _ballHandler;
	private GameManager _gamemanager;
	public static int noOfRoundUp;
	public static int noOfAttempts;
	public int multiplier = 1;
	public bool isGoal;
	public int continuousGoal;
	public int continuousMissed;
	public  int goalStreak = 0;
	public enum WindType
	{
		
		right=0,
		left
	}	
	public WindType _windType;
	private InputManager _inputManager;
	private int dist=0;
	private Vector3 ballPos=new Vector3(-4,-18.5f,90);
	
	
	public int time ;
	public int waitingTime;
	public GameObject flashingPlane;
	private GameObject flashingPlaneClone;
	
	private float startTime1;
	private float totaltimeplayed;
	
	private bool isTimeMessage = true;
	
	public AudioSource _timerAudioSource;
	public AudioClip _timerClip;
	
	int miPrevoiusTimeLeft;
	int miPrevoiusPlayTime;
	
	InstructionManager instructionManager;
	
	public int _TimerCount;
	public static bool _ReviveForTimeMode; 

	void Awake()
	{
		 mitimeLeft=1;
		 instance = this;
	}
	
	void Start () {
		
		_inputManager=GameObject.Find("GameManager").GetComponent<InputManager>();
		_ballHandler= GameObject.Find("GameManager").GetComponent("BallHandler") as BallHandler ;
		_gamemanager= GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		noOfRoundUp = 0;
	   mistartTime=(Time.time);
	   startTime1 = Time.time;
	   timer = Time.time;
	   mitimeLeft=1;
	   for(int i=0 ; i<bars.Length ; i++)
		{
			barscopy.Add(bars[i]);
		}
		StartCoroutine(FlashZone(0.3f));
		
		EventManager.Pause += Pause;
		EventManager.Resume += Resume;
		
		instructionManager = InstructionManager.GetInstance();
	}

	void OnDestroy()
	{
		EventManager.Pause -= Pause;
		EventManager.Resume -= Resume;
	}
	// Update is called once per frame
	void Update () {		
		
		if(mbCheckTime)
		{
			miTimer=(int)(Time.time-mistartTime);
			mitimeLeft = mitotalTime-miTimer;
			
			_TimerCount = miTimer;
				
			/*if(mitimeLeft != miPrevoiusTimeLeft && mitimeLeft < 10 && Mathf.Abs(miPrevoiusTimeLeft - mitimeLeft) >= 1)
			{
			   //_timerAudioSource.Play();
				
				AudioSource.PlayClipAtPoint(_timerClip,Vector3.zero);
				
			   //miPrevoiusPlayTime = (int)(Time.time);
				
				miPrevoiusTimeLeft = mitimeLeft;
				
			}*/
			
			
			if(_TimerCount == 1200)
				_gamemanager.updateAchievements (9);
			
			if(_TimerCount == 2400)
				_gamemanager.updateAchievements (10);
			
		}
		
		if(mitimeLeft<=0)
		{
			mbCheckTime=false;
		}
		
		if(mitimeLeft==10 && isTimeMessage)
		{
			instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"THE CLOCK IS TICKING FAST",true);
			isTimeMessage = false;
		}
		if (mitimeLeft <= 0 ) {
			
			if (_inputManager.GetRefOfBall () == null) {
				TimeAttackScreenGUIItemsManager.instance.StopAllActions ();

				_gamemanager.BeforeGameOverCondition ();

				this.enabled = false;
				ChangeTimerCondition (true);
				
			}
			if ((_inputManager.GetRefOfBall () != null && _inputManager.GetInputStatus () == InputManager.InputStatus.idle)) {
				TimeAttackScreenGUIItemsManager.instance.StopAllActions ();

				_gamemanager.BeforeGameOverCondition ();

				ChangeTimerCondition (true);

				this.enabled = false;
				ChangeTimerCondition (true);

			}


		}
		//else if(mitimeLeft <= 0 && _ReviveForTimeMode == true)
		//{		
			
//			TimeAttackScreenGUIItemsManager.GetBallSelectionClone().GetComponent<ScreenManager>().closeScreenManager();
//
//			TimeAttackScreenGUIItemsManager.instance.StopAllActions ();
//
//				this.enabled = false;
//				GetMoreTimeGUIItemsManager.mbGameOverScreen = false;
//				EventManager.GameOverTrigger();
		//}
	}
	
	public static int GetTimeLeft()
	{
		return(mitimeLeft);
	}
    IEnumerator FlashZone(float time)
	{
		
	   	int index=Random.Range(0,barscopy.Count);
		GameObject obj=Instantiate(barscopy[generationIndex]) as GameObject;// replace index by generationIndex
		 obj.GetComponent<BarAnimation>().EntryAnimation();
		EventManager.ActiveObjects.Add(obj);
		generationPattern.Add(obj);
	//	barscopy.Remove(barscopy[generationIndex]);  // replace index by generationIndex
		yield return new WaitForSeconds(time);
		if(generationIndex <= 3)
		{		
			  generationIndex++;
			  StartCoroutine(FlashZone(0.3f));
			 
			  
		}
		else
		{
			if(flashingPlaneClone == null)
			flashingPlaneClone = Instantiate(flashingPlane) as GameObject;
			EventManager.ActiveObjects.Add(flashingPlaneClone);
			BlinkZone(blinkNo);
			ActivateFlashingZone(true);
		}
	}
	public void SetHittedPattern(string name)
	{
//		hittedPattern.Add(name);
		nOfHittedBar +=1;
		if(nOfHittedBar==5)
		{
			ChkBarHittedPattern();
			SetRoundUpCondition(true);
		}
	}
	public void SetTime(int time)
	{
		mitotalTime += time;
		mitimeLeft  += time;
	}
	
	public void SetContinuousGoal()
	{ 
		if(isGoal)
		{
			continuousGoal++;
			continuousMissed = 0;
			if(continuousGoal == 3)
				instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"AMAZING, YOU SURELY KNOW HOW TO \n IMPRESS THE AUDIENCE DON’T YOU",true);
				
		}
		
		else
		{
			continuousGoal = 0;
			continuousMissed ++;
			if(continuousMissed == 3)
			instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"RELAX, TAKE A DEEP BREADTH, FOCUS, AND \n TRY AGAIN. DON’T LOSE HOPE",true);
		}
		
		isGoal = false;
	}
	
	public void ChkBarHittedPattern()
	{
//		for(int i=0;i<generationPattern.Count;i++)
//		{
//				
//			if(generationPattern[i]==hittedPattern[i])
//			{
//				if(i==4)
//				{
//					SetRoundUpParameter();
//					
//				}
//			
//				continue;
//			}
//			else
//			{
//				print ("roundUp");
//				SetTime(5);
//				SetRoundUpParameter();
//				return;
//			}
//		}
//		SetTime(10);
		
		SetTime(5);
		SetRoundUpParameter();
	}
	
	
	public void ChangeBlinkZone(GameObject hittedObj)
	{
		noOfAttempts++;
//		print ("Attempts" + noOfAttempts);
		ActivateFlashingZone(false);
		if(flashingStatus == FlashingStatus.notflashing)
			return ;
		if(blinkNo<=3)
		{
			if(generationPattern[blinkNo] == hittedObj)
			{
				multiplier ++;
				blinkNo++;
				BlinkZone(blinkNo);
				isFlashingZone = true;
			}
			
			else
			{
				flashingStatus = FlashingStatus.notflashing;
				//generationPattern[blinkNo].GetComponent<OffSetManager>().enabled = false;
				//flashingPlaneClone.SetActive(false);
				isFlashingZone = false;
			}
		}
		
		else
		{
			multiplier ++;
			isFlashingZone = true;
		}
			
	}
	
	public static void SetNoOfAttempts()
	{
		noOfAttempts++;
		
	}
	
	public static TimeAttack GetInstance()
	{
		return(instance);
	}
	
	public WindType GetWindType()
	{
		return(_windType);
	}
	
	public void ActivateFlashingZone(bool x)
	{
		if(flashingPlaneClone == null)
			return ;
		if(x)
			flashingPlaneClone.SetActive(true);
		else
			flashingPlaneClone.SetActive(false);
	}
	
	public void BlinkZone(int x)
	{
		//generationPattern[x].GetComponent<OffSetManager>().enabled = true;
		if(flashingPlaneClone==null)
			return ;
		Vector3 temp = flashingPlaneClone.transform.position;
		temp.x = generationPattern[x].transform.position.x;
		flashingPlaneClone.transform.position = temp;
		
	}
	
	public void SetRoundUpParameter()
	{
		blinkNo = 0;
		//isFlashingZone = false;
		generationIndex = 0;
		flashingStatus = FlashingStatus.flashing;
		nOfHittedBar=0;
		miwindeffect +=1;
		noOfRoundUp +=1;
		dist +=1;
		_inputManager.SetWindEffect(miwindeffect);
		for(int i=0 ; i<bars.Length ; i++)
		{
			barscopy.Add(bars[i]);
		}
		
		generationPattern.Clear();
		hittedPattern.Clear();
		StartCoroutine(delay(1.0f));
		Destroy(flashingPlaneClone);
		
		_gamemanager.updateAchievements (6);
	}
	IEnumerator delay(float time)
	{
		yield return new WaitForSeconds(time);
		StartCoroutine(FlashZone(.2f));
	}
	public  int GetWind()
	{
		switch(_windType)
		{
			case WindType.right:
			{
				return(miwindeffect);	
			}
			case WindType.left:
			{
				return(-miwindeffect);
			}
		   	default:
				return(0);
		}
	}
	public void ChangeWindDirection()
	{
		int x = Random.Range (0,2);
		switch(x)
		{
			case 0:
			{
				_windType=WindType.left;
				break;
			}
			case 1:
			{
				_windType=WindType.right;
				break;
			}
		}
		if(TimeAttackScreenGUIItemsManager.instance)
		TimeAttackScreenGUIItemsManager.instance.ChangeWindImage(_windType);
	}
	public void ChangeTimerCondition(bool x)
	{
		if(x)
		{
			mbCheckTime=false;
			mistopedTime=Time.time;
		}
		else
		{

			float startTime=Time.time;
			float delay = (startTime-mistopedTime);
			mistartTime = mistartTime+delay;
			mbCheckTime=true;
		}
	}
	public void ChangeBallPosition()
	{
		GameObject ball= _inputManager.GetRefOfBall();
		 ballPos = ball.transform.position;
		ballPos.z = ballPos.z-dist;
		ballPos.x = Random.Range(-14,10);
		//ball.transform.position = ballPos;
	}
	public int GetMultiplier()
	{
		return(multiplier);
	}
	
	public int GetRoundUpValue()
	{
		return(noOfRoundUp);
	}
	
	public Vector3 GetBallPos()
	{
		
		return(ballPos);
	}
	public bool ChkRoundComdition()
	{
		return(isRoundUp);
	}
	public void SetRoundUpCondition(bool x)
	{
		if(x)
			isRoundUp=true;
		else
			isRoundUp=false;
		
	}
	public void SetScore(int x)
	{
		score=x;
		isGoal = true;
	}
	
	public  int GetScore()
	{
		return(score);
	}
	
	public bool GetFlashingZoneCondition()
	{
		return(isFlashingZone);
	}
	
	public void SetHittedStatus(HittedStatus hittedstatus)
	{
		switch(hittedstatus)
		{
			case HittedStatus.hitted:
			{
				_hittedStatus=HittedStatus.hitted;
				break;
			}
			case HittedStatus.notHitted:
			{
				_hittedStatus=HittedStatus.notHitted;
				break;
			}
		}
	}
	
	public HittedStatus GetHittedStatus()
	{
		return(_hittedStatus);
	}
	private void Pause()
	{
		if(_hittedStatus==HittedStatus.notHitted)
		ChangeTimerCondition(true);
	}
	private void Resume()
	{
		if(_hittedStatus==HittedStatus.notHitted)
		ChangeTimerCondition(false);
	}
	
	public void  SeChkTimeCondition(bool x)
	{
		mbCheckTime = x;
		mistartTime = Time.time;
	}
	
	public void SeTcondition(bool x)
	{
		mbCheckTime=x;
	}
	
	public int GetNoOfRound()
	{
		return(noOfRoundUp);
	}
	
	
	public void ChkForStatsUpdate()
	{
		
		StatsData statsData = _gamemanager.GetStatsData();
		//print (ScoreManager.instance.CurrentScore());
		if(statsData._clockShowDownData._highestScore<ScoreManager.instance.CurrentScore())
		{
			
			statsData._clockShowDownData._highestScore = ScoreManager.instance.CurrentScore();
			_gamemanager.UpdateIntDataInDevice("_clockShowDownData._highestScore",statsData._clockShowDownData._highestScore,false);
		}
		
		if(statsData._clockShowDownData._highestMultiplier<multiplier)
		{
			statsData._clockShowDownData._highestMultiplier = multiplier;
			_gamemanager.UpdateIntDataInDevice("_clockShowDownData._highestMultiplier",statsData._clockShowDownData._highestMultiplier,false);
		}
		
		if(statsData._clockShowDownData._highestRoundsCleared<noOfRoundUp)
		{
			statsData._clockShowDownData._highestRoundsCleared = noOfRoundUp;
			_gamemanager.UpdateIntDataInDevice("_clockShowDownData._highestRoundsCleared",statsData._clockShowDownData._highestRoundsCleared,false);
		}
		
		if(statsData._clockShowDownData._longestSession<(int)(Time.time-startTime1))
		{
			//print ("longestSession");
			statsData._clockShowDownData._longestSession = (int)(Time.time-startTime1);
			_gamemanager.UpdateIntDataInDevice("_clockShowDownData._longestSession",statsData._clockShowDownData._longestSession,false);
		}
		
		
	}
}
