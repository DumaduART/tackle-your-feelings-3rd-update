using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventManager : MonoBehaviour {

	// Use this for initialization
//	public static EventManager instance;
	public delegate void GameEvent();
	
	public static event GameEvent GameStart,Home, GameOver,Resume,Pause,ReStart;
	
	static List<GameObject> _activeObjects = new List<GameObject>();
	public static List<GameObject> ActiveObjects
	{
		set
		{
			
		}
		get
		{
			return _activeObjects;
		}
	}
	
	public GameManager _gameManager;
	public InputManager _inputManager;
	public BallHandler _ballHandler;
	public GameObject _ballStand;
	public GameObject pole;
	private Camera _camera;
    public static GameObject practice;
	public Vector3 ballEulerAngel;
	public float rightRestriction = 345;
	public float leftRestriction= 10;
	public float left;
	public float right;
	
	private  float min = 20 ;
	private float max = 20;
	private Quaternion minQuaternion;
	private Quaternion maxQuaternion ;
	Quaternion currentRotation;
	float range;

	void Start () 
	{
		//instance=this;
		//_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_camera =_inputManager.GetCameraref();
	}
	
	// -------------------------Giving force to the ball----------------------------
    public	void AddForceToball(GameObject ball,float velocity,Vector3 balldir)
	{
//		if(_gameManager.GetGameMode()!= GameManager.GameMode.Training)
//			if(BallSelectionSubScreenManager.instance!=null)
		//BallSelectionSubScreenManager.instance.ReduceBallCount();
		SoundManager.instance.BallThrowingEvent();
		RotationConstraint rotConstraint = ball.GetComponent<RotationConstraint>();
		if(rotConstraint != null)
			Destroy(rotConstraint);
		if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
		{

			practice = GameObject.Find("PracticeGameplay(Clone)"); 
			if(practice != null)
				practice.SetActive(false);
		}
		ball.GetComponentInChildren<TrailRenderer>().enabled = true;
	    if(velocity<5)
		{
			velocity=11;
//			ball.rigidbody.maxAngularVelocity=10;
//			_camera.GetComponent<CameraMovement>().SetMoveCondition(true);
		}
		
	    _camera.transform.parent=null;
		_ballStand.transform.parent=null;
		ball.transform.forward=new Vector2(balldir.x,balldir.z);
		ball.transform.forward=balldir;
		ball.GetComponent<Rigidbody>().useGravity=true;
		ball.GetComponent<Rigidbody>().velocity=velocity*balldir;
		ball.GetComponent<Rigidbody>().rotation=Quaternion.identity;
		_camera.GetComponent<CameraMovement>().enabled=true;
		_camera.GetComponent<CameraMovement>()._gtarget=ball;
		_camera.GetComponent<CameraMovement>().CameraSpeed(ball.GetComponent<Rigidbody>().velocity.magnitude);
		_camera.GetComponent<CameraMovement>().SetCameraObjPos(ball.transform.position);
		 if(velocity>10)
		 {
			_inputManager.SetWindEffect(_gameManager.GetWind());
			_gameManager.SetAirDrag();
			_inputManager.GetRefOfBall().GetComponent<CollisionDetection>()._flyStatus = CollisionDetection.flyStatus.air;
			ball.GetComponent<Rigidbody>().AddTorque(-20*ball.transform.right);
		 	ball.GetComponent<Rigidbody>().maxAngularVelocity=20;
		
		 }
		 else
		 {
			ball.GetComponent<Rigidbody>().maxAngularVelocity=10;
			_camera.GetComponent<CameraMovement>().SetMoveCondition(true);
		 }
		if(_gameManager.GetGameMode()==GameManager.GameMode.SkillShots)
		{
			LevelHandler levelHandler = LevelHandler.instance;
			if(levelHandler.GetGameOverParameter()==LevelHandler.GameoverParameter.ball)
			{
				levelHandler.UpDateRemaingBall(levelHandler.RemainingBall()-1);
				levelHandler.SetCurrentAttempts();
			}
		}
		
		
	}
	IEnumerator Delay(float time)
	{
		yield return new WaitForSeconds(time);
	    GameObject refOfBall=_inputManager.GetRefOfBall();
		_ballHandler.GenerateBall(_camera,refOfBall);	
	}
//------------------------Rotation of environment for Device-------------------------	
	
	public  void SetEuelerAngel(Vector3 angel)
	{   if(angel.y>180)
		{
			//rightRestriction = rightRestriction-(360-angel.y);
			//leftRestriction =10;
			rightRestriction = Mathf.Abs( (angel.y-right));
			leftRestriction =  angel.y + left;
		}
		if(angel.y<180)
		{
			rightRestriction = angel.y-right;	
			leftRestriction =  angel.y + left;
		}
		ballEulerAngel = angel;
		RotationConstraint rotConstraint = _inputManager.GetRefOfBall().GetComponent<RotationConstraint>();
		Destroy(rotConstraint);
		rotConstraint = null;
		if(rotConstraint == null)
			rotConstraint = _inputManager.GetRefOfBall().AddComponent<RotationConstraint>();
		rotConstraint.min = -10;
		rotConstraint.max = 10;
		rotConstraint.axis = RotationConstraint.ConstraintAxis.Y;
		//rotConstraint.enabled = false;
		
	}
public void RotationIos(GameObject cameraObj)
{
    //  Touch touch;
	if(cameraObj==null)
			return ;
	 foreach(Touch touch in Input.touches)
	 {
	 	if(Input.touchCount==1)
	 	{ 
	 	 	if(touch.phase == TouchPhase.Ended) 
	 		{
	 			break;
	 		}
	 		if(touch.phase == TouchPhase.Moved)
	 		{	
				float speed= 40.0f;
	 			float cons=0.0f;
	 			if(touch.deltaPosition.y > cons)
	 			{
	 				if(cameraObj.transform.eulerAngles.x <=10||cameraObj.transform.eulerAngles.x >=180) 
					{
		            	cameraObj.transform.RotateAround (cameraObj.transform.position,
						cameraObj.transform.right,  speed* Time.deltaTime);
					}
	 			}
	 			if(touch.deltaPosition.y <- cons)
	 			{
	 				if(cameraObj.transform.eulerAngles.x>= 359||cameraObj.transform.eulerAngles.x<=180)
					{
						cameraObj.transform.RotateAround (cameraObj.transform.position, 
						-cameraObj.transform.right, speed* Time.deltaTime);
					}
	 			}
	 			if(Mathf.Abs(touch.deltaPosition.x) > cons)
				{		
		 			if(touch.deltaPosition.x > cons)
		 			{
			 				cameraObj.transform.RotateAround (cameraObj.transform.position, 
							-Vector3.up, speed* Time.deltaTime);
		 			}
		 			if(touch.deltaPosition.x <- cons)
		 			{		
				          cameraObj.transform.RotateAround (cameraObj.transform.position,
						   Vector3.up,  speed* Time.deltaTime);
		 			}
				}
	 		}
	 		
	 	}
	 }
		
}
//-------------------------------------Rotation of environment for pc --------------------------
public void Rotation(GameObject cameraObj)
{    
	if(cameraObj==null)
			return ;
     float speed=20;  
     float cons=.3f;	
		if(Input.GetAxis("Mouse Y") > cons)
	 {
	 	 if(cameraObj.transform.eulerAngles.x <=10||cameraObj.transform.eulerAngles.x >=180) 
		 {
		   cameraObj.transform.RotateAround (cameraObj.transform.position,
		  cameraObj.transform.right,  speed* Time.deltaTime);
		 }		
	 }
	 if(Input.GetAxis("Mouse Y") <-cons)
	 {
	   	 if(cameraObj.transform.eulerAngles.x>= 359||cameraObj.transform.eulerAngles.x<=180)
		 {
		  cameraObj.transform.RotateAround (cameraObj.transform.position, 
		  -cameraObj.transform.right, speed* Time.deltaTime);
		 }
	 }
	
	if(Mathf.Abs(Input.GetAxis("Mouse X")) > cons)
	{
		 if(Input.GetAxis("Mouse X") > cons)
		 {
			 {
			 	cameraObj.transform.RotateAround (cameraObj.transform.position, 
				-Vector3.up, speed* Time.deltaTime);
			 }		
		 }
		 if(Input.GetAxis("Mouse X") < -cons)
		 {
			{		
				cameraObj.transform.RotateAround (cameraObj.transform.position, 
				Vector3.up,  speed* Time.deltaTime);
			}
		 }
	}
}
	public static void GameStartTrigger()
	{

			GameStart();
			PopUpManager.instance.CreatePopUp(PopUpType.go);
			
	}
	
	public static void GameOverTrigger()
	{
		
			GameOver();

	}
	
	public static void GamePauseTrigger()
	{
		if(Pause != null)
		{
			Pause();
		}
		
	}
	 
	public static void GameReStartTrigger()
	{
		if(ReStart != null)
		{
			if(ActiveObjects.Count > 0)
			{
				foreach(GameObject obj in ActiveObjects)
				{
					if(obj != null)
					{
						Destroy(obj);
					}
				}
			}
			ActiveObjects.Clear();
			ReStart();
		}
		ScoreManager.instance.currentScore =0;
		PopUpManager.instance.CreatePopUp(PopUpType.go);
	}
	
	public static void GameHomeTrigger()
	{
		SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gui);
		
		if(Home != null)
		{
			
			if(ActiveObjects.Count > 0)
			{
				foreach(GameObject obj in ActiveObjects)
				{
					if(obj != null)
					{
						Destroy(obj);
					}
				}
			}
			ActiveObjects.Clear();
			Home();
			ScoreManager.instance.currentScore =0;
		}
	
	}
    public static void GameResumeTrigger()
	{
		if(Resume != null)
		{
			Resume();
			
		}
	}
	
	public static void GameUITrigger()
	{
		if(ActiveObjects.Count > 0)
		{
			foreach(GameObject obj in ActiveObjects)
			{
				if(obj != null)
				{
					Destroy(obj);
				}
			}
		}
		ActiveObjects.Clear();
		Pause = null;
		Resume = null;
	}
}
