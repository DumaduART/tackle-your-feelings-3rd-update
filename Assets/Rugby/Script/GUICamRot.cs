using UnityEngine;
using System.Collections;

public class GUICamRot : MonoBehaviour 
{



 public Transform myTransform ;
 Vector3 startposition ;
 Vector3 ellipseCenter ;
 float startAngle;

 float camSpeed = 10;
	
	
	public static GUICamRot instance;
	// Use this for initialization
	void  Start ()
	{
		
		instance = this;
		startposition= new Vector3(0,.7f,-3);
		startAngle= 0.0f;
		ellipseCenter= new Vector3(0,.7f,2);
		myTransform.position=startposition;
		
	
	}
	
	
	// Update is called once per frame
	void  FixedUpdate ()
	{
		
		startAngle += camSpeed*Time.deltaTime;
		if(startAngle>360.0f)
			startAngle=0.0f;
		Vector3 dir = Quaternion.Euler(5,startAngle,1)*Vector3.right;
		myTransform.position = ellipseCenter + dir*( (5*6)/
		                     (Mathf.Sqrt( (6*Mathf.Cos(Mathf.Deg2Rad*startAngle)*(6*Mathf.Cos(Mathf.Deg2Rad*startAngle)) ) + (5*Mathf.Sin(Mathf.Deg2Rad*startAngle)*(5*Mathf.Sin(Mathf.Deg2Rad*startAngle)) ))));
		
	
		 myTransform.rotation = Quaternion.LookRotation((new Vector3(0,0.5f,10) - myTransform.position) );
	}

}
