﻿using UnityEngine;
using System.Collections;

public class ChallengeInstructor : MonoBehaviour {
	
	public string _instructionText = "Write insctruction here";
	
	public GameObject _instructionScreen;
	
	GameObject mInstruction;
	// Use this for initialization
	void Start () {
	
		mInstruction = (GameObject)GameObject.Instantiate(_instructionScreen,_instructionScreen.transform.position,Quaternion.identity);
		
		SetChallengeText mSetChallengeText = mInstruction.GetComponent<SetChallengeText>();
		
		mSetChallengeText.SetText(_instructionText);
	}
	
	
}
