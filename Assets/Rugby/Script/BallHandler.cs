using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallHandler : MonoBehaviour {

	public GameObject[] ballPrefab;
	public GameObject _ballStand;
	public GameObject target;
	private Vector3 pos;
	private GameManager _gameManager;
	private GUIManager _guimanager;
	private GameObject mgBall;
	private Rect mrsliderRect;
	private InputManager inputManager;
	private CameraMovement _cameraMovement;
	private Vector3 initialVector;
	private Vector3 initialBallPos;
	public List<GameObject> ring = new List<GameObject>();
	BallSelectionManager ballSelectionmanager;
	// Use this for initialization
	void Start () {
	
	ballSelectionmanager = BallSelectionManager.GetInstance();
	pos=new Vector3(-4.0f,-18.57f,90.0f);	
	_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
	inputManager=GameObject.Find("GameManager").GetComponent("InputManager") as InputManager;
	_guimanager= GameObject.Find("GameManager").GetComponent("GUIManager") as GUIManager;
	_cameraMovement=GameObject.Find("Main Camera").GetComponent<CameraMovement>();
	mgBall=inputManager.GetRefOfBall();
	mrsliderRect=new Rect(275.0f,42.0f,18.0f,394.0f);
	initialVector=(target.transform.position-new Vector3(-4,-18.57f,90));
	initialBallPos=new Vector3(-4,-18.57f,75);
	EventManager.Home += Home;
	}

	void OnDestroy()
	{
		EventManager.Home -= Home;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButton(0) && !inputManager.GetRotationCondition() &&
			inputManager.GetInputStatus() !=InputManager.InputStatus.capturingSwing)
		{
			if(_gameManager.GetGameMode()==GameManager.GameMode.Training)
			{
				 pos= mgBall.transform.position;
				 pos.z=_gameManager.GetDistanceValue() ;
				 mgBall.transform.position=pos;
				 _cameraMovement.ChangeCameraDirection(mgBall);	
			}
		}
	}
	public void UpdateTexture()
	{
		for(int i= 0 ; i<ballSelectionmanager._ballType.Length;i++)
		{
			if(ballSelectionmanager._ballType[i].isSelected)
			{
				inputManager.GetRefOfBall().transform.GetChild(0).GetComponent<Renderer>().material.SetTexture("_MainTex",ballSelectionmanager._ballType[i].ballTexture);
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) 
				inputManager.SetBallParameter(ballSelectionmanager._ballType[i].speedInAir,ballSelectionmanager._ballType[i].windResistance
					,ballSelectionmanager._ballType[i].SwingAngel);
				break;
			}
		}
	}
	
	
	
	public void GenerateBall(Camera camera,GameObject ball ) // Ball Generator Function
	{
		if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
		{
			SuddenDeath suddenDeath = SuddenDeath.instance;
			suddenDeath.UpdateBall();
			StartCoroutine(suddenDeath.DestroyTarget(suddenDeath.GetTargetCloneLocalScale()));
			suddenDeath.GeneRateTarget();
		}
		
		
		
		if(_gameManager.ChkGameOverCondition()){
			
			SetSkillShotsLevelAssets();
			
			return ;
			
		}
		
		if(_gameManager.GetGameMode()==GameManager.GameMode.SkillShots)
		{
			SkillShots skillShot = SkillShots.instance;
			LevelHandler levelHandler = LevelHandler.instance;
//			if(levelHandler.GetGameOverParameter()==LevelHandler.GameoverParameter.ball)
//			{
//				levelHandler.UpDateRemaingBall(levelHandler.RemainingBall()-1);
//			}

			levelHandler.IncreaseWindEffect();
			if(_gameManager.GetCategory()== GameManager .Category.ThroughRings)
			{
				if(levelHandler.ChKLevelCompletion())
				{
					if(_gameManager.GetIndex() == 0)
					{
						InstructionManager instructionManager = InstructionManager.GetInstance();
						instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,
						"THATS JUST A FLUKE, DO IT TWICE \n AND PROVE ME",true);
					}
				}
				for(int i=0;i<levelHandler._arrLevelAssetHandlers.Length;i++)
				{
					LevelAssetHandler levelAssethndler= levelHandler._arrLevelAssetHandlers[i].GetComponent<LevelAssetHandler>();
					levelAssethndler.SetAssetCleared(true);
				}
			}
			for(int j=0;j< ring.Count;j++)
				{
					if(ring[j]!=null)
					ring[j].GetComponent<MeshRenderer>().enabled=true;
				}
			
			ring.Clear ();
			
			
		}
		
		Destroy(ball);
	//	ball=Instantiate(ballPrefab[0],pos,Quaternion.identity)as GameObject;
		ball=Instantiate(ballPrefab[0])as GameObject;
		mgBall=ball;
		EventManager.ActiveObjects.Add(ball);
		mgBall.transform.position = pos;
		initialBallPos=mgBall.transform.position;
		initialVector=(target.transform.position-mgBall.transform.position);
		inputManager.SetBallRef(ball);
		//inputManager.enabled=true;
		camera.transform.parent=ball.transform;
		_ballStand.transform.parent = ball.transform;
		_ballStand.transform.localPosition= new Vector3(.02f,-1.28f,.2f);
		_ballStand.transform.eulerAngles= new Vector3(270,0,0);
	//	camera.transform.localPosition=new Vector3(.25f,1.6f,-5.8f);
		//camera.transform.rotation= new Quaternion(0,0,0,0);
		camera.transform.eulerAngles=new Vector3(354,0,0);
		inputManager.ResetVelocity();
		inputManager.inputStatus=InputManager.InputStatus.idle;
		camera.enabled=true;
		_cameraMovement.SetMoveCondition(false);
		_cameraMovement.SetRotateCondition(true);
		
		if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown) // timer condition for time Attack mode
		{
			TimeAttack timeAttack=TimeAttack.instance;
			timeAttack.SetHittedStatus(TimeAttack.HittedStatus.notHitted);
			if(TimeAttackScreenGUIItemsManager.instance!=null)
			timeAttack.ChangeTimerCondition(false);
			
			if(timeAttack.GetFlashingZoneCondition())
			timeAttack.ActivateFlashingZone(true);
			if(timeAttack.ChkRoundComdition())
			{
				PopUpManager.instance.CreatePopUp(PopUpType.roundUp);
				timeAttack.ChangeBallPosition();
				timeAttack.SetRoundUpCondition(false);
				timeAttack.ChangeWindDirection();
				TimeAttackScreenGUIItemsManager.instance.isActivate = true;
			}
			ball.transform.position=timeAttack.GetBallPos();
			_cameraMovement.ChangeCameraDirection(ball);
			if(GuiTextManager.instance !=null)
			GuiTextManager.instance.UpdateText();
		}
		if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
		{
			_cameraMovement.ChangeCameraDirection(ball);
			
		}
		if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
		{
			SuddenDeath suddenDeath = SuddenDeath.instance;
			ball.transform.position = suddenDeath.ChangeBallPos();
			_cameraMovement.ChangeCameraDirection(ball);
			ParticleEffectManager.instance.GenerateParticle(ParticleEffectType.powerUpParticle,ball.transform.position);
			if(GuiTextManager.instance !=null)
			GuiTextManager.instance.UpdateText();
		}
		
		if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
		{
			LevelHandler levelHandler = LevelHandler.instance;
		   SkillShots.instance.ChangeballPosition();
		   Vector3 ballpos1 = levelHandler.GetBallPos();
		   ballpos1.y = mgBall.transform.position.y;
		   mgBall.transform.position = ballpos1;
		   _cameraMovement.ChangeCameraDirection(ball);
			if(!levelHandler.getTimerCondition())
			levelHandler.ChangeTimerCondition(false);
			if(_gameManager.GetCategory() == GameManager.Category.NoSwingStorm)
			{
				if(levelHandler.ChkRaondomiseCondition())
				{
					SkillShots.instance.ChangePosInZone();
				}
				if(PoleMovementRotation.instance)
				PoleMovementRotation.instance.ReSet();
			}
			if(GuiTextManager.instance !=null)
			GuiTextManager.instance.UpdateText();
		}
		
		_gameManager.ChangePoleColor(true);
		UpdateTexture();
	}
	
	public void SetSkillShotsLevelAssets()
	{
		if(_gameManager.GetGameMode()==GameManager.GameMode.SkillShots)
		{
			
				SkillShots skillShot = SkillShots.instance;
				LevelHandler levelHandler = LevelHandler.instance;
				if(levelHandler.GetGameOverParameter()==LevelHandler.GameoverParameter.ball)
				{
					
				
					for(int i=0;i<levelHandler._arrLevelAssetHandlers.Length;i++)
					{
						LevelAssetHandler levelAssethndler= levelHandler._arrLevelAssetHandlers[i].GetComponent<LevelAssetHandler>();
						levelAssethndler.SetAssetCleared(true);
					}
					
				}
			
			
		}
		
	}
	public void SetballPos(Vector3 modfPos)
	{
		pos=modfPos;
	}
	
	public int GetDistOfBall()
	{
		int dist=0;
		dist= (int)(target.transform.position.z-TimeAttack.instance.GetBallPos().z); /*(int)(Vector3.Distance(target.transform.position,initialBallPos))*/
		return(dist);
	}
	public int GetCurveAngel(Vector3 finalballpos)
	{
		int angel=0;
		Vector3 finalvector;
		finalvector=finalballpos-initialBallPos;
		finalvector.y=initialVector.y;
		angel=(int)(Vector3.Angle(initialVector,finalvector));
		return(angel);
	}
	
	public void setPosition()
	{
		pos=new Vector3(-4.0f,-18.57f,90.0f);	
	}
	
	private  void Home()
	{
		pos=new Vector3(-4.0f,-18.57f,90.0f);	
	}

}
