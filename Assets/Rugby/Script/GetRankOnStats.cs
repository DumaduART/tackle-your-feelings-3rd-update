﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GetRankOnStats : MonoBehaviour {
	
	public Vector3 _position;
	
	public Vector3 _otherPosition;
	
	public string _faceBookLeaderBoardKey = "";
	
	public GameObject _goTimeRank;
	
	Transform mTransform;
	// Use this for initialization
	void Start () {
	
		mTransform = transform;
		//OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK,"",CallBackforSuddenDeath,"SuddenDeathScore");
		
	}
	
	// Update is called once per frame
	void Update () {
	
		mTransform.localPosition = _position;
		
		_goTimeRank.transform.localPosition = _otherPosition;
	}
	
//	void CallBackforSuddenDeath(string strResult ,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null)
//	{
//		Debug.Log("===Login CallBack===");		
//		if(eRequestType == eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK)
//		{
//			gameObject.GetComponent<GUIText>().text = "Sudden Death Rank:"+strResult;
//			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK,"",CallBackforTime,"TimeScore");
//		}
//	}
//				
//	void CallBackforTime(string strResult ,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null)
//	{
//		Debug.Log("===Login CallBack===");		
//		if(eRequestType == eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK)
//		{
//			_goTimeRank.GetComponent<GUIText>().text = "Clock ShowDown Rank:"+strResult;			
//		}
//	}
				
}
