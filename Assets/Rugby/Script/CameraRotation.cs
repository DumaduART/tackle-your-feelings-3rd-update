using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour {

			float a =30.0f;
			float b =30.0f;
	       // Vector3 position; 
			int tapCount;
			public static CameraRotation instance;
			private bool rotation = true; 
			public  GameObject lookAtobject;
	        //Vector3 startPosition;
			Vector3 endPosition;
			void Start ()
			{
				instance = this;
				//startPosition = transform.position;
				EventManager.GameStart += GameStart ;
				EventManager.GameOver += GameOver ;
				EventManager.ReStart += ReStart ;
				EventManager.Home += Home ;
			}
			
	void OnDestroy()
	{
		EventManager.GameStart -= GameStart ;
		EventManager.GameOver -= GameOver ;
		EventManager.ReStart -= ReStart ;
		EventManager.Home -= Home ;
	}
			void Update ()
			{ 
				if(rotation)
				{
					Vector3 position; 
					transform.LookAt(lookAtobject.transform);
					position = transform.position;
					position.z= 60 + 30*Mathf.Sin(Time.time*.2f);
					position.x= 0 + a*Mathf.Cos(Time.time*.2f);
					position.y= -10;
			        transform.position = position;
			
				}
		
			}
	  
	private void GameStart()
	{
		this.enabled = false;
	}
	
	private void GameOver()
	{
		this.enabled = true;
	}
	
	private void ReStart()
	{
		this.enabled = false;
	}
	 
	private void Home()
	{
		this.enabled = true;
	}
}
