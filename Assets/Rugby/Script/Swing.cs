using UnityEngine;
using System.Collections;

public class Swing : MonoBehaviour {

	// Use this for initialization
	InputManager _inputManager;
	GameObject ball;
	float currentSwing;
	void Start () {
			_inputManager=GameObject.Find("GameManager").GetComponent<InputManager>();
			_inputManager.ReSetTargetSwing();
			ball = _inputManager.GetRefOfBall();
	}
	
	// Update is called once per frame
	void Update () {
		
		currentSwing = Mathf.MoveTowards(currentSwing,_inputManager.GetTargetSwing(),Time.deltaTime);
		if(Mathf.Abs( currentSwing)<Mathf.Abs( _inputManager.GetTargetSwing()))
		{
	        Vector3 velocityDirHorzComponent = ball.GetComponent<Rigidbody>().velocity;
			velocityDirHorzComponent.y = 0;
			Vector3 normal = Vector3.zero;
			normal = Quaternion.AngleAxis(90,Vector3.up)*velocityDirHorzComponent;
			normal = currentSwing * normal.normalized;
			velocityDirHorzComponent = velocityDirHorzComponent + normal;
			velocityDirHorzComponent.y = ball.GetComponent<Rigidbody>().velocity.y;
			ball.GetComponent<Rigidbody>().velocity = velocityDirHorzComponent;
		}
				
	
	}
}
