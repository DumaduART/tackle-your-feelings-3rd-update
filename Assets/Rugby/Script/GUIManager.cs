using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {

	// Use this for initialization
	public GameObject _ballStand;
	public GameObject pole;
	public SkillShots.Category _category;
	private GameManager _gameManager;
	private InputManager _inputManager;
	private BallHandler _ballHandler;
	private InstructionManager _instructionManager;
	public static GUIManager instance;
	public enum GuiState {HomeScreen,PauseScreen,GameOverScreen,PlayScreen,PracticeScreen
		,TimeAttackScreen,SkillShotScreen,PassItOnScreen,HitTheBar,ThroughRings,NoswingStorm,DodgeShoot};
	public GuiState currentUIState;
	public int index;
	int sizeX = 100;
	int sizeY = 50;
	int sizeY1=40;
	
	public GUITexture Bgtexture;
	void Awake()
	{
		instance = this;
		instance.enabled = false;
	}
	void Start () 
	{
		_ballHandler= GameObject.Find("GameManager").GetComponent("BallHandler") as BallHandler ;
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager=GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		EventManager.GameStart += GameStart;
		EventManager.GameOver += GameOver;
		EventManager.ReStart += ReStart;
		EventManager.Home += Home;
		_instructionManager = InstructionManager.GetInstance();
	}

	void OnDestroy()
	{
		EventManager.GameStart -= GameStart;
		EventManager.GameOver -= GameOver;
		EventManager.ReStart -= ReStart;
		EventManager.Home -= Home;
		EventManager.Pause -= Pause;
		EventManager.Resume -= Resume;
	}
	
	public GUITexture GetBgTexture()
	{
		return(Bgtexture);
	}
	
	// Update is called once per frame
//	void OnGUI () 
//	{
//		switch(currentUIState)
//		{
//			case GuiState.HomeScreen:
//				if(GUILayout.Button("Practice",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					Practice();
//				}
//				if(GUILayout.Button("TimeAttack",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					TimeAttack();
//				}
//				if(GUILayout.Button("SkillShot",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					SkillShot();
//				}
//				if(GUILayout.Button("PassItOn",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					PassItOn();
//				}
//			   break;
//			
//			case GuiState.PauseScreen:
//			if(GUILayout.Button("Resume",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{ 
//						EventManager.GameResumeTrigger();
//				}
//			if(GUILayout.Button("Home",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{ 
//						_ballStand.transform.parent=null;
//				        _inputManager.GetCameraref().transform.parent=null; 
//						EventManager.GameHomeTrigger();
//				}
//			  break;
//			
//			case GuiState.PlayScreen:
//			   if(GUILayout.Button("Pause",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{ 
//						EventManager.GamePauseTrigger();
//				}
//			  break;
//			
//			case GuiState.GameOverScreen:
//			if(GUILayout.Button("Restart",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{ 
//						EventManager.GameReStartTrigger();	
//	
//				}
//			if(GUILayout.Button("Home",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{ 
//					    _ballStand.transform.parent=null;
//				        _inputManager.GetCameraref().transform.parent=null; 
//						EventManager.GameHomeTrigger();
//				}
//			  break;
//			
//			
//		    case GuiState.PracticeScreen:
//			if(GUILayout.Button("Play",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{ 
//					EventManager.GameStartTrigger();
//				}
//			break;
//			
//			case GuiState.TimeAttackScreen:
//			if(GUILayout.Button("Play",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{ 
//					EventManager.GameStartTrigger();
//				}
//			break;
//			
//		    case GuiState.SkillShotScreen:
//				if(GUILayout.Button("HitTheBar",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					HitTheBar();
//				}
//				if(GUILayout.Button("TroughRings",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					ThroughRings();
//				}
//				if(GUILayout.Button("NoSwingStorm",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					NoswingStorm();
//				}
//				if(GUILayout.Button("DodgeShoot",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//				{
//					DodgeShoot();
//				}
//			break; 
//			
//			case GuiState.PassItOnScreen:
//				if(GUILayout.Button("Play",GUILayout.Width(sizeX),GUILayout.Height(sizeY)))
//					{ 
//						EventManager.GameStartTrigger();
//					}
//				break;
//			
//		 	case GuiState.HitTheBar:
//			
//				if(GUILayout.Button("Challenge1",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(0);
//				 		index=0;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge2",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(1);
//						index=1;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge3",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(2);
//						index=2;
//						EventManager.GameStartTrigger();
//					}
//			    if(GUILayout.Button("Challenge4",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(3);
//						index=3;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge5",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(4);
//						index=4;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge6",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(5);
//						index=5;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge7",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(6);
//						index=6;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge8",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(7);
//						index=7;
//						EventManager.GameStartTrigger();
//					}
//			  	if(GUILayout.Button("Challenge9",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//				
//						_gameManager.LoadChallenge(8);
//						index=8;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge10",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(9);
//						index=9;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge11",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(10);
//						index=10;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge12",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(11);
//						index=11;
//						EventManager.GameStartTrigger();
//					}
//				break;
//			
//			case GuiState.ThroughRings:
//				if(GUILayout.Button("Challenge1",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(0);
//				 		index=0;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge2",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(1);
//						index=1;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge3",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(2);
//						index=2;
//						EventManager.GameStartTrigger();
//					}
//			    if(GUILayout.Button("Challenge4",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(3);
//						index=3;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge5",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(4);
//						index=4;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge6",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(5);
//						index=5;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge7",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(6);
//						index=6;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge8",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(7);
//						index=7;
//						EventManager.GameStartTrigger();
//					}
//			  	if(GUILayout.Button("Challenge9",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//				
//						_gameManager.LoadChallenge(8);
//						index=8;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge10",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(9);
//						index=9;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge11",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(10);
//						index=10;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge12",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(11);
//						index=11;
//						EventManager.GameStartTrigger();
//					}
//				break;
//			
//			case GuiState.NoswingStorm:
//				if(GUILayout.Button("Challenge1",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(0);
//				 		index=0;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge2",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(1);
//						index=1;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge3",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(2);
//						index=2;
//						EventManager.GameStartTrigger();
//					}
//			    if(GUILayout.Button("Challenge4",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(3);
//						index=3;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge5",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(4);
//						index=4;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge6",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(5);
//						index=5;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge7",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(6);
//						index=6;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge8",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(7);
//						index=7;
//						EventManager.GameStartTrigger();
//					}
//			  	if(GUILayout.Button("Challenge9",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//				
//						_gameManager.LoadChallenge(8);
//						index=8;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge10",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(9);
//						index=9;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge11",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(10);
//						index=10;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge12",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(11);
//						index=11;
//						EventManager.GameStartTrigger();
//					}
//				break;
//			
//			case GuiState.DodgeShoot:
//				if(GUILayout.Button("Challenge1",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(0);
//				 		index=0;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge2",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(1);
//						index=1;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge3",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(2);
//						index=2;
//						EventManager.GameStartTrigger();
//					}
//			    if(GUILayout.Button("Challenge4",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(3);
//						index=3;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge5",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(4);
//						index=4;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge6",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(5);
//						index=5;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge7",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(6);
//						index=6;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge8",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(7);
//						index=7;
//						EventManager.GameStartTrigger();
//					}
//			  	if(GUILayout.Button("Challenge9",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//				
//						_gameManager.LoadChallenge(8);
//						index=8;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge10",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(9);
//						index=9;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge11",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(10);
//						index=10;
//						EventManager.GameStartTrigger();
//					}
//				if(GUILayout.Button("Challenge12",GUILayout.Width(sizeX),GUILayout.Height(sizeY1)))
//					{ 
//						_gameManager.LoadChallenge(11);
//						index=11;
//						EventManager.GameStartTrigger();
//					}
//				break;
//			
//		}	
//	}
//	
	public void SetStateUI(GuiState guiState)
	{
		
		currentUIState = guiState;
		this.enabled = true;
		
	}
	
	private void GameStart()
	{
		Bgtexture.gameObject.SetActive(false);
		EventManager.Pause += Pause;
		EventManager.Resume += Resume;
		SetStateUI(GuiState.PlayScreen);
		if(_gameManager.GetGameMode() != GameManager.GameMode.SkillShots)
		_gameManager.LoadGamePrefab();
		_gameManager.startGame=true;
		_inputManager.camera.enabled=true;
		GameObject refOfBall=_inputManager.GetRefOfBall();
		Camera camera =_inputManager.GetCameraref();
		_ballHandler.GenerateBall(camera,refOfBall);
		
		
		StatsData statsdata = _gameManager.GetStatsData();
			switch(_gameManager.GetGameMode())
			{
				case GameManager.GameMode.Training:
				{
					statsdata._trainingData._totalTimeplayed++;
					 _gameManager.UpdateIntDataInDevice("_trainingData._totalTimeplayed",statsdata._trainingData._totalTimeplayed,false);
					_instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"WELCOME KEEP PLAYING HERE \n AS LONG AS YOU WANT",true);
					break;
				}
				
				case GameManager.GameMode.ClockShowDown:
				{
					
					statsdata._clockShowDownData._totalNoofTimesPlayed++;
					_gameManager.UpdateIntDataInDevice("_clockShowDownData._totalNoofTimesPlayed",statsdata._clockShowDownData._totalNoofTimesPlayed,false);
					_instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"READY FORSHOWDOWN? \n KEEP AN EYE ON THE CLOCK",true);
					break;
				}
				
				case GameManager.GameMode.SkillShots:
				{
					break;
					//_instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"MISS ONES, AND ITS ALLOVER",true);
				}
				
				case GameManager.GameMode.SuddenDeath:
				{
//					print (statsdata._suddenDeathData._totalNoofTimesPlayed);
					statsdata._suddenDeathData._totalNoofTimesPlayed++;
					_gameManager.UpdateIntDataInDevice("_suddenDeathData._totalNoofTimesPlayed",statsdata._suddenDeathData._totalNoofTimesPlayed,false);
					_instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"TRY TO HIT THE COLOURED BARS",true);
					break;
				}
			}
		
		
	}
	
	private void ReStart()
	{
		
		SetStateUI(GuiState.PlayScreen);
		_gameManager.LoadGamePrefab();
		_ballHandler.setPosition();
		if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
		{
			_gameManager.practiceDistance = 0;
		}
		if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
		{
			_gameManager.LoadChallenge(_gameManager.GetIndex());
			PoleMovementRotation polemovement = _gameManager.GetChallenge().GetComponent<PoleMovementRotation>();
			if(polemovement!=null)
			{
				polemovement.Enable();
			}
		}
		GameObject refOfBall=_inputManager.GetRefOfBall();
		Camera camera =_inputManager.GetCameraref();
		_ballHandler.GenerateBall(camera,refOfBall);
		pole.transform.eulerAngles= new Vector3 (270,0,0);
		pole.transform.position = new Vector3(-5.08428f,-4.71f,115.0f);
		
		
		
			StatsData statsdata = _gameManager.GetStatsData();
			switch(_gameManager.GetGameMode())
			{
				case GameManager.GameMode.Training:
				{
					statsdata._trainingData._totalTimeplayed++;
					 _gameManager.UpdateIntDataInDevice("_trainingData._totalTimeplayed",statsdata._trainingData._totalTimeplayed,false);
					break;
				}
				
				case GameManager.GameMode.ClockShowDown:
				{
					statsdata._clockShowDownData._totalNoofTimesPlayed++;
					_gameManager.UpdateIntDataInDevice("_clockShowDownData._totalNoofTimesPlayed",statsdata._clockShowDownData._totalNoofTimesPlayed,false);
					break;
				}
				
				case GameManager.GameMode.SkillShots:
				{
					break;
				}
				
				case GameManager.GameMode.SuddenDeath:
				{
					statsdata._suddenDeathData._totalNoofTimesPlayed++;
					_gameManager.UpdateIntDataInDevice("_suddenDeathData._totalNoofTimesPlayed",statsdata._suddenDeathData._totalNoofTimesPlayed,false);
					break;
				}
			}
		
	}
	
	private void GameOver()
	{
		SetStateUI(GuiState.GameOverScreen);
	}
	
	private void Pause()
	{
		SetStateUI(GuiState.PauseScreen);
		
	}
	
	private void Home()
	{
		SetStateUI(GuiState.HomeScreen);
		 _ballStand.transform.parent=null;
	     _inputManager.GetCameraref().transform.parent=null; 
		pole.transform.eulerAngles= new Vector3 (270,0,0);
		pole.transform.position = new Vector3(-5.08428f,-4.71f,115.0f);
		Bgtexture.gameObject.SetActive(true);
	}
	
	private void Resume()
	{
		SetStateUI(GuiState.PlayScreen);
		
	}
	private void Practice()
	{
		SetStateUI(GuiState.PracticeScreen);
		_gameManager.SetGameMode(GameManager.GameMode.Training);
		
	}
	
	private void TimeAttack()
	{
		SetStateUI(GuiState.TimeAttackScreen);
		//_gameManager.LoadGamePrefab();
		
	}
	
	private void SkillShot()
	{
		SetStateUI(GuiState.SkillShotScreen);
		_gameManager.SetGameMode(GameManager.GameMode.SkillShots);
		_gameManager.LoadGamePrefab();
	}
	
	private void PassItOn()
	{
		SetStateUI(GuiState.PassItOnScreen);
		//_gameManager.LoadGamePrefab();
		_gameManager.SetGameMode(GameManager.GameMode.PassitOn);
	}
	private void HitTheBar()
	{
		SetStateUI(GuiState.HitTheBar);
		_gameManager.SetCategory(GameManager .Category.HitTheBar);
		_category = SkillShots.Category.HitTheBar;
	}
	
	private void ThroughRings()
	{
		SetStateUI(GuiState.ThroughRings);
		_gameManager.SetCategory(GameManager .Category.ThroughRings);
		_category =	SkillShots.Category.ThroughRings;
	}
	
	private void NoswingStorm()
	{
		SetStateUI(GuiState.NoswingStorm);
		_gameManager.SetCategory(GameManager .Category.NoSwingStorm);
		_category =	SkillShots.Category.NoSwingStorm;
	}
	
	private void DodgeShoot()
	{
		SetStateUI(GuiState.DodgeShoot);
		_gameManager.SetCategory(GameManager .Category.DodgeShoot);
		_category =	SkillShots.Category.DodgeShoot;
	}
	
	public static void SetValues()
	{
		
	}
	
	public static void DisplayValues()
	{
		
	}
	
	IEnumerator Delay(float time)
	{
		yield return new WaitForSeconds(time);
		GameObject refOfBall=_inputManager.GetRefOfBall();
		Camera camera =_inputManager.GetCameraref();
		_ballHandler.GenerateBall(camera,refOfBall);
	}
}






//using UnityEngine;
//using System.Collections;
//
//public class GuiManager : MonoBehaviour 
//{
//
//	// Use this for initialization
//	private GameManager _gameManager;
//	public InputManager _inputManager;
//	void Start () 
//	{
//		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
//	}
//	private bool _bisMode=true;
//	// Update is called once per frame
//	void Update () 
//	{
//	
//	}
//	void OnGUI()
//		
//	{
//		if(_gameManager.GetGameMode()!=GameManager.GameMode.SkillShots)
//		{
//			if(!_gameManager.startGame)
//			{
//				if(!_bisMode)
//				{
//					if(GUI.Button(new Rect(Screen.width/2-100,Screen.height/2-37,200,75),"PLAY"))
//					{
//						
//							_gameManager.startGame=true;	
//		//					GameObject.Find("GuiCamera").camera.enabled=false;
//							_inputManager.camera.enabled=true;
//							_gameManager.LoadGamePrefab();
//						
//					}
//				}
//			}
//			if(_bisMode)
//			{
//				if(GUI.Button(new Rect(0,0,200,75),"Training"))
//				{
//					_gameManager.SetGameMode(GameManager.GameMode.Training);
//					_bisMode=false;
//				}
//				if(GUI.Button(new Rect(0,75,200,75),"ClockShowDown"))
//				{
//					_gameManager.SetGameMode(GameManager.GameMode.ClockShowDown);
//					_bisMode=false;
//				}
//				if(GUI.Button(new Rect(0,150,200,75),"SkillShots"))
//				{
//					_gameManager.SetGameMode(GameManager.GameMode.SkillShots);
//					_bisMode=false;
//					_gameManager.LoadGamePrefab();
//				}
//				if(GUI.Button(new Rect(0,225,200,75),"PassItOn"))
//				{
//					_gameManager.SetGameMode(GameManager.GameMode.PassitOn);
//					_bisMode=false;
//				}
//			}
//		}
//		
//	}
//}
