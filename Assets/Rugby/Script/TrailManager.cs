using UnityEngine;
using System.Collections;

public class TrailManager : MonoBehaviour {

	public GameObject swipeTrailPrefab;
	Transform swipeObjInScene;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0))
		{
			Instantiate(swipeTrailPrefab,Camera.main.ScreenPointToRay(Input.mousePosition).GetPoint(10),Quaternion.identity);
		}
	}
}
