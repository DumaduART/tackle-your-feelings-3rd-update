using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	// Use this for initialization
	public enum GoalType
	{
		normalGoal,HittingZone
	};
	
	public static ScoreManager instance; 
	
	public GameManager _gameManager;
	
	public  int miScoreOfOneShot;
	public  int miCurveangelBonus = 0;
	public  int miDistanceBonus = 0;
	public  int miWindSpeedBonus = 0;
	public  int noOfgoal;
	public  int noOfTokens;
	
	public int XpScore;
	public int levelUpXp;
	public  int modeScore;
	public int currentXp;
	public int currentScore;
	public int currentTokens;
	public int ScoreOfCurrentShot;
	void Start () {
		instance = this;
	   EventManager.ReStart += Retry;
	   EventManager.Home += Home;
	   XpScore = _gameManager.UpdateIntDataInDevice ("Xpscore",XpScore ,true);
	   levelUpXp = _gameManager.UpdateIntDataInDevice("levelUpXp",levelUpXp,true);
	}


	void OnDestroy()
	{
		EventManager.ReStart -= Retry;
		EventManager.Home -= Home;
	}
	// Update is called once per frame
	void Update () {
	
	}
	// ................... Score calculation of Time Attack...........................
	
	public void ScoreOfTimeAttackShot(GoalType goalType)
	{
		
		switch(goalType)
		{
			case GoalType.normalGoal:
			{
				ScoreOfCurrentShot = 1*(TimeAttack.instance.GetMultiplier());
				miScoreOfOneShot += 1*(TimeAttack.instance.GetMultiplier());
				break;
			}
			case GoalType.HittingZone:
			{
				ScoreOfCurrentShot = 2*(TimeAttack.instance.GetMultiplier());
				miScoreOfOneShot += 2*(TimeAttack.instance.GetMultiplier());
				break;
			}
		}
		modeScore = miScoreOfOneShot;
		currentScore = miScoreOfOneShot;
		noOfgoal += 1;
		if(ScoreOfCurrentShot>=TimeAttack.instance.goalStreak)
		{
			TimeAttack.instance.goalStreak = ScoreOfCurrentShot;
//			print ("goalStreak"+TimeAttack.instance.goalStreak);
		}
		//ScoreOfCurrentShot = 0;
	}
	
	//--------------------- Score Calculation of SuddenDaeth-----------------------
	
	// ................................Tokens calculation...............................
	public void TokensCalculation()
	{
		if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
			noOfTokens += (modeScore * 100 / 1000);
			if (_gameManager._coinUpgrade.currentUpgrade > 1)
				currentTokens = (modeScore * 100 / 1000) + (modeScore * 100 / 1000) * _gameManager._coinUpgrade.totalUpgrade [_gameManager._coinUpgrade.currentUpgrade - 1].UpgradePercentage / 100;
			else
				currentTokens = (modeScore * 100 / 1000);
			_gameManager.AddCoin (currentTokens);
		}
	}
	//.............................. Getting Score of a particular Shot..................... 
	public int GetScorePerShot()
	{
		return(miScoreOfOneShot);
	}
	
	public  void SetModeScore( int x ,int y)
	{
		modeScore = x;
		currentScore = x;
		ScoreOfCurrentShot = y;
	}
	
	public void XpCalculation()
	{
		
			if (_gameManager._xpUpgrade.currentUpgrade > 1) {
				currentXp = (int)(modeScore / 10 + (modeScore / 10) * _gameManager._xpUpgrade.totalUpgrade [_gameManager._xpUpgrade.currentUpgrade - 1].UpgradePercentage / 100);
			
			} else {
				currentXp = (int)modeScore / 10;
			}
			XpScore += currentXp;
			levelUpXp += (int)modeScore / 10;
			_gameManager.UpdateIntDataInDevice ("Xpscore", XpScore, false);
			_gameManager.UpdateIntDataInDevice ("levelUpXp", levelUpXp, false);

		
	}
	
	public void SetXp(int x)
	{
		
			XpScore += x;
			levelUpXp += x;
			_gameManager.UpdateIntDataInDevice ("Xpscore", XpScore, false);

		
	}
	
	public void SetLevelUpXp(int x)
	{
		
			levelUpXp = x;
			_gameManager.UpdateIntDataInDevice ("levelUpXp", levelUpXp, false);

	}
	
	public int GetLevelUpXp()
	{
		return(levelUpXp);
	}
	
	public int GetXpScore()
	{
		return(XpScore);
	}
	
	public int GetCurrentXp()
	{
		return(currentXp);
	}
	public int CurrentScore()
	{
		return(currentScore);
	}
	
	public int CurrentTokens()
	{
		return(currentTokens);
	}
	
	public int GetCurrentShotScore()
	{
		return (ScoreOfCurrentShot);
	}
	
	private void  Retry()
	{
		miScoreOfOneShot = 0;
		miCurveangelBonus = 0;
		miDistanceBonus = 0 ;
		miWindSpeedBonus = 0;
		noOfgoal = 0;
		modeScore = 0; 
	}
	
	private void Home()
	{
		miScoreOfOneShot = 0;
		miCurveangelBonus = 0;
		miDistanceBonus = 0 ;
		miWindSpeedBonus = 0;
		noOfgoal = 0;
		modeScore = 0; 
	}
	
		
}
