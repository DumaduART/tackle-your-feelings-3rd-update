using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MonetizationHelper;

public class GameManager : MonoBehaviour {

	// Use this for initialization
	public bool isInappNotReq;
	public bool isFacebookReq;
	public bool isNativeCallReq;
	public bool isRemoveAddReq;
	public ScreenManager mainScreenManager;
	public float practiceDistance;
	public InputManager _inputManager;
	public ScoreManager _scoreManager;
	public BallHandler _ballHandler;
	public BallSelectionManager _ballSelection;
	public ScreenManager FaceBookSubScrren;
	private GameObject expPrefabclone;
	public string LocalLeatherBord;
	public enum GameMode
	{
		None=0,
		Training,
		ClockShowDown,
		SkillShots,
		SuddenDeath,
		PassitOn
	}
	public enum Category
	{
		none=0,
		HitTheBar,
		ThroughRings,
		NoSwingStorm,
		DodgeShoot
	};
	public Category _cateGory;
    public  GameMode _gameMode;
	public int  index =0;
	//public static GameManager instance;
	public bool startGame;
	public bool _callLogin;
	public GameObject _gPracticePrefab;
	public GameObject _gTimeAttackPrefab;
	public GameObject _gSkillShotPrefab;
	public GameObject _gPassItOnPrefab;
	public GameObject _gSuddenDeath;
	public GameObject[] _HittheBar;
	public GameObject[] _PassThroughRings;
	public GameObject[] _NoswingStorm;
	public GameObject[] _DodgeShoot;
	public int _totalcoin;
	private string string1;
	private string string2;
	private string key;
	public GameObject challenge;
	public int levelNo ;
	public int levelUpCuttoff;
	private int extraLevelUpPoint;
	
	public int noOfSuddenDeathBall;
	public int x2UpgradePercent;
	public int x3UpgradePercent;
	public int x4UpgradePercent;
	public GameObject _XpCamera;
	private GameObject xpCameraClone;
	
	public List<GameObject> IncomingPopups = new List<GameObject>();
	
	public ItemUpgrade _xpUpgrade;
	public ItemUpgrade _coinUpgrade;
	public ItemUpgrade _lifeUpgrade;
	public ItemUpgrade _timeUpgrade;
	
	public int[] _isthroughrings;
	public int[] _isHitTheBar;
	public int[] _isNoSwing;
	public int[] _throughringsUnlockingCost;
	public int[] _hitthebarUnlockingCost;
	public int[] _noswingUnlockingCost;
	
	public GetMoreTime[] _moreTime;
	public GetMoreLife[] _moreLife;
	
	public static GameManager instance;
	
	public StatsData _statsData;
	
	float startTime;
	
	bool isLevelUp = false;
	
	public int noOfInvite;
	
	public int  noOfThroughRingsGold;
	
	public int  noOfHitTheBarGold;
	
	public int noOfNoswingGold;
	
	int _gameTime;
	int _chkTime = 1;
	
	public bool checkingAdd = true;
	
	public int _gameOverCounter;
//	private GameObject mprefabInstance;
	void Awake()
	{
		//PlayerPrefs.DeleteAll();
		instance = this;
		Input.multiTouchEnabled = false;

		
		
//		List<string> name = new List<string>();
//		string t = "Brajesh kumar dinkar" + " ";
//		char[] tempArray = t.ToCharArray();
//		int checkpoint = 0;
//		int j = 0;
//		for(j = 0;j<tempArray.Length;j++)
//		{
//			if(tempArray[j] == ' ')
//			{
//				string n = t.Substring(checkpoint,(j-checkpoint));
//				name.Add(n);
//				checkpoint = j;
//			}
//		}
//		t = "";
//		for(j = 0 ;j<name.Count ; j++)
//		{
//			if(t.Length + name[j].Length > 15)
//				break;
//			t = t + name[j] + " ";
//		}
//		Debug.Log(t);
	}
	void Start () 
	{
//		instance=this;
		GUIManager.instance.SetStateUI(GUIManager.GuiState.HomeScreen);
		startGame=false;
		//Application.targetFrameRate=100;
		EventManager.GameOver += GameOver ;
		string2 = "0" ;
		_ballSelection = BallSelectionManager.GetInstance();
		if(PlayerPrefs.GetString("firstTime")=="")
		{
			UpdateFloatDataInDevice("sfxvalue",0.9f,false);
			UpdateFloatDataInDevice("musicvalue",0.9f,false);
			_statsData.UpdateDataInDevice();
			UpdateIntDataInDevice("_totalcoin",_totalcoin,false);
			UpdateIntDataInDevice("coinCurrentUpdate" ,_coinUpgrade.currentUpgrade,false);
			UpdateIntDataInDevice("xpCurrentUpdate" ,_xpUpgrade.currentUpgrade,false);
			UpdateIntDataInDevice("timeCurrentUpdate" ,_timeUpgrade.currentUpgrade,false);
			UpdateIntDataInDevice("lifeCurrentUpdate" ,_lifeUpgrade.currentUpgrade,false);
			UpdateIntDataInDevice("noOfInvite", noOfInvite,false);
			PlayerPrefs.SetString("sound","on") ;
			PlayerPrefs.SetString("instructor","instructoron");
			PlayerPrefs.SetString("firstTime","Done");
			UpdateLifeCostinDevice();
			UpdateTimeCostInDevice();
			_ballSelection.UpdateInDevice();
			RetriveThroughRingsCondition(true);
			RetriveHittheBarCondition(true);
			RetriveNoswingCondition(true);
		}
		
		else
		{
			RetriveLifeCost();
			RetriveTimeCost();
		}
		
		levelNo = UpdateIntDataInDevice("levelNo",levelNo,true);
		SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gui);
		_totalcoin = UpdateIntDataInDevice("_totalcoin",_totalcoin,true);
		_coinUpgrade.currentUpgrade = UpdateIntDataInDevice("coinCurrentUpdate" ,_coinUpgrade.currentUpgrade,true);
		_xpUpgrade.currentUpgrade = UpdateIntDataInDevice("xpCurrentUpdate" ,_xpUpgrade.currentUpgrade,true);
		_timeUpgrade.currentUpgrade = UpdateIntDataInDevice("timeCurrentUpdate" ,_timeUpgrade.currentUpgrade,true);
		_lifeUpgrade.currentUpgrade = UpdateIntDataInDevice("lifeCurrentUpdate" ,_lifeUpgrade.currentUpgrade,true);
		noOfInvite = UpdateIntDataInDevice("noOfInvite", noOfInvite,true);
		levelNo = UpdateIntDataInDevice("levelNo",levelNo,true);
		_statsData.RetriveDataFromDevice();
		RetriveThroughRingsCondition(false);
		RetriveHittheBarCondition(false);
		RetriveNoswingCondition(false);
		
		if(PlayerPrefs.GetInt("ISADSRemoved") == 0)
		{
			checkingAdd = true;
		}
		else
		{
			checkingAdd = false;
		}
		
	}

	void OnDestroy()
	{
		EventManager.GameOver -= GameOver ;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//-----------Selection Of GameMode----------
		_gameTime = (int)Time.time;
		if(_gameTime >= _chkTime)
		{
			_chkTime = _gameTime+1;
			_statsData._overAllData._durationOfTimePlayed += 1;
			UpdateIntDataInDevice("_overAllData._durationOfTimePlayed",_statsData._overAllData._durationOfTimePlayed,false);
		}
	}
	public void SetGameMode(GameMode  gameMode)
	{
		switch(gameMode)
		{
			case GameMode.Training:
				{
					_gameMode=GameMode.Training;
					break;
				}
			case GameMode.ClockShowDown:
				{
					_gameMode=GameMode.ClockShowDown;
					break;
				}
			case GameMode.PassitOn:
				{
					_gameMode=GameMode.PassitOn;
					break;
				}
			case GameMode.SkillShots:
				{
					_gameMode=GameMode.SkillShots;
					break;
				}
			case GameMode.SuddenDeath:
			{
				_gameMode=GameMode.SuddenDeath;
				break;
			}
			
			case GameMode.None:
			{
				_gameMode=GameMode.None;
				break;
			}
		}
		
	}
	public void LoadGamePrefab()
	{
		_inputManager.SetSwingCondition(false);
		switch(_gameMode)
		{
			case GameMode.Training:
				{
					GameObject training = Instantiate(_gPracticePrefab,_gPracticePrefab.transform.position,Quaternion.identity) 
				    as GameObject;
					EventManager.ActiveObjects.Add(training);
					break;
				}
			case GameMode.SkillShots:
				{
					GameObject skillShot = Instantiate(_gSkillShotPrefab,_gSkillShotPrefab.transform.position,Quaternion.identity)
				     as GameObject;
					EventManager.ActiveObjects.Add(skillShot);
					break;
				}
			case GameMode.ClockShowDown:
				{
					GameObject clockshowdown = Instantiate(_gTimeAttackPrefab,_gTimeAttackPrefab.transform.position,Quaternion.identity)
					as GameObject;
					EventManager.ActiveObjects.Add(clockshowdown);
					break;
				}
			case GameMode.PassitOn:
				{
					GameObject passiton = Instantiate(_gPassItOnPrefab,_gPassItOnPrefab.transform.position,Quaternion.identity) 
					as GameObject;
					EventManager.ActiveObjects.Add(passiton);
					break; 
				}
			case GameMode.SuddenDeath:
			{
				GameObject suddendeath = Instantiate(_gSuddenDeath,_gSuddenDeath.transform.position,Quaternion.identity) 
				    as GameObject;
					EventManager.ActiveObjects.Add(suddendeath);
					break;
			}
		 }	
	}
	public  int GetWind()
	{
		int wind = 0;
		switch(_gameMode)
		{
			case GameMode.Training:
				{
//					print (Practice.instance.GetPracticeWind());
					//return(Practice.instance.GetPracticeWind());
					wind = Practice.instance.GetPracticeWind();
					break;
				}
			case GameMode.SkillShots:
				{
					LevelHandler levelHandler = LevelHandler.instance;
					//return(levelHandler.GetWind());
					wind = levelHandler.GetWind();
					break;
				}
			case GameMode.ClockShowDown:
				{
					//return(TimeAttack.instance.GetWind());
					wind = TimeAttack.instance.GetWind();
					break;
				}
			case GameMode.PassitOn:
				{
					//return(0);
					break;
				}
			case GameMode.SuddenDeath:
				{
					//return(SuddenDeath.instance.GetWind());
					wind = SuddenDeath.instance.GetWind();
					break;
				}
			
		 }	
		
		switch(_inputManager._windRsistance)
		{
			case 1:
			{
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) 
					wind =((wind*100)/100);
				else
					wind =((wind*100)/100);
				break;
			}
			
			case 2:
			{
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) 
					wind = ((wind*80)/100);
				else
					wind =((wind*100)/100);
				break;
			}
			
			case 3:
			{
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) 
					wind = ((wind*60)/100);
				else
					wind =((wind*100)/100);
				break;
			}
			
			case 4:
			{
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) 
					wind = ((wind*50)/100);
				else
					wind =((wind*100)/100);
				break;
			}
			
			case 5:
			{
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) 
					wind = ((wind*30)/100);
				else
					wind =((wind*100)/100);
				break;
			}
			
		}
		return(wind);
	}
	
	public void SetAirDrag()
	{
		switch(_inputManager._ballSpeed)
		{
			case 1:
			{
				_inputManager.GetRefOfBall().GetComponent<Rigidbody>().drag = 0.5f;
				break;
			}
			
			case 2:
			{
				_inputManager.GetRefOfBall().GetComponent<Rigidbody>().drag = 0.3f;
				break;
			}
			
			case 3:
			{
				_inputManager.GetRefOfBall().GetComponent<Rigidbody>().drag = 0.2f;
				break;
			}
			
			case 4:
			{	
				_inputManager.GetRefOfBall().GetComponent<Rigidbody>().drag = 0.1f;
				break;
			}
			
			case 5:
			{
				_inputManager.GetRefOfBall().GetComponent<Rigidbody>().drag = 0.0f;
				break;
			}
		}
	}
	
	public float GetDistanceValue()
	{
		switch(_gameMode)
		{
			case GameMode.Training:
				{
							practiceDistance = Practice.instance.GetDistanceToincrease();
					return(practiceDistance);
					//break;
				}
			case GameMode.SkillShots:
				{
					return(115-LevelHandler.instance._ballPos.z);
					//break;
				}
			case GameMode.ClockShowDown:
				{
					return(_ballHandler.GetDistOfBall());
					//break;
				}
			case GameMode.PassitOn:
				{
					return(0);
					//break;
				}
			case GameMode.SuddenDeath:
				{
					return(SuddenDeath.instance.GetDist());
					//break;
				}
		    default:
				return(0);	
		 }	
	}
	public void SetCategory(Category category)
	{
		switch(category)
		{
			case Category.HitTheBar :
			{
				_cateGory=Category.HitTheBar;
				string1 = "HitTheBar";
				_inputManager.camera.enabled=true;
		  		_inputManager.SetSwingCondition(false);
		        startGame=true;
				break;
			}
			case Category.ThroughRings :
			{
				_cateGory=Category.ThroughRings;
				_inputManager.camera.enabled=true;
				string1 = "ThroughRings";
				_inputManager.SetSwingCondition(false);
				startGame=true;
				break;
			}
			case Category.NoSwingStorm :
			{
				_cateGory=Category.NoSwingStorm;
				_inputManager.camera.enabled=true;
				_inputManager.SetSwingCondition(true);
				string1 = "NoSwingStorm";
				//  LoadChallenge(index);
				 startGame=true;
				break;
			}
			case Category.DodgeShoot :
			{
				_cateGory=Category.DodgeShoot;
				_inputManager.camera.enabled=true;
				//  LoadChallenge(index);
				 startGame=true;
				break;
			}
				
		}
		
		key = string1 + string2 ;
	}
	public void LoadChallenge(int index)
	{
		InstructionManager instructionManager = InstructionManager.GetInstance();
		_statsData._killShotData._totalAttempts++;
		_statsData.UpdateDataInDevice();
		if(challenge!=null)
			Destroy(challenge);
		switch(_cateGory)
		{
			case Category.HitTheBar:
			{
				challenge = Instantiate(_HittheBar[index]) as GameObject;
				EventManager.ActiveObjects.Add(challenge);
				if(index<4)
				{
					if(index ==0)
					{
						instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"TRY TO HIT THE BAR,LETS SEE HOW GOOD \n YOU ARE",true);
					}
				
					else if(index == 1)
					{
						instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"YOU DID THAT? LETS SEE \n IF YOU CAN DO IT TWICE THIS TIME",true);
					}
					
					else if(index == 2)
					{
						instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"YOU ARE TOUGH EH!! \n LETS SEE WHAT YOU CAN DO THIS TIME",true);
					}
				
					else if(index == 3)
					{
						instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"RAM I ANNOYING YOU? \n YOU MAY TURN ME OFF FROM SETTINGS",true);
					}
				}
				break;
			}
			case Category.ThroughRings:
			{
				
				challenge = Instantiate(_PassThroughRings[index])as GameObject;
				EventManager.ActiveObjects.Add(challenge);
				if(index ==0)
				{
					instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"OKAY, FLICK THE BALL THROUGH \n THE RINGS INTO THE BARS, DARE ?",true);
				}
			
				else if(index == 1)
				{
					//instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"THAT’S JUST A FLUKE, OD IT TWICE \n AND PROVE ME",true);
				}
			
				else if(index == 2)
				{
					instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"I WANNA GET OUTTA HERE, \n CAN YOU PLEASE TURN ME OFF FROM SETTINGS ?",true);
				}
				break;
			}
			case Category.NoSwingStorm:
			{
				_inputManager.SetSwingCondition(true);
				challenge = Instantiate(_NoswingStorm[index]) as GameObject;
				EventManager.ActiveObjects.Add(challenge);
				if(index ==0)
				{
					instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"YOU CANT SWING, BUT YOU GOTTA WIN, \n IS THAT  DARING ?? HA HA HA",true);
				}
			
				else if(index == 1)
				{
					instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"SHUFFLE AND DISPLAY FROM \n OTHER CHALLENGE MODES",true);
				}
				break;
			}
			case Category.DodgeShoot:
			{
				challenge = Instantiate(_DodgeShoot[index])as GameObject;
				EventManager.ActiveObjects.Add(challenge);
				break;
			}
		}
	}
	
	public void BeforeGameOverCondition()
	{
		ScreenManager screenManager = GameObject.Find("ScreenManager").GetComponent<ScreenManager>();
		switch(_gameMode)
		{
			case GameMode.ClockShowDown:
			{
				if (TimeAttack._ReviveForTimeMode == false) {
                        
					//if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
					//	BallSelectionSubScreenManager.instance.setBall1Status (BALLSTATE.BALLACTIVATED);
					//	TimeAttackScreenGUIItemsManager.GetBallSelectionClone ().GetComponent<ScreenManager> ().closeScreenManager ();
					//	screenManager.LoadScreen ("GetMoreTimesScreen");
					//} else {

						GetMoreTimeGUIItemsManager.mbGameOverScreen = false;
						EventManager.GameOverTrigger();

					//}
						
					_inputManager.enabled = false;
				}
				else {
					GetMoreTimeGUIItemsManager.mbGameOverScreen = false;
					EventManager.GameOverTrigger();
				}
				break;
			}
			
			case GameMode.SuddenDeath:
			{
				if (SuddenDeathGUIItemsManager._ReviveSuddenDeath == false) {
					//if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode)
					//   screenManager.LoadScreen ("GetMoreLifeScreen");
					//if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode) {
						GetMoreLifeGUIItemsManager.mbGameOverScreen = false;
						EventManager.GameOverTrigger ();
					//}
					_inputManager.enabled = false;
				}
				else if(SuddenDeathGUIItemsManager._ReviveSuddenDeath == true)
				{
					GetMoreLifeGUIItemsManager.mbGameOverScreen = false;
					EventManager.GameOverTrigger();
				}
				break;
			}
		}
	}
	int callCount;
	public bool ChkGameOverCondition()
	{
		callCount++;
		
		bool isGameOver = false;
		switch(_gameMode)
		{
			case GameMode.Training:
			{
				break;
			}
			case GameMode.SkillShots:
			{
				LevelHandler levelHandler = LevelHandler.instance;
				if(levelHandler.GetGameOverParameter()==LevelHandler.GameoverParameter.ball)
				{
				  
				
				    if(levelHandler._isConsecutiveBased)
				    {
					 
					
				    }
				
//						Debug.Log("SuccessfullShots "+levelHandler.GetSuccessFulShot()+" Objective "+levelHandler.GetObjective());
				
					if(levelHandler.RemainingBall()<=0 || levelHandler.GetSuccessFulShot()>= levelHandler.GetObjective() && !levelHandler._isConsecutiveBased)
					{
						levelHandler.ChkforTrophy();
						isGameOver = true;
						EventManager.GameOverTrigger();
						Destroy(_inputManager.GetRefOfBall());
						_inputManager.enabled=false;
					}
				//Edited By Ankit
					else if(levelHandler.GetConsecutiveBased())// && callCount != 1)
					{
						if(levelHandler.RemainingBall()>=levelHandler.GetRequiredConsecutiveShots()-levelHandler.GetFinishedConsecutive())
					     {
							//print ("Due To ball");
							int clearCount = 0;
						    for(int i=0;i<_ballHandler.ring.Count;i++)
						     {
								  if(_ballHandler.ring[i]!=null)
							      if(_ballHandler.ring[i].GetComponent<MeshRenderer>().enabled == false)
									{
										clearCount++;
									}
						     }
							
								if(levelHandler.GetFinishedConsecutive() == levelHandler.GetRequiredConsecutiveShots())
								{
									/*Debug.Log("Level Cleared successful from consecutive" + clearCount + " ring count " +
									_ballHandler.ring.Count + " finish cons " + levelHandler.GetFinishedConsecutive() +
									" req " + levelHandler.GetRequiredConsecutiveShots());*/
									Debug.Log("finish cons " + levelHandler.GetFinishedConsecutive() +
									" req " + levelHandler.GetRequiredConsecutiveShots());
									levelHandler.ChkforTrophy();
									isGameOver = true;
									EventManager.GameOverTrigger();
									Destroy(_inputManager.GetRefOfBall());
									_inputManager.enabled=false;
								}
						    
					     }
					
					    else
						{  //if Remaaining Balls are less than Required Consecutive Shots
						       
							//Debug.Log("Level Failed" + levelHandler.RemainingBall() + " req ->" + levelHandler.GetRequiredConsecutiveShots());
							levelHandler.OnConsecutiveFailed();
							isGameOver = true;
							EventManager.GameOverTrigger();
							Destroy(_inputManager.GetRefOfBall());
							_inputManager.enabled=false;
					    }
					}//<-
				
				
				}
			
				if(levelHandler.GetGameOverParameter()==LevelHandler.GameoverParameter.time)
				{
					if(levelHandler.GetSuccessFulShot()>= levelHandler.GetObjective() && !levelHandler.GetConsecutiveBased())
					{
						levelHandler.ChkforTrophy();
						isGameOver = true;
						EventManager.GameOverTrigger();
						Destroy(_inputManager.GetRefOfBall());
						_inputManager.enabled=false;
					}
				
					//Edited By Ankit
					else if(levelHandler.GetConsecutiveBased())// && callCount != 1)
					{
//							print ("Due To Time");
							int clearCount = 0;
//						    for(int i=0;i<_ballHandler.ring.Count;i++)
//						     {
//							      if(_ballHandler.ring[i].GetComponent<MeshRenderer>().enabled == false)
//									{
//										clearCount++;
//									}
//						     }
							
								if(levelHandler.GetFinishedConsecutive() == levelHandler.GetRequiredConsecutiveShots())
								{
									/*Debug.Log("Level Cleared successful from consecutive" + clearCount + " ring count " +
									_ballHandler.ring.Count + " finish cons " + levelHandler.GetFinishedConsecutive() +
									" req " + levelHandler.GetRequiredConsecutiveShots());*/
									Debug.Log("finish cons " + levelHandler.GetFinishedConsecutive() +
									" req " + levelHandler.GetRequiredConsecutiveShots());
									levelHandler.ChkforTrophy();
									isGameOver = true;
									EventManager.GameOverTrigger();
									Destroy(_inputManager.GetRefOfBall());
									_inputManager.enabled=false;
								}
						
//								else
//								{  //if Remaaining Balls are less than Required Consecutive Shots
//								       
//									Debug.Log("Level Failed" + levelHandler.RemainingBall() + " req ->" + levelHandler.GetRequiredConsecutiveShots());
//									levelHandler.OnConsecutiveFailed();
//									isGameOver = true;
//									EventManager.GameOverTrigger();
//									Destroy(_inputManager.GetRefOfBall());
//									_inputManager.enabled=false;
//							    }
						
					   
					}//<-
				
				
				}
				
				break;
			}
			case GameMode.ClockShowDown:
			{
				
				break;
			}
			case GameMode.SuddenDeath:
			{
				SuddenDeath suddenDeath = SuddenDeath.instance;
				if(suddenDeath.GetBall()<=0)
				{	
					if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
						BallSelectionSubScreenManager.instance.setBall1Status (BALLSTATE.BALLACTIVATED);
						if (SuddenDeathGUIItemsManager.GetBallSelectionClone ())
							SuddenDeathGUIItemsManager.GetBallSelectionClone ().GetComponent<ScreenManager> ().closeScreenManager ();
					}
					BeforeGameOverCondition();
					//isGameOver = true;
				}
				break;
			}
		}
//		if(isGameOver)
//		{   
//			
//			EventManager.GameOverTrigger();
//			Destroy(_inputManager.GetRefOfBall());
//			_inputManager.enabled=false;
//		}
		if(callCount == 1)
			return(false);
		else
		 return(isGameOver) ;
	}
	
	public int GetSuddenDeathBall()
	{
		return(noOfSuddenDeathBall);
	}
	
	public int GetPowerUpUpgradePercentage(int i)
	{
		int percentage =0;
		switch(i)
		{
		case 2:
			percentage = x2UpgradePercent;
			break;
		case 3:
			percentage = x3UpgradePercent;
			break;
		case 4:
			percentage = x4UpgradePercent;
			break;	
		}
		return(percentage);
		
	}
	
	public Category GetCategory()
	{
		return(_cateGory);
	}
	
	public GameMode GetGameMode()
	{
		return(_gameMode);
	}
	
	public void SetIndex(int i)
	{
		index = i;
		string2 = index.ToString();
		key = string1 + string2 ;
	}
	
	public int GetIndex()
	{
		return(index);
	}
	
	private void GameOver()
	{
		_gameOverCounter++;
		if(_gameOverCounter == 4)
		{
			_gameOverCounter = 0;
			// Rate The Application
		}
		
		SoundManager.instance.WhistleSound(1);
		ScreenManager screenManager = GameObject.Find("ScreenManager").GetComponent<ScreenManager>();
		
		updateAchievements (17);
		updateAchievements (18);
		updateAchievements (19);
		updateAchievements (20);
		switch(_gameMode)
		{
			
			case GameMode.Training:
			{
				break;
			}
			case GameMode.ClockShowDown:
			{
			
				TimeAttack.instance.ChkForStatsUpdate();
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
					_scoreManager.TokensCalculation ();
					_scoreManager.XpCalculation ();
					LevelUpcondition (_scoreManager.GetLevelUpXp ());
				}
				screenManager.LoadScreen("TimeAttackGameOver");
				if(isNativeCallReq)
				{
				}
			//CROLAND
				else
				{
					ScoreHandler.Instance.PostScore(_scoreManager.CurrentScore(),"ClockShowDownLB");
				}
				updateAchievements (2);
				
				updateAchievements (3);
			
				updateAchievements(4);
				
				updateAchievements(5);
				
			
				break ;
			}
			case GameMode.SkillShots:
			{
				updateAchievements (12);
				updateAchievements (13);
				updateAchievements (14);
			
				LevelHandler levelHandler = LevelHandler.instance;
				if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
				{
					ArcadeGamepalyGUIItemsManager1.GetballSelectionScreen().GetComponent<ScreenManager>().closeScreenManager();
					screenManager.LoadScreen("SkillShotGameOver");
				}
				if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.time)
				{
					SkillShotGUIItemsManager.GetBallSelectionClone().GetComponent<ScreenManager>().closeScreenManager();
					screenManager.LoadScreen("SkillShotGameOver");
				}

				break ;
			}
			case GameMode.SuddenDeath:
			{
				
				SuddenDeath.instance.ChkForStatsUpdate();
				if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
					_scoreManager.TokensCalculation ();
					_scoreManager.XpCalculation ();
					LevelUpcondition (_scoreManager.GetLevelUpXp ());
				}
				screenManager.LoadScreen("SuddendeathOver");
				if(isNativeCallReq)
				{
				}
				
				else
				{
					ScoreHandler.Instance.PostScore(_scoreManager.CurrentScore(),"SuddenDeathLB");
				}
				updateAchievements (21);
				
				if(_scoreManager.CurrentScore() >= 20000)
					updateAchievements (22);
				if(_scoreManager.CurrentScore() >= 50000)
					updateAchievements (23);
				if(_scoreManager.CurrentScore() >= 100000)
					updateAchievements (24);
			
				break;
			}
		}		
	}
	
	public void CallGameOverScreen()
	{
		ScreenManager screenManager = GameObject.Find("ScreenManager").GetComponent<ScreenManager>();
		Destroy(xpCameraClone);
		
		switch(_gameMode)
		{
			
			case GameMode.Training:
			{
				break;
			}
			case GameMode.ClockShowDown:
			{
				_scoreManager.TokensCalculation();
				_scoreManager.XpCalculation();
			    LevelUpcondition(_scoreManager.GetLevelUpXp());
				screenManager.LoadScreen("TimeAttackGameOver");
				break ;
			}
			case GameMode.SkillShots:
			{
				LevelHandler levelHandler = LevelHandler.instance;
				if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
				{
					screenManager.LoadScreen("SkillShotGameOver");
				}
				if(levelHandler._gameOverParameter == LevelHandler.GameoverParameter.time)
				{
					screenManager.LoadScreen("SkillShotGameOver");
				}

				break ;
			}
			case GameMode.SuddenDeath:
			{
				_scoreManager.TokensCalculation();
				_scoreManager.XpCalculation();
				 LevelUpcondition(_scoreManager.GetLevelUpXp());
				screenManager.LoadScreen("SuddendeathOver");
				break;
			}
		}		
	}
	
	public void LevelUpcondition(int levelUpXp)
	{
//		print (levelUpXp);
		int currentLvel = (int)_scoreManager.GetXpScore()/levelUpCuttoff;
		if(currentLvel>levelNo)
		{
			isLevelUp = true;
			//PopUpManager.instance.CreatePopUp(PopUpType.levelup);
		}
		levelNo = currentLvel;
		UpdateIntDataInDevice("levelNo",levelNo,false);
//		if(levelUpXp>levelUpCuttoff)
//		{
//			extraLevelUpPoint = levelUpXp - levelUpCuttoff;
//			levelUpXp = levelUpCuttoff;
//			levelNo += 1;
//			UpdateIntDataInDevice("levelNo",levelNo,false);
//		}
//		
//		else 
//			extraLevelUpPoint = 0;
//		
//		_scoreManager.SetLevelUpXp(levelUpXp);
	}
	
	public bool ChkLevelUpCondition()
	{
		return(isLevelUp);
	}
	
	public void SetLevelUpCondition(bool x)
	{
		isLevelUp = x;
	}
	
	public int ExtraLveluPPoint()
	{
		return(extraLevelUpPoint);
	}
	
	public void SetExtraLveluPPoint(int x)
	{
		extraLevelUpPoint = x;
	}
	
	public int GetLevelUpCutoff()
	{
		return(levelUpCuttoff);
	}
	
	public string GetKey()
	{
		return(key);
	}
	
	public int GetCoins()	
	{
		return(_totalcoin);
	}
	
	public void AddCoin(int x)
	{
		_totalcoin += x;
		UpdateIntDataInDevice("_totalcoin",	_totalcoin ,false);
	}
	
	public int GetLevelNo()
	{
		return(levelNo+1);
	}
	
	public GameObject GetChallenge()
	{
		return(challenge);
	}
	
	public void ChangePoleColor(bool isOriginal)
	{
		GameObject pole = GameObject.Find("Poles");
		if(isOriginal)
		{
			pole.GetComponent<MeshRenderer>().material.color = Color.white;
		}
		else
		{
			switch(_gameMode)
			{
				case GameManager.GameMode.Training:
					{
						pole.GetComponent<MeshRenderer>().material.color = new Color(1,0.5f,0.5f,1);// Color.red;
						break;
						
					}
				case GameManager.GameMode.ClockShowDown:
					{
						pole.GetComponent<MeshRenderer>().material.color = new Color(1,0.5f,0.5f,1);
						break;
						
					}	
				case GameManager.GameMode.SkillShots:
					{
						if(_cateGory == GameManager.Category.HitTheBar)
							pole.GetComponent<MeshRenderer>().material.color = new Color(0.75f,1,0.75f,1);
						else 
							pole.GetComponent<MeshRenderer>().material.color = new Color(1,0.5f,0.5f,1);
						break;
						
					}	
			}
		}	
	}
//	BallSelectionPopupScreen
	public string UpdateStringDataInDevice( string key ,string id ,bool isFromDevice)
	{
		
		if(isFromDevice)
		{
			return(PlayerPrefs.GetString(key));
		}
		
		else
		{
			PlayerPrefs.SetString(key,id);
			return "" ;
		}
	}
	
	public int UpdateIntDataInDevice(string key , int id , bool isFromDevice)
	{
		if(isFromDevice)
		{
			return(PlayerPrefs.GetInt(key));
		}
		
		else
		{
			PlayerPrefs.SetInt(key,id);
			return 0;
		}
	}
	
	public float UpdateFloatDataInDevice(string key , float id , bool isFromDevice)
	{
		if(isFromDevice)
		{
			return(PlayerPrefs.GetFloat(key));
		}
		
		else
		{
			PlayerPrefs.SetFloat(key , id);
			return 0.0f;
		}
	}
	
	public void ChkRemoveAds()
	{
		if(PlayerPrefs.GetInt("ISADSRemoved") == 0)
		{
			
			PlayerPrefs.SetInt("ISADSRemoved",1);
			if(PurchaseScreenGUIItemsManager.instance)
			PurchaseScreenGUIItemsManager.instance._removeAdsText.text = "Checkout for more games from Game++";
			if(CoinPurchaseScreenManager.instance)
				CoinPurchaseScreenManager.instance._removeAdsText.text = "Checkout for more games from Game++";
			checkingAdd = false;
		}
	}
	
	public void Receiver(int _pValue)
	{	
		if(PurchaseScreenGUIItemsManager.instance!=null)
		PurchaseScreenGUIItemsManager.instance.CheckButton(true);		
        switch (_pValue)		
			{
				case 1:
					
					AddCoin(1000);
					GuiTextManager.instance.UpdateText();
					//ChkRemoveAds();
					break;
				case 2:
					AddCoin(4000);
					GuiTextManager.instance.UpdateText();
					//ChkRemoveAds();
					break;
				case 3:
					AddCoin(9000);
					GuiTextManager.instance.UpdateText();
					//ChkRemoveAds();
					break;	
				case 4:
					AddCoin(13000);
					GuiTextManager.instance.UpdateText();
					//ChkRemoveAds();
					break;
				case 5:
					
					AddCoin(24000);
					GuiTextManager.instance.UpdateText();
					//ChkRemoveAds();
					break;
				case 6:
					
					AddCoin(50000);
					GuiTextManager.instance.UpdateText();
					//ChkRemoveAds();
					break;
			case 7:
				PlayerPrefs.SetInt("ISADSRemoved",1);
				if(GameObject.Find("MainMenu(Clone)") != null)
                {
					
                    PlayerPrefs.SetInt("ISADSRemoved",1);
					GameObject.Find("RemoveAdsbtn").SetActive(false);
					GameObject.Find("RemoveAdsText").SetActive(false);
					GameObject.Find("RemoveAdsIcon").SetActive(false);
					AddCoin(200);
					checkingAdd = false;
					GuiTextManager.instance.UpdateText();
                }
				break;		
			}	
		}	
	
	public void InAppCalling( int ID)
	{
		switch(ID)
		{
			case 1 :
			{
                Receiver(1);
				break;
			}
				
			case 2:
			{
                Receiver(2);
				break;
			}
				
			case 3:
			{
                Receiver(3);
				break;
			}
			
			case 4:
			{
                Receiver(4);	
				break;
			}
				
			case 5:
			{
                Receiver(5);
				break;
			}
			case 6:
			{
                Receiver(6);	
				break;
			}
		}
	}
	
	public static GameManager GetInstance()
	{
		return(instance);
	}
	
	public bool Upgrade(int id)
	{
		bool isUpgrade = false;
		
		switch(id)
		{
			case 1:// for coin upgrade
			{
				if(_totalcoin >= _coinUpgrade.totalUpgrade[_coinUpgrade.currentUpgrade].UpgradeCost)
				{
					AddCoin(-_coinUpgrade.totalUpgrade[_coinUpgrade.currentUpgrade].UpgradeCost);
					_coinUpgrade.currentUpgrade +=1;
					UpdateIntDataInDevice("coinCurrentUpdate" ,_coinUpgrade.currentUpgrade,false);
					isUpgrade = true;
				}
				break;
			}
			
			case 2: // for xp upgrade
			{
				if(_totalcoin >= _xpUpgrade.totalUpgrade[_xpUpgrade.currentUpgrade].UpgradeCost)
				{
					AddCoin(-_xpUpgrade.totalUpgrade[_xpUpgrade.currentUpgrade].UpgradeCost);
					_xpUpgrade.currentUpgrade +=1;
					UpdateIntDataInDevice("xpCurrentUpdate" ,_xpUpgrade.currentUpgrade,false);
					isUpgrade = true;
				}
				break;
			}
			
			case 3: // for time upgrade 
			{
				if(_totalcoin >=  _timeUpgrade.totalUpgrade[_timeUpgrade.currentUpgrade].UpgradeCost)
				{
					AddCoin(-_timeUpgrade.totalUpgrade[_timeUpgrade.currentUpgrade].UpgradeCost);
					_timeUpgrade.currentUpgrade +=1;
					UpdateIntDataInDevice("timeCurrentUpdate" ,_timeUpgrade.currentUpgrade,false);
					isUpgrade = true;
				}
				break;
			}
			
			case 4: // for life upgrade
			{
				if(_totalcoin >= _lifeUpgrade.totalUpgrade[_lifeUpgrade.currentUpgrade].UpgradeCost)
				{
					AddCoin(-_lifeUpgrade.totalUpgrade[_lifeUpgrade.currentUpgrade].UpgradeCost);
					_lifeUpgrade.currentUpgrade +=1;
					UpdateIntDataInDevice("lifeCurrentUpdate" ,_lifeUpgrade.currentUpgrade,false);
					isUpgrade = true;
				}
				break;
			}
		}
		GuiTextManager.instance.UpdateText();
		return(isUpgrade);
	}
	
	public void UpdateLifeCostinDevice()
	{
		for(int i=0;i<_moreLife.Length;i++)
		{
			int x =0;
			if(_lifeUpgrade.currentUpgrade>0)
			 x = _moreLife[i]._lifeCost - ((_moreLife[i]._lifeCost*_lifeUpgrade.totalUpgrade[_lifeUpgrade.currentUpgrade-1].UpgradePercentage)/100);
			else 
				x = _moreLife[i]._lifeCost;
			
			UpdateIntDataInDevice("moreLifeCost"+i,x,false);
			_moreLife[i]._lifeCost = UpdateIntDataInDevice("moreLifeCost"+i,x,true);
		}
	}
	
	public void RetriveLifeCost()
	{
		for(int i=0;i<_moreLife.Length;i++)
		{
			_moreLife[i]._lifeCost = UpdateIntDataInDevice("moreLifeCost"+i,_moreLife[i]._lifeCost,true);
		}
	}
	
	public void UpdateTimeCostInDevice()
	{
		for(int i=0;i<_moreTime.Length;i++)
		{
			int x;
			if(_timeUpgrade.currentUpgrade>0)
			 x = _moreTime[i]._extraTimeCost- ((_moreTime[i]._extraTimeCost*_timeUpgrade.totalUpgrade[_timeUpgrade.currentUpgrade-1].UpgradePercentage)/100);
			else
				x = _moreTime[i]._extraTimeCost;
			UpdateIntDataInDevice("moreTimeCost"+i,x,false);
			_moreTime[i]._extraTimeCost = UpdateIntDataInDevice("moreTimeCost"+i,x,true);
		}
	
	}
	
	public void RetriveTimeCost()
	{
		for(int i=0;i<_moreTime.Length;i++)
		{
			_moreTime[i]._extraTimeCost = UpdateIntDataInDevice("moreTimeCost"+i,_moreTime[i]._extraTimeCost,true);
		}
	}
	
	public StatsData GetStatsData()
	{
		return(_statsData);
	}
	
	public void RetriveThroughRingsCondition(bool isfromDevice)
	{
		if(isfromDevice)
		{
			for(int i =0;i<_isthroughrings.Length;i++)
			{
				UpdateIntDataInDevice("_isthroughrings"+i,_isthroughrings[i],false);
			}
		}
		
		else
		{
			for(int i =0;i<_isthroughrings.Length;i++)
			{
				_isthroughrings[i] = UpdateIntDataInDevice("_isthroughrings"+i,_isthroughrings[i],true);
			}
		}
	}
	
	public void RetriveHittheBarCondition(bool isfromDevice)
	{
		if(isfromDevice)
		{
			for(int i =0;i<_isHitTheBar.Length;i++)
			{
				UpdateIntDataInDevice("_isHitTheBar"+i,_isHitTheBar[i],false);
			}
		}
		
		else
		{
			for(int i =0;i<_isHitTheBar.Length;i++)
			{
				_isHitTheBar[i] = UpdateIntDataInDevice("_isHitTheBar"+i,_isHitTheBar[i],true);
			}
		}
	}
	
	public void RetriveNoswingCondition(bool isfromDevice)
	{
		if(isfromDevice)
		{
			for(int i =0;i<_isNoSwing.Length;i++)
			{
				UpdateIntDataInDevice("_isNoSwing"+i,_isNoSwing[i],false);
			}
		}
		
		else
		{
			for(int i =0;i<_isNoSwing.Length;i++)
			{
			   _isNoSwing[i] = UpdateIntDataInDevice("_isNoSwing"+i,_isNoSwing[i],true);
			}
		}
	}
	
	public void SaveThroughRingsCondition(int index)
	{
		
		_isthroughrings[index] = 1;
		UpdateIntDataInDevice("_isthroughrings"+index,_isthroughrings[index],false);
	}
	
	public void SaveHitTheBarCondition(int index)
	{
		_isHitTheBar[index] = 1;
		UpdateIntDataInDevice("_isHitTheBar"+index,_isHitTheBar[index],false);
	}
	
	public void SaveNoSwingCondition(int index)
	{
		_isNoSwing[index] = 1;
		UpdateIntDataInDevice("_isNoSwing"+index,_isNoSwing[index],false);
	}
	
	
	public void updateAchievements (int ach_index)
	{


		switch (ach_index)
		{
			// for Ready To Roll From Training Mode
			case 1:
				if(PlayerPrefs.GetInt ("ReadyToRoll") == 0)
				{
//					print ("Ready To Roll");
					PlayerPrefs.SetInt ("ReadyToRoll", 1);
					//""---> ReadyToRollID
				}	
			break;
			
			//for playing ClockShowdown  Time Keeper
			case 2:
				if(PlayerPrefs.GetInt ("TimeKeeper") == 0)
				{
					//print ("Time Keeper");	
					PlayerPrefs.SetInt ("TimeKeeper", 1); 
				}
			break;
			
			//for scoring 10000 in ClockShowDown The Hourglass 
			case 3:
				if(_scoreManager.CurrentScore() >= 10000)
					if(PlayerPrefs.GetInt ("TheHourGlass") == 0)
					{
						//print ("The Hour Glass");
						PlayerPrefs.SetInt ("TheHourGlass",1);
					}
			break;
			
			//for scoring 20000 in clockshowDown Mr. Clock
			case 4:
				if(_scoreManager.CurrentScore() >= 20000)
					if(PlayerPrefs.GetInt ("MrClock") == 0)
					{
						//print ("Mr. Clock");
						PlayerPrefs.SetInt ("MrClock",1);
					}
			break;
			
			//for scoring 50000 in clockshowDown Killer Instinct
			case 5:
				if(_scoreManager.CurrentScore() >= 50000)
					if(PlayerPrefs.GetInt ("KillerInstinct") == 0)
					{
						//print ("Killer Instinct");
						PlayerPrefs.SetInt ("KillerInstinct",1);
					}
			break;
			
			//clear 10 Rounds Sharp Fangs
			case 6:
				if(TimeAttack.instance.GetNoOfRound() == 10)
					if(PlayerPrefs.GetInt ("SharpFangs") == 0)
					{
						//print ("Sharp Fangs");
						PlayerPrefs.SetInt ("SharpFangs",1);
					}
			break;
			
			//clear 20 rounds Bullet Flicker
			case 7:
				if(TimeAttack.instance.GetNoOfRound() == 20)
					if(PlayerPrefs.GetInt ("BulletFlicker") == 0)
					{
						//print ("Bullet Flicker");
						PlayerPrefs.SetInt("BulletFlicker",1);
					}
			break;
			
			//clear 50 rounds nsane Flick
			case 8:
				if(TimeAttack.instance.GetNoOfRound() == 50)
					if(PlayerPrefs.GetInt ("InsaneFlick") == 0)
					{
						//print ("Insane Flick");
						PlayerPrefs.SetInt("InsaneFlick",1);
					}
			break;
			
			//play for 20 mins Time Shifter
			case 9:
				if(PlayerPrefs.GetInt ("TimeShifter") == 0)
				{
					//print ("Time Shifter");
					PlayerPrefs.SetInt ("TimeShifter", 1);
				}
			break;
			
			//play for 40 mins God of flick 
			case 10:
				if(PlayerPrefs.GetInt("Godofflick") == 0)
				{
					//print ("God of flick");
					PlayerPrefs.SetInt("Godofflick",1);
				}
			break;
			
			//play for 60 min Flick Freak
			case 11:
				if(PlayerPrefs.GetInt ("FlickFreak") == 0)
				{
					//print ("Flick Freak");
					//PlayerPrefs.SetInt("FlickFreak",1);
				}
			break;
			
			//Gold in all through the ring challenges Ring Master
			case 12:
				if(noOfThroughRingsGold == 15)
				if(PlayerPrefs.GetInt ("RingMaster") == 0)
				{
					//print ("Ring Master");
					//PlayerPrefs.SetInt ("RingMaster",1);
				}
			break;
			
			//Gold in all hit the bar challenges Pole Magnet
			case 13:
				if(noOfHitTheBarGold == 12)
				if(PlayerPrefs.GetInt ("PoleMagnet") == 0)
				{
					//print ("Pole Magnet");
					//PlayerPrefs.SetInt ("PoleMagnet",1);
				}
			break;
			 //Gold in all no swing in storm The Sailor
			case 14:
				if(PlayerPrefs.GetInt ("TheSailor") == 0)
				{
//					print ("The Sailor");
					//PlayerPrefs.SetInt ("TheSailor",1);
				}
			break;
			
			//play with all balls once  Variant gamer
			case 15:
				if(PlayerPrefs.GetInt ("ball4Activated") == 1 && PlayerPrefs.GetInt ("ball3Activated") == 1 && PlayerPrefs.GetInt ("ball2Activated") == 1)
				if(PlayerPrefs.GetInt ("Variantgamer") == 0)
				{
					//print ("Variant gamer");
					//PlayerPrefs.SetInt ("Variantgamer",1);
				}
			break;
			
			//Get all upgrades to full Skilled 100%
			case 16:
				if(_lifeUpgrade.currentUpgrade == 4 && _xpUpgrade.currentUpgrade == 4 && _timeUpgrade.currentUpgrade == 4 && _coinUpgrade.currentUpgrade == 4)
				{
					
					if(PlayerPrefs.GetInt ("Skilled100") == 0)
					{
						//print ("Skilled 100%");
						PlayerPrefs.SetInt ("Skilled100", 1);
					}
				}
			break;
			
			//Reach level 10 in game  Amateur
			case 17:
				if(levelNo >= 10)
				if(PlayerPrefs.GetInt ("Amateur") == 0)
				{
					//print ("Amateur");
					PlayerPrefs.SetInt ("Amateur",1);
				}
			break;
			
			//Reach level 20 in game  Regular
			case 18:
				if(levelNo >= 20)
				if(PlayerPrefs.GetInt ("Regular") == 0)
				{
					//print ("Regular");
					PlayerPrefs.SetInt ("Regular",1);
				}
			break;
			
			//Reach level 50 in game  World Class
			case 19:
				if(levelNo >= 50)
				if(PlayerPrefs.GetInt ("WorldClass") == 0)
				{
					//print ("World Class");
					//PlayerPrefs.SetInt ("WorldClass",1);
				}
			break;
			
			
			//Reach level 70 in game  Veteran
			case 20:
				if(levelNo >= 70)
				if(PlayerPrefs.GetInt ("Veteran") == 0)
				{
					//print ("Veteran");
					PlayerPrefs.SetInt ("Veteran",1);
				}
			break;
			
			//play once sudden death    Dare to play
			case 21:
				if(PlayerPrefs.GetInt ("Daretoplay") == 0)
				{
					PlayerPrefs.SetInt ("Daretoplay", 1);
					//print ("Dare to play");
				}
			break;
			
			//The Survivor
			case 22:
				if(PlayerPrefs.GetInt ("TheSurvivor") == 0)
				{
					PlayerPrefs.SetInt ("TheSurvivor", 1);
					//print ("The Survivor");
				}
			break;
			
			//The Immortal
			case 23:
				if(PlayerPrefs.GetInt ("TheImmortal") == 0)
				{
					PlayerPrefs.SetInt ("TheImmortal", 1);
					//print ("The Immortal");
				}
			break;
			
			//The Creator
			case 24:
				if(PlayerPrefs.GetInt ("TheCreator") == 0)
				{
					PlayerPrefs.SetInt ("TheCreator", 1);
					//print("The Creator");
				}
			break;
			
			//Mr Rich
			case 25:
				if(PlayerPrefs.GetInt ("MrRich") == 0)
				{
					PlayerPrefs.SetInt ("MrRich", 1);
					//print ("Buy 10 packs of each ball");
				}
			break;
		}
	}
	
	public void IncrementBallCountOfIndex (int index)
	{
		switch (index)
		{
			case 2:
				PlayerPrefs.SetInt ("ball2Counter", PlayerPrefs.GetInt ("ball2Counter")+1);
			break;
			
			case 3:
				PlayerPrefs.SetInt ("ball3Counter", PlayerPrefs.GetInt ("ball3Counter")+1);
			break;
			
			case 4:
				PlayerPrefs.SetInt ("ball4Counter", PlayerPrefs.GetInt ("ball4Counter")+1);
			break;
		}
		
		if(PlayerPrefs.GetInt ("ball2Counter") >= 10 && PlayerPrefs.GetInt ("ball3Counter") >= 10 && PlayerPrefs.GetInt ("ball4Counter") >= 10)
			updateAchievements (25);
	}
}