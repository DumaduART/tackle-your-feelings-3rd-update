using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {
//	 public static CameraMovement instance;
	 public GameObject rightCameraRange;
	 public GameObject leftCameraRange;
	 public GameObject _gtarget;
	 public GameObject _glookAt;
	 public GameObject _cameraObject;
	 public InputManager _inputManager;
	 public GameManager _gameManager;
	 public BallHandler _ballHandler;
	 public EventManager _eventManager;	
	 private bool mblookAt=true;
	 private Quaternion mqRotaion;
	 private float _frotaionSpeed=0.10f;
	 private float _fmovingSpeed=0.15f;
	 private bool mbfollowtarget=true;
	 private bool mbrotation;
	 private float mCameraSpeed;
	 private Vector3 targetAngel;
	 private Vector3 targetPos;
	// Use this for initialization
	void Start () {
//	instance=this;
	}
	
	// Update is called once per frame
	void Update () {
		if(_gtarget==null)
			return ;
		
		else
		{   
			if(mbrotation)
			{
				mqRotaion=Quaternion.LookRotation(new Vector3(0,_glookAt.transform.position.y,_glookAt.transform.position.z)- new Vector3(0,transform.position.y,transform.position.z));
				transform.rotation = Quaternion.Slerp(transform.rotation, mqRotaion, Time.deltaTime * _frotaionSpeed);	
				
			}
			if(mbfollowtarget)
			{
				if(_gtarget.transform.position.z-transform.position.z>2)
				{
					transform.position=Vector3.MoveTowards(transform.position,
					new Vector3(_gtarget.transform.position.x,_gtarget.transform.position.y/*-_gtarget.transform.position.y*/,_gtarget.transform.position.z)
					,_fmovingSpeed*Time.deltaTime*mCameraSpeed);
//					mqRotaion=Quaternion.LookRotation(new Vector3(0,_glookAt.transform.position.y,_glookAt.transform.position.z)- new Vector3(0,transform.position.y,transform.position.z));
//				    transform.rotation = Quaternion.Slerp(transform.rotation, mqRotaion, Time.deltaTime * _frotaionSpeed/4);
				}
			}
		}
		if(transform.position.x<leftCameraRange.transform.position.x)
		{
			GameObject refOfBall=_inputManager.GetRefOfBall();
		    Camera camera =_inputManager.GetCameraref();
			_ballHandler.GenerateBall(camera,refOfBall);
		}
		if(transform.position.x>rightCameraRange.transform.position.x)
		{
			GameObject refOfBall=_inputManager.GetRefOfBall();
		    Camera camera =_inputManager.GetCameraref();
			_ballHandler.GenerateBall(camera,refOfBall);
		}
	   if(transform.position.z>_cameraObject.transform.position.z)
		{
			
			mCameraSpeed = Mathf.MoveTowards(mCameraSpeed,2.0f,Time.deltaTime*mCameraSpeed/1.5f);	
		}
	}
    public void SetMoveCondition(bool istrue)
	{
		if(istrue)
		{
			mbfollowtarget=false;
		}
		else
		{
			mbfollowtarget=true;
		}
	}
	public void SetRotateCondition(bool istrue)
	{
		
		if(istrue)
		{
			mbrotation=false;
		}
		else
		{
			mbrotation=true;
		}
	}
	public void ChangeCameraDirection(GameObject ball)
	{
		if(ball==null)
			return ;
		Vector3 dir=_glookAt.transform.position-ball.transform.position;
		ball.transform.forward=new Vector3(dir.x,0,dir.z).normalized;
		Vector3 angel = ball.transform.eulerAngles;
		ball.transform.eulerAngles=new Vector3(0,angel.y,angel.z);
		//transform.eulerAngles = new Vector3(354,angel.y,angel.z);
		targetAngel = new Vector3(354,angel.y,angel.z);
		targetPos = ball.transform.TransformPoint(new Vector3(.25f,1.6f,-5.8f));
		_eventManager.SetEuelerAngel(ball.transform.eulerAngles);
		StartCoroutine( ResetCamera(transform.position,transform.eulerAngles));
	}
	public void CameraSpeed(float cameraSpeed)
	{
		mCameraSpeed = cameraSpeed/2;
	}
	
	public IEnumerator ResetCamera(Vector3 startPos,Vector3 startAngel)
	{  
		while(transform.position != targetPos)
		{
			
//			transform.position = Vector3.MoveTowards(transform.position,targetPos,((1/(5*(transform.position-targetPos).magnitude)) + 25) * Time.deltaTime);
//			transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles,targetAngel,5 * Time.deltaTime);
			transform.position = Vector3.Lerp(transform.position,targetPos,Time.time);
			transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,targetAngel,Time.time);
			
			yield return new WaitForEndOfFrame();
		}
		
		
	}
	
	public void SetCameraObjPos(Vector3 pos)
	{
		float dist ;
		dist = (((15-0)*(_gameManager.GetDistanceValue()-25))/(50-25))+0;
		Vector3 temp = _cameraObject.transform.position;
		temp.z = pos.z +dist;
		 _cameraObject.transform.position = temp;
	}
}
