using UnityEngine;
using System.Collections;

public class CollisionDetection : MonoBehaviour {

	// Use this for initialization
	public enum BallStatus
	{
		nothitted=0,
		hitted
	};
	public enum BallCrossStatus
	{
		notCrossed=0,crossed
	};
	public enum flyStatus
	{
		ground=0,air
	};
	public flyStatus _flyStatus;
	public BallStatus ballStatus;
	public BallCrossStatus ballCrossedStatus;
	public GameObject particle;
	public GameObject particle1;
	public GameObject projectileObj;
	public GameObject tapToskipSubScreen;
	public bool isProjectile ;
	public GameObject ringPrefab;
	public float FrontRange;
	public float leftRange;
	public float rightRange;
	private InputManager _inputManager;
	private CameraMovement _cameraMovement;
	private BallHandler _ballHandler;
	private GameManager _gameManager;
	private ScoreManager _scoreManager;
	private ParticleEffectManager _particleEffectManager;
	private bool isbar=true;
	private bool isring;
	private bool istarget=true;
	private bool isgoal=true;
	private bool isTaptoSkip;
	private bool isScore;
	private bool isGround = true;
	private bool isPoleHitted = true;
	private bool ismissed = true;
	public  float startTime;
	public  int time;
	private float pauseStartTime;
	public int totalPauseTime;
	public int waitingTime = 5;
	public int totaltime;
	private GameObject ringClone;
	private GameObject taptoSkip;
	public GameObject _scoreincreaser;
	
	
	private float PoleHittingTime;
	
	InstructionManager instructionManager;
	void Start () {

		 EventManager.Pause += Pause;
		 EventManager.Resume += Resume;

		_inputManager=GameObject.Find("GameManager").GetComponent<InputManager>();
		_cameraMovement=GameObject.Find("Main Camera").GetComponent<CameraMovement>();
		_ballHandler=GameObject.Find ("GameManager").GetComponent<BallHandler>();
		_gameManager=GameObject.Find ("GameManager").GetComponent<GameManager>();
		_scoreManager=GameObject.Find("GameManager").GetComponent<ScoreManager>();
		_particleEffectManager = GameObject.Find("GameManager").GetComponent<ParticleEffectManager>();
		instructionManager = InstructionManager.GetInstance();
		
	}

	void OnDestroy()
	{
		EventManager.Pause -= Pause;
		EventManager.Resume -= Resume;
	}
	
	// Update is called once per frame
	Vector3 targetSpeed = Vector3.zero;
	void Update () {
		     
			if(isProjectile)
			{
				if(_inputManager.GetInputStatus()== InputManager.InputStatus.capturingSwing)
				{
					isProjectile=false;
					Instantiate(projectileObj,transform.position,Quaternion.identity);
				    StartCoroutine(DelayProjectile(.1f));
				}
			}
		
			if(isTaptoSkip)
			{
				time = (int)(Time.time-startTime);   
			}
			if(transform.position.x>30||transform.position.x<-36)
			{
				GenerateBall();
				if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
					EventManager.practice.SetActive(true);
			}
		   if(ballStatus == BallStatus.hitted)
			{
				if(time >= waitingTime)
				{
					
					GenerateBall();
					if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
					{
						if(EventManager.practice)
						EventManager.practice.SetActive(true);
					}
					
					
				}
			}
		
			if(!isPoleHitted && ballStatus == BallStatus.nothitted)
			{
				if(Time.time-PoleHittingTime>=.25f)
				{
					Instantiate(tapToskipSubScreen);
					 ballStatus = BallStatus.hitted;
					Delay(2);
				}
			}
			
		//C ROLAND	
		   RaycastHit hit;
		   if (Physics.Raycast(transform.position,transform.GetComponent<Rigidbody>().velocity, out hit,10))
		   {
			 // print ("raycast hitting");
				if(hit.collider.tag == "raycast")
				{
					
					if(!isring)
					{
						
						if(ringClone)
							Destroy(ringClone);
						ringClone =	Instantiate(ringPrefab,hit.point,ringPrefab.transform.rotation) as GameObject;
						isring = true;
					}
					if(ringClone)
					ringClone.transform.position = hit.point;
				}
				
				if(ringClone)
					{
						
						Vector3 scale = ringClone.transform.localScale;
						scale.x = ((.4f-.05f)/(10.0f))*(116.0f-transform.position.z)+.05f;
						scale.z = ((.4f-.05f)/(10.0f))*(116.0f-transform.position.z)+.05f;
						ringClone.transform.localScale = scale;
					}
				    
		   }
		
		if(transform.position.z>115||(transform.position.z>100&&transform.position.y<-15))
		{
			if(ringClone)
			{
				Destroy(ringClone);
				if(transform.position.y>-15 && transform.position.x<1.3f && transform.position.x>-11.2f)
				{
					_particleEffectManager.GenerateParticle(ParticleEffectType.waveParticle,transform.position);
					_particleEffectManager.GenerateParticle(ParticleEffectType.Goal,transform.position);
				}
			}
		}
		if((transform.position.z>FrontRange || transform.position.x>rightRange ||transform.position.x<leftRange) && istarget)
		{
			
			ballStatus = BallStatus.hitted;
			if(GameObject.Find("TaptoSkipSubscreen(Clone)") == null)
				Instantiate(tapToskipSubScreen);
			Delay(2);
			istarget=false;
			_inputManager.ResetSwingVelocity();
			_cameraMovement.SetRotateCondition(false);
			_inputManager.SetInputStatus(InputManager.InputStatus.none);
			
			if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
			{
				TimeAttack timeAttack=TimeAttack.instance;
				timeAttack.ChangeTimerCondition(true);
			}
			
			if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
			{
				SkillShots skillShot= SkillShots.instance;
				LevelHandler.instance.ChangeTimerCondition(true);
			}
			
		}
		
	}
	
	void OnTriggerEnter(Collider other)
	{
//		if(other.collider.tag == "gaol")
//		{
//			ismissed = false;
//		}
		
		
		if(ballStatus == BallStatus.nothitted)
		{	
			
			//ballStatus = BallStatus.hitted;
			if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)//Time Attack Mode
			{
				TimeAttack timeAttack=TimeAttack.instance;
				timeAttack.ChangeTimerCondition(true);
				if(other.GetComponent<Collider>().transform.parent.tag == "bar" && isbar)
				{
					GameObject temp = other.GetComponent<Collider>().transform.parent.gameObject;
					BarAnimation barAnimation = temp.GetComponent<BarAnimation>();
					 barAnimation.ExitAnimation();
					_scoreManager.ScoreOfTimeAttackShot(ScoreManager.GoalType.HittingZone);
					PopUpManager.instance.CreatePopUp(PopUpType.scoreincreaser);
					TimeAttack.SetNoOfAttempts();
					timeAttack.SetScore(_scoreManager.GetScorePerShot());
					timeAttack.ChangeBlinkZone(other.GetComponent<Collider>().transform.parent.gameObject);
					isScore = true;
					timeAttack.SetHittedStatus(TimeAttack.HittedStatus.hitted);
					isbar=false;	
					timeAttack.SetHittedPattern(other.GetComponent<Collider>().transform.parent.name);
					PopUpManager.instance.CreatePopUp(PopUpType.timeIncreaser);
					
					if((75<_inputManager.ChkAngel())&&(_inputManager.ChkAngel()<105))
					{
						//instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"PRETTY NICE, YOU WILL LEARN REAL SOON",true);
					}
					
					else
					{
						//instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"IF FLICK IS OUTSIDE GOAL SCORING RANGE, BUT \n LAST MOMENT SWING MAKES IT GO IN",true);
					}
					
					if(timeAttack.GetFlashingZoneCondition())
					{
						//timeAttack.SetTime(2);
						SoundManager.instance.OnGoal(2);
						PopUpManager.instance.CreatePopUp(PopUpType.multiplierIncreaser);
						
					}
					else
					{
						//timeAttack.SetTime(1);
						SoundManager.instance.OnGoal(1);
						
					}
					//timeAttack.ChangeWindDirection();
					timeAttack.ChangeTimerCondition(true);
					//Destroy(other.transform.parent.gameObject);
				//	_particleEffectManager.GenerateParticle(ParticleEffectType.Goal,transform.position);	
					_particleEffectManager.GenerateParticle(ParticleEffectType.celebration,Vector3.zero);
					//print (other.collider.transform.name);	suddendeath.DestroyTarget(
					
					if(other.GetComponent<Collider>().transform.parent.name == "Bar_1(Clone)")
					{
						if(timeAttack.GetFlashingZoneCondition())
						{
							PopUpManager.instance.CreatePopUp(PopUpType.awesome);
						}
						GameObject.Find("spark1(Clone)").GetComponent<AutoDestruct>().SetColor(0, 0, 1, 1);
						GameObject.Find("spark2(Clone)").GetComponent<AutoDestruct>().SetColor(0, 0, 1, 1);

					}
					else if(other.GetComponent<Collider>().transform.parent.name == "Bar_2(Clone)")
					{
						if(timeAttack.GetFlashingZoneCondition())
						{
							PopUpManager.instance.CreatePopUp(PopUpType.great);
						}
						GameObject.Find("spark1(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0.92f, 0.016f, 1);
						GameObject.Find("spark2(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0.92f, 0.016f, 1);
					
					}
					else if(other.GetComponent<Collider>().transform.parent.name == "Bar_3(Clone)")
					{
						if(timeAttack.GetFlashingZoneCondition())
						{
							PopUpManager.instance.CreatePopUp(PopUpType.great);
						}
						GameObject.Find("spark1(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0, 1, 1);
						GameObject.Find("spark2(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0, 1, 1);
						
					}
					else if(other.GetComponent<Collider>().transform.parent.name == "Bar_4(Clone)")
					{
						if(timeAttack.GetFlashingZoneCondition())
						{
							PopUpManager.instance.CreatePopUp(PopUpType.good);
						}
						GameObject.Find("spark1(Clone)").GetComponent<AutoDestruct>().SetColor(0, 1, 0, 1);
						GameObject.Find("spark2(Clone)").GetComponent<AutoDestruct>().SetColor(0, 1, 0, 1);
						
					}
					else if(other.GetComponent<Collider>().transform.parent.name == "Bar_5(Clone)")
					{
						if(timeAttack.GetFlashingZoneCondition())
						PopUpManager.instance.CreatePopUp(PopUpType.good);
						GameObject.Find("spark1(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0, 0, 1);
						GameObject.Find("spark2(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0, 0, 1);
						
					}
				}
				
                if(other.GetComponent<Collider>().tag=="gaol" || other.GetComponent<Collider>().name == "goalCollider" && isbar)
				{
					
					isbar = false;
					_scoreManager.ScoreOfTimeAttackShot(ScoreManager.GoalType.normalGoal);
					timeAttack.SetScore(_scoreManager.GetScorePerShot());
					TimeAttack.SetNoOfAttempts();
					SoundManager.instance.OnGoal(0);
					PopUpManager.instance.CreatePopUp(PopUpType.scoreincreaser);
				}
				
				
	
			}
			
				
					
					if(other.GetComponent<Collider>().transform.parent.tag == "ring")
					{  
						if(_gameManager.GetCategory() == GameManager .Category.ThroughRings)
						{
							other.GetComponent<Collider>().GetComponent<MeshRenderer>().enabled=false;
							_ballHandler.ring.Add(other.GetComponent<Collider>().gameObject);
							GameObject obj = other.GetComponent<Collider>().transform.parent.gameObject;
							LevelAssetHandler levelAssetsHandler= obj.GetComponent<LevelAssetHandler>();
						//	print (other.collider.transform.parent.name);
							levelAssetsHandler.SetAssetCleared(false);
							SoundManager.instance.PassThroughRingsSound();
						}
					}
					
				
            if(other.GetComponent<Collider>().tag=="gaol" || other.GetComponent<Collider>().name == "goalCollider" && isgoal)
					{
						if(_gameManager.GetGameMode() == GameManager.GameMode.Training)
						{
							SoundManager.instance.OnGoal(0);
							isgoal = false;
						}
						if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
						{
							SkillShots skillShot= SkillShots.instance;
							LevelHandler levelhandler = LevelHandler.instance;
							levelhandler.ChangeTimerCondition(true);
							isgoal=false;
						
							if(_gameManager.GetCategory() == GameManager .Category.NoSwingStorm)
							{
								levelhandler.SetSuccessFullShot();	
							//	_particleEffectManager.GenerateParticle(ParticleEffectType.Goal,transform.position);	
								_particleEffectManager.GenerateParticle(ParticleEffectType.celebration,Vector3.zero);	
								PopUpManager.instance.CreatePopUp(PopUpType.awesome);
								SoundManager.instance.OnGoal(2);
							}
						
							if(_gameManager.GetCategory() == GameManager.Category.ThroughRings)
							{
//								Debug.Log("Successfull Shots"+levelhandler.GetSuccessFulShot());
								if(levelhandler.ChKLevelCompletion())
								{
									
									levelhandler.SetSuccessFullShot();
								//	_particleEffectManager.GenerateParticle(ParticleEffectType.Goal,transform.position);	
									_particleEffectManager.GenerateParticle(ParticleEffectType.celebration,Vector3.zero);	
									PopUpManager.instance.CreatePopUp(PopUpType.awesome);
									SoundManager.instance.OnGoal(2);
									
									 levelhandler.SetFinishedConsecutive();
								}
								
								else
								{
									SoundManager.instance.OnMissed();
									
									levelhandler.ResetFinishedConsecutive();	
								}
						
							}
					
							if(_gameManager.GetCategory() == GameManager .Category.HitTheBar && ismissed)
							{
								SoundManager.instance.OnMissed();
							}
						}
						
						if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath && isbar)
						{
							isbar = false;
							SuddenDeath suddendeath = SuddenDeath.instance;
							suddendeath.SetGoalCondition(true);
							suddendeath.UpdateGoalCounter();
							suddendeath.CalculateScore("gaol");
							PopUpManager.instance.CreatePopUp(PopUpType.scoreincreaser);
							PopUpManager.instance.CreatePopUp(PopUpType.good);
							SoundManager.instance.OnGoal(0);
						//	Instantiate(_scoreincreaser);
							//suddendeath.UpdateTargetMultiplier(-1);
//							suddendeath.UpdatePowerUpCounter();
							
						}
					   
					}
			if(other.GetComponent<Collider>().transform.parent.tag == "bar" && isbar)
			{
				isbar = false;
				GameObject temp = other.GetComponent<Collider>().transform.parent.gameObject;
				BarAnimation barAnimation = temp.GetComponent<BarAnimation>();
				if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
				{  
					
					SuddenDeath suddendeath = SuddenDeath.instance;
					suddendeath.SetGoalCondition(true);
					suddendeath.UpdateGoalCounter();
					barAnimation.ExitAnimation(); //suddendeath.DestroyTarget(0.0f));
					if(suddendeath.GetCurrentcontinuousGoal()<5)
					{
						PopUpManager.instance.CreatePopUp(PopUpType.great);
					}
					else
					{
						PopUpManager.instance.CreatePopUp(PopUpType.awesome);
					}
					suddendeath.CalculateScore(other.GetComponent<Collider>().tag);
					
					suddendeath.UpdateTargetMultiplier(1);
					_particleEffectManager.GenerateParticle(ParticleEffectType.Goal,transform.position);	
					_particleEffectManager.GenerateParticle(ParticleEffectType.celebration,Vector3.zero);
					//_particleEffectManager.GenerateParticle(ParticleEffectType.barHittingEffect,transform.position);
					SoundManager.instance.OnGoal(2);
					PopUpManager.instance.CreatePopUp(PopUpType.scoreincreaser);
					PopUpManager.instance.CreatePopUp(PopUpType.multiplierIncreaser);
					GameObject.Find("spark1(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0, 0, 1);
					GameObject.Find("spark2(Clone)").GetComponent<AutoDestruct>().SetColor(1, 0, 0, 1);
				}
			}
			
		}
		
        if(other.GetComponent<Collider>().tag == "gaol" || other.GetComponent<Collider>().name == "goalCollider")
		{
			ismissed = false;
		}
	}
	void OnCollisionEnter(Collision collision)
	{
			_inputManager.SetInputStatus(InputManager.InputStatus.none);
			if(collision.collider.tag=="pol" || collision.collider.name == "PoleBaseCollider")
			{
				_cameraMovement.SetRotateCondition(false);
				if(GameObject.Find("TaptoSkipSubscreen(Clone)") == null)
				{
					_gameManager.ChangePoleColor(false);
//					Instantiate(tapToskipSubScreen);
//					 ballStatus = BallStatus.hitted;
				}
			
				if(ringClone)
				{
					Destroy(ringClone);
				}
				
				
			}
		
			if(collision.collider.name=="SideColliders"||(collision.collider.name=="GroundCollider" && isGround ))//
			{
			
			    isGround = false;
				_inputManager.ResetSwingVelocity();
				_cameraMovement.SetMoveCondition(true);
				_cameraMovement.SetRotateCondition(false);
				if(_flyStatus == flyStatus.air)
				{
					if(GameObject.Find("TaptoSkipSubscreen(Clone)") == null)
						Instantiate(tapToskipSubScreen);
				}
				Delay(2);
				
				 ballStatus = BallStatus.hitted;
			}
	
					if(collision.collider.tag=="pol"  && isPoleHitted)
					{
						PoleHittingTime = Time.time;
						isPoleHitted = false;
						if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
						{
							LevelHandler.instance.ChangeTimerCondition(true);
							SkillShots skillShot = SkillShots.instance;
							if(_gameManager.GetCategory() == GameManager .Category.HitTheBar)
							{
								ismissed = false;
								LevelHandler levelHanler = LevelHandler.instance;
//								print (collision.collider.name);
								levelHanler.SetSuccessFullShot();
								if(collision.collider.name == "poleColliderLeft")
									_particleEffectManager.GenerateParticle(ParticleEffectType.PoleHittingLeft,transform.position);	
								else 
									_particleEffectManager.GenerateParticle(ParticleEffectType.PoleHittingRight,transform.position);	
								PopUpManager.instance.CreatePopUp(PopUpType.awesome);
								SoundManager.instance.OnGoal(2);
							}
						}
						SoundManager.instance.GoalPost();
					}
//				}
					
//			}
			_inputManager.ResetSwingVelocity();	
//		}	
	}
//	IEnumerator Delay(float time)
//	{
//		yield return new WaitForSeconds(time);
//	    GameObject refOfBall=_inputManager.GetRefOfBall();
//		Camera camera =_inputManager.GetCameraref();
//		_ballHandler.GenerateBall(camera,refOfBall);
//		
//	}
    public void Delay(int x)
	{
		
		waitingTime = x;
		startTime = (int)Time.time;	
		isTaptoSkip=true;
		if(ismissed)
		{
			ismissed = false;
			SoundManager.instance.OnMissed();
		}
		
		if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
		{
			TimeAttack timeAttack = TimeAttack.instance;
			timeAttack.ChangeTimerCondition(true);
		}
	}
	void GenerateBall()
	{
		if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
		{
			SuddenDeath suddendeath = SuddenDeath.instance;
			suddendeath.UpdatePowerUpCounter();
			suddendeath.DeActivateZone();
			suddendeath.UpdateRoundUpParameter();
		}
		
		if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
		{
			TimeAttack.instance.SetContinuousGoal();
		}
		
		GameObject refOfBall=_inputManager.GetRefOfBall();
		Camera camera =_inputManager.GetCameraref();
		_ballHandler.GenerateBall(camera,refOfBall);
		if(GameObject.Find("TaptoSkipSubscreen(Clone)")!= null)
		   GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
		if(_gameManager.GetGameMode()!=GameManager.GameMode.Training)
		if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode)
		   BallSelectionSubScreenManager.instance.ReduceBallCount();
		
	}
	public Vector3 GetVelocity(GameObject ball)
	{
		return(ball.GetComponent<Rigidbody>().velocity);
	}
	
	
	
	
	IEnumerator DelayProjectile(float time)
	{
		yield return new WaitForSeconds(time);
		isProjectile=true;
	}
	private void Pause()
	{
		
		isTaptoSkip=false;
		pauseStartTime = (int) Time.time;
		if(GameObject.Find("TaptoSkipSubscreen(Clone)"))
		{
		    	GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().DisableInput();
				if(GameObject.Find("TapToskip(Clone)"))
			    {
					taptoSkip = GameObject.Find("TapToskip(Clone)");
					taptoSkip.SetActive(false);
			   }
		}
	}
	private void Resume()
	{
		isTaptoSkip=true;
		totalPauseTime = (int)(Time.time - pauseStartTime);
		startTime += totalPauseTime;
		if(GameObject.Find("TaptoSkipSubscreen(Clone)"))
		{
			if(taptoSkip !=null)
			taptoSkip.SetActive(true);
			GameObject.Find("TaptoSkipSubscreen(Clone)").GetComponent<ScreenManager>().EnableInput();
		}
	}
}
