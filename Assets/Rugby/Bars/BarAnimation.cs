﻿using UnityEngine;
using System.Collections;

public class BarAnimation : MonoBehaviour {

	// Use this for initialization
	
	public static BarAnimation barAnimation;
	void Start () {
		barAnimation = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void ExitAnimation()
	{
//		float target = 0.0f;
//		Vector3 temp = transform.localScale;
//		while(temp.z != target)
//		{
//			temp.z = Mathf.MoveTowards (temp.z,target,8);
//			transform.localScale = temp ;
//			yield return new WaitForEndOfFrame() ;
//			
//		}
//		Destroy(this.gameObject);
		mAnimationObject = new GameObject();
		mAnimationObject.name = "Trial animation";
		
		Vector3 scale = transform.localScale;
		
		CScaleTo tScale = mAnimationObject.AddComponent<CScaleTo>();
		tScale.actionWith(gameObject,new Vector2(scale.x,0),.2f);
		
		CEaseElastic tease1 = mAnimationObject.AddComponent<CEaseElastic>();
		tease1.actionWithAction(tScale,EaseType.EaseInOut);
		CCallFunc call = mAnimationObject.AddComponent<CCallFunc>();
		call.actionWithCallBack(StopActions1);
		
		CSequence seq = mAnimationObject.AddComponent<CSequence>();
		seq.actionWithActions(tease1,call);
		seq.runAction();
		
		
	}
	GameObject mAnimationObject = null;
	public void EntryAnimation()
	{

		StopActions();
		mAnimationObject = new GameObject();
		mAnimationObject.name = "Trial animation";
		
		Vector3 scale = transform.localScale;
		
		CScaleTo tScale = mAnimationObject.AddComponent<CScaleTo>();
		tScale.actionWith(gameObject,new Vector2(scale.x,52),.3f);
		
		CEaseElastic tease1 = mAnimationObject.AddComponent<CEaseElastic>();
		tease1.actionWithAction(tScale,EaseType.EaseInOut);
		
//		CScaleTo tScale2 = mAnimationObject.AddComponent<CScaleTo>();
//		tScale2.actionWith(availabaleText.gameObject,new Vector2(scale.x,scale.y),1.0f);
//		
//		CEaseElastic tease2 = mAnimationObject.AddComponent<CEaseElastic>();
//		tease2.actionWithAction(tScale2,EaseType.EaseOut);
//		
//		CSequence seq1 = mAnimationObject.AddComponent<CSequence>();
//		seq1.actionWithActions(tease1,tease2);
		
		CCallFunc call = mAnimationObject.AddComponent<CCallFunc>();
		call.actionWithCallBack(StopActions);
		
		CSequence seq = mAnimationObject.AddComponent<CSequence>();
		seq.actionWithActions(tease1,call);
		seq.runAction();
	}
	void StopActions()
	{
		if(mAnimationObject != null)
		{
			Destroy(mAnimationObject);
			mAnimationObject = null;
			
		}
	}
	
	void StopActions1()
	{
		if(mAnimationObject != null)
		{
			Destroy(mAnimationObject);
			mAnimationObject = null;
			
		}
		
		Destroy(this.gameObject);
	}
	
}
