using UnityEngine;
using System.Collections;

public class CreateProjectile : MonoBehaviour {

	// Use this for initialization
    public GameObject ProjectileObj;
	public float time;
	InputManager _inputManager;
	void Start () {
	_inputManager=GameObject.Find("GameManager").GetComponent<InputManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if(_inputManager.GetInputStatus()==InputManager.InputStatus.capturingSwing)
	  StartCoroutine(createProjectile());
	}
	IEnumerator createProjectile()
	{
		
		yield return new WaitForSeconds(time);
	  Instantiate(ProjectileObj,transform.position,Quaternion.identity);
	}
}
