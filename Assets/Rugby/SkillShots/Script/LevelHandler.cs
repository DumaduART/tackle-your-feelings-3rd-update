using UnityEngine;
using System.Collections;

public class LevelHandler : MonoBehaviour 
{
	public enum GameoverParameter
	{
		ball=0,
		time
		
	};
	public enum WindType
	{
		right=0,left
	};
	
	public GameoverParameter _gameOverParameter;
	
	public int _noOfBall;
	public int _cutoffAttemptsforBronze;
	public int _cutoffAttemptsforSilver;
	public int _cutoffAttemptsforGold;
	
	public int _totalTime;
	public int _cutoffTimeForBronze;
	public int _cutOffTimeForSilver;
	public int _cutOffTimeForGold;
	
	//public int _shotReqToCmpltLevel;
	public int _objective;
	public int _windEffect;
	public int _shotIntervalforWind;
	public int _timeIntervalforWind;
	
	
	
	
	
	public WindType _windType;
	public Vector3 _ballPos;
	public bool isRandomise;
	
	public LevelAssetHandler[] _arrLevelAssetHandlers;
	public static LevelHandler instance;
	public int misuccessFullShot;
	private float miStartTime;
	private int miTimeLeft;
	private int miTimer;
	private bool mIsTimer=false;
	private float mstoppedtime;
	private int mtotalShot;
	private int mistartShot=0;
	private int mstartTime=0;
	private int tokenscollected;
	private int XpPoint;
	public int fixedWindeffect;
	
	public static int isGoldCleared = 0;
	public static int isSilverCleared = 0;
	public static int isBronzeCleared =0;
	
	public GameManager _gameManager;
	private InputManager _inputManager;
	 
	
	public int _currentAttempts;
	public int _currentTime;
	
	private bool isGold;
	private bool isSilver;
	private bool isBronze;
	private bool isChallengeCleared;
	
	//Edited By Ankit
	public bool _isConsecutiveBased;
	public int _requiredConsecutiveShots = 0;    //Make it 0 if not required.
	public int _finishedConsecutive;
	
	// Use this for initialization
	
	void Awake()
	{
		instance=this;
		miTimeLeft=1;
		
	}
	
	void Start () 
	{
		EventManager.Pause += Pause;
		EventManager.Resume +=Resume;
		_gameManager = GameManager.GetInstance(); //GameObject.Find ("GameManager").GetComponent<GameManager>();
		_inputManager = GameObject.Find ("GameManager").GetComponent<InputManager>();
		isGoldCleared   = _gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "3",isGoldCleared , true);
		isSilverCleared = _gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "2",isSilverCleared,true);
		isBronzeCleared = _gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "1",isBronzeCleared,true);
		miStartTime = Time.time;
	}

	void OnDestroy()
	{
		EventManager.Pause -= Pause;
		EventManager.Resume -= Resume;
	}
	void Update()
	{
		if(_gameOverParameter==GameoverParameter.time)
		{
			if(mIsTimer)
			{
				miTimer = (int)((Time.time)-miStartTime);
				_currentTime = miTimer ;
				miTimeLeft= _totalTime-miTimer;
			}
			if(miTimeLeft<=0)
			{ 
				mIsTimer=false;
			}
			
			
			if(miTimeLeft<=0)
			{
				if(_inputManager.GetRefOfBall()==null)
				{
					_inputManager.enabled=false;
					EventManager.GameOverTrigger();
					Destroy(this.gameObject);
				}
				if((_inputManager.GetRefOfBall()!=null&&_inputManager.GetInputStatus()==InputManager.InputStatus.idle))
				{
					_inputManager.enabled=false;
					EventManager.GameOverTrigger();
					Destroy(this.gameObject);
				}
				
			}
		}
	}
	//--------------------------------------
	public bool CheckLevelCompletionStatus()
	{
		bool bIsAllAssetsCleared = false;
		foreach( LevelAssetHandler levelAssetHandler in _arrLevelAssetHandlers)
		{
			bIsAllAssetsCleared = levelAssetHandler.IsAssetCleared();
			if(!bIsAllAssetsCleared)
				break;
			else
				continue;
		}
		
		return bIsAllAssetsCleared;
	}
	//---------------------------------
	
	public bool ChKLevelCompletion()
	{
		bool isLevelCmpltd=false;
		SkillShots skillshot=SkillShots.instance;
		foreach( LevelAssetHandler levelAssetHandler in _arrLevelAssetHandlers)
		{
			isLevelCmpltd = levelAssetHandler.IsAssetCleared();
			if(!isLevelCmpltd)
			{
				break;
			}
			else
		 	{
				continue;
			}
		}
//		print (isLevelCmpltd);
		return(isLevelCmpltd);	
	}
	//---------------------------
	
	public int RemainingBall()
	{
		return(_noOfBall);
	}
	//---------------------------
	public void UpDateRemaingBall(int x)
	{
		_noOfBall=x;
		mtotalShot +=1;
	}
	//------------------------------
	public void SetSuccessFullShot()
	{
		misuccessFullShot +=1;
	}
	
	//.................................
	public void SetCurrentAttempts()
	{
		_currentAttempts++;
	}
	
	public int GetAttempts()
	{
		return(_currentAttempts);
	}
	
	public int GetObjective()
	{
		return(_objective);
	}
	
	//--------------------------------
	public int GetSuccessFulShot()
	{
		return(misuccessFullShot);
	}
	//--------------------------------
	public int Gettime()
	{
		return(_totalTime);
	}
	//---------------------------------
	public GameoverParameter GetGameOverParameter()
	{
		return(_gameOverParameter);
	}
	//----------------------------------------- getting remaining time---------
	public int GetTimeLeft()
	{
		return(miTimeLeft);
	}
	
	public bool ChkRaondomiseCondition()
	{
		return(isRandomise);
	}
	 
	//--------------------------------------chnging timer condition------------------
	public void ChangeTimerCondition(bool x)
	{
		if(x)
		{
			mIsTimer=false;
			mstoppedtime=(int)Time.time;
		}
		else
		{
			float startTime = Time.time;
			int delay = (int)(startTime-mstoppedtime);
			miStartTime += delay;
			mIsTimer=true;
		}
	}
	
	public void SetIsTimerCondition(bool x)
	{
		mIsTimer = x;
		miStartTime =  Time.time;
	}
	
	public bool getTimerCondition()
	{
		return(mIsTimer);
	}
	
	public bool GetGoldCondition()
	{
		return(isGold);
	}
	
	public bool GetSilverCondition()
	{
		return(isSilver);
	}
	
	public bool GetBronzeCondition()
	{
		return(isBronze);
	}
	
	public bool GetChallengeCleredCondition()
	{
		return(isChallengeCleared);
	}
	
	//-------------------increasing wind effect--------------
	public void IncreaseWindEffect()
	{
		
		switch(_gameOverParameter)
		{
		
			case GameoverParameter.ball:
			{
			    if(mtotalShot-mistartShot==_shotIntervalforWind)
				{
					mistartShot=mtotalShot;
				    fixedWindeffect +=_windEffect;
//					print (fixedWindeffect);
				    if(ArcadeGamepalyGUIItemsManager1.instance)
					{
						
						ArcadeGamepalyGUIItemsManager1.instance.ChangeWindImage(_windType);
						if(_shotIntervalforWind>0)
						ArcadeGamepalyGUIItemsManager1.instance.isActivate = true;
					}
				}
			    else
				{
//					print ("not increased");
				}
				break;
			}
			
			case GameoverParameter.time:
			{
//				print (miTimer- mstartTime);
			    if(miTimer - mstartTime >= _timeIntervalforWind)
				{
					
					mstartTime = miTimer;
				    fixedWindeffect += _windEffect;
				if(SkillShotGUIItemsManager.instance)
				{
					SkillShotGUIItemsManager.instance.ChangeWindImage(_windType);
					if(_timeIntervalforWind>0)
					SkillShotGUIItemsManager.instance.isActivate = true;
				}
				}
				
				break;
			}
		}
	}
	//-------------------getting Windeffect------------
	public int GetWind()
	{
		int wind=0;
		switch(_windType)
		{
			case WindType.left:
			{
				wind = -fixedWindeffect;
				break;
			}
			case WindType.right:
			{
				wind = fixedWindeffect;
				break;
			}
		}
		return(wind);
	}
	public Vector3 GetBallPos()
	{
		return(_ballPos);
	}
	//-------------------chking for Trophy------------
	public void ChkforTrophy()
	{
		
		if(GetSuccessFulShot()>=_objective)
		{
			_gameManager._statsData._killShotData._totalChallengedCleared++;
			isChallengeCleared = true;
			
			switch(_gameOverParameter)
			{
				
				case GameoverParameter.ball:
				{
//					print ("challengeCleared");
					if(_currentAttempts <= _cutoffAttemptsforGold)
					{ 
						//print ("Gold");
						_gameManager._statsData._killShotData._totalGold++;
						isGold = true;
						if(isGoldCleared == 0)
						{	
							
							if(isSilverCleared == 1 && isBronzeCleared == 1)
							{
								tokenscollected = 5;
								XpPoint = 10;
							}
						
							else if(isSilverCleared == 1)
							{
								tokenscollected = 20;
								XpPoint = 40;
							}
						
							else if(isBronzeCleared == 1)
							{
								tokenscollected = 30;
								XpPoint = 60;
							}
						
							else
							{
								tokenscollected = 50;
								XpPoint = 100;
							}
							isGoldCleared = 1;
							
						}
						
						else 
						{
							tokenscollected = 5;
							XpPoint = 10;
						}
					}
					else if(_currentAttempts <= _cutoffAttemptsforSilver)
					{
//						print ("Silver");
						_gameManager._statsData._killShotData._totalSilver++;
						isSilver = true;
						if(isSilverCleared == 0)
						{ 
							
						   if(isGoldCleared ==1)
						   {
								tokenscollected = 3;
						  		 XpPoint = 6;
						   }
							
						   else if(isBronzeCleared == 1)
						   {
								tokenscollected = 30;
						   		XpPoint = 60;
						   }
							
						   else
						   {
								tokenscollected = 30;
						  		 XpPoint = 60;
						   }
						
				           
						   isSilverCleared = 1;
							
						}
						
						else 
						{
							tokenscollected = 3;
							XpPoint = 6;
						}
					}
					else if(_currentAttempts <= _cutoffAttemptsforBronze)
					{
						//print ("Bronze");
						_gameManager._statsData._killShotData._totalBronze++;
						isBronze = true;
						if(isBronzeCleared == 0)
						{
						
						   if(isGoldCleared ==1)
						   {
								tokenscollected = 2;
						  		 XpPoint = 4;
						   }
							
						   else if(isSilverCleared == 1)
						   {
								tokenscollected = 20;
						   		XpPoint = 40;
						   }
							
						   else
						   {
								tokenscollected = 20;
						  		 XpPoint = 40;
						   }
							isBronzeCleared = 1;
							
							
						}
						
						else 
						{
							tokenscollected = 2;
							XpPoint = 4;
						}
					}
				_gameManager.AddCoin(tokenscollected);
					break;
				}
				
				case GameoverParameter.time:
				{
					if(_currentTime <= _cutOffTimeForGold)
					{ 
					//	print ("Gold");
						_gameManager._statsData._killShotData._totalGold++;
						isGold = true;
						if(isGoldCleared == 0)
						{	
							
							if(isSilverCleared == 1 && isBronzeCleared == 1)
							{
								tokenscollected = 5;
								XpPoint = 10;
							}
						
							else if(isSilverCleared == 1)
							{
								tokenscollected = 20;
								XpPoint = 40;
							}
						
							else if(isBronzeCleared == 1)
							{
								tokenscollected = 30;
								XpPoint = 60;
							}
						
							else
							{
								tokenscollected = 50;
								XpPoint = 100;
							}
							isGoldCleared = 1;
							
						}
						
						else 
						{
							tokenscollected = 5;
							XpPoint = 10;
						}
					}
					else if(_currentTime <= _cutOffTimeForSilver)
					{
						//print ("Silver");
						_gameManager._statsData._killShotData._totalSilver++;
						isSilver = true;
						if(isSilverCleared == 0)
						{ 
							
						   if(isGoldCleared ==1)
						   {
								tokenscollected = 3;
						  		 XpPoint = 6;
						   }
							
						   else if(isBronzeCleared == 1)
						   {
								tokenscollected = 30;
						   		XpPoint = 60;
						   }
							
						   else
						   {
								tokenscollected = 30;
						  		 XpPoint = 60;
						   }
						
				           
						   isSilverCleared = 1;
							
						}
						
						else 
						{
							tokenscollected = 3;
							XpPoint = 6;
						}
					}
					else if(_currentTime <= _cutoffTimeForBronze)
					{
						//print ("Bronze");
						_gameManager._statsData._killShotData._totalBronze++;
						isBronze = true;
						if(isBronzeCleared == 0)
						{
						
						   if(isGoldCleared ==1)
						   {
								tokenscollected = 2;
						  		 XpPoint = 4;
						   }
							
						   else if(isSilverCleared == 1)
						   {
								tokenscollected = 20;
						   		XpPoint = 40;
						   }
							
						   else
						   {
								tokenscollected = 20;
						  		 XpPoint = 40;
						   }
							isBronzeCleared = 1;
							
							
						}
						
						else 
						{
							tokenscollected = 2;
							XpPoint = 4;
						}
					}
				_gameManager.AddCoin(tokenscollected);
					break;
				}
			}
			
			_gameManager._statsData.UpdateDataInDevice();
		}
		else
		{
//			print ("Challenge Not Cleared");
		}	
		 ScoreManager.instance.SetXp(XpPoint);
		 _gameManager.LevelUpcondition(ScoreManager.instance.GetLevelUpXp());
		 _gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "3",isGoldCleared , false);
		_gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "2",isSilverCleared ,false);
		_gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "1",isBronzeCleared ,false);
	}
	
	public void OnConsecutiveFailed(){
		
		 	ScoreManager.instance.SetXp(XpPoint);
			 _gameManager.LevelUpcondition(ScoreManager.instance.GetLevelUpXp());
			 _gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "3",0 , false);
			_gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "2",0 ,false);
			_gameManager.UpdateIntDataInDevice(_gameManager.GetKey() + "1",0 ,false);
		
	}
	
	public int GetTokens()
	{
		return(tokenscollected);
	}
	
	public int GetXp()
	{
		return(XpPoint);
	}
	
	private void Pause()
	{
//		if(_hittedStatus==HittedStatus.notHitted)
		ChangeTimerCondition(true);
	}
	private void Resume()
	{
//		if(_hittedStatus==HittedStatus.notHitted)
		ChangeTimerCondition(false);
	}
	
	public void StartTimer()
	{
		miStartTime =  Time.time;
		mIsTimer = true;
	}
	
	public bool GetConsecutiveBased()
	{
		
		return _isConsecutiveBased;
	}
	
	public int GetRequiredConsecutiveShots()
	{
	   return _requiredConsecutiveShots;	
		
	}
	
	public void SetFinishedConsecutive()
	{
	    _finishedConsecutive++;	
	}
	
	public int GetFinishedConsecutive()
	{
		return _finishedConsecutive;
		
	}
	
	public void ResetFinishedConsecutive()
	{
		_finishedConsecutive = 0;
		
	}
		
}
