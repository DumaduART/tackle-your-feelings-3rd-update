using UnityEngine;
using System.Collections;

public class SkillShots : MonoBehaviour {
	public GameObject[] _HittheBar;
	public GameObject[] _PassThroughRings;
	public GameObject[] _NoswingStorm;
	public GameObject[] _DodgeShoot;
	public enum Category
	{
		none=0,
		HitTheBar,
		ThroughRings,
		NoSwingStorm,
		DodgeShoot
	};
	private bool isCategory=true;
	public Category _cateGory;
	private GameManager _gameManager;
	private InputManager _inputManager;
	private CameraMovement _cameraMovement;
	private BallHandler _ballHandler;
	public static SkillShots instance;
	// Use this for initialization
	void Start () {
		instance=this;
	_gameManager=GameObject.Find ("GameManager").GetComponent<GameManager>();
	_inputManager=GameObject.Find("GameManager").GetComponent<InputManager>();
	_cameraMovement = GameObject.Find("Main Camera").GetComponent<CameraMovement>();
	_ballHandler = GameObject.Find ("GameManager").GetComponent<BallHandler>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnGUI()
	{
//		     if(isCategory)
//			{
//		     if(GUI.Button(new Rect(0,0,200,75),"HitTheBar"))
//				{
//				     isCategory=false;
//					 SetCategory(Category.HitTheBar);
//					 _inputManager.camera.enabled=true;
//					 LoadChallenge(0);
//					 _gameManager.startGame=true;
//				}
//				if(GUI.Button(new Rect(0,75,200,75),"ThroughTheRings"))
//				{
//					SetCategory(Category.ThroughRings);
//					 isCategory=false;
//					 LoadChallenge(0);
//					_gameManager.startGame=true;
//				}
//				if(GUI.Button(new Rect(0,150,200,75),"NoSwingStorm"))
//				{
//					SetCategory(Category.NoSwingStorm);
//					 isCategory=false;
//				   _inputManager.ChangeSwingCondition();
//					_gameManager.startGame=true;
//					LoadChallenge(0);
//				}
//				if(GUI.Button(new Rect(0,225,200,75),"DodgeShoot"))
//				{
//					 SetCategory(Category.DodgeShoot);
//					 isCategory=false;
//					 _gameManager.startGame=true;
//					 LoadChallenge(0);
//				}
//			}
	}
//	public void LoadCategory()
//	{
//		 SetCategory(Category.HitTheBar);
//		 
//	}
//	public void SetCategory(Category category)
//	{
//		print (category);
//		switch(category)
//		{
//			case Category.HitTheBar :
//			{
//				_cateGory=Category.HitTheBar;
//				_inputManager.camera.enabled=true;
//		  		 LoadChallenge(0);
//		        _gameManager.startGame=true;
//				break;
//			}
//			case Category.ThroughRings :
//			{
//				_cateGory=Category.ThroughRings;
//				_inputManager.camera.enabled=true;
//				  LoadChallenge(0);
//				 _gameManager.startGame=true;
//				break;
//			}
//			case Category.NoSwingStorm :
//			{
//				_cateGory=Category.NoSwingStorm;
//				_inputManager.camera.enabled=true;
//				  LoadChallenge(0);
//				 _gameManager.startGame=true;
//				break;
//			}
//			case Category.DodgeShoot :
//			{
//				_cateGory=Category.DodgeShoot;
//				_inputManager.camera.enabled=true;
//				  LoadChallenge(0);
//				 _gameManager.startGame=true;
//				break;
//			}
//				
//		}
//	}
//	public void LoadChallenge(int index)
//	{
//		switch(_cateGory)
//		{
//			case Category.HitTheBar:
//			{
//				GameObject temp1 = Instantiate(_HittheBar[index]) as GameObject;
//				EventManager.ActiveObjects.Add(temp1);
//				break;
//			}
//			case Category.ThroughRings:
//			{
//				GameObject temp2 = Instantiate(_PassThroughRings[index])as GameObject;
//				EventManager.ActiveObjects.Add(temp2);
//				break;
//			}
//			case Category.NoSwingStorm:
//			{
//				GameObject temp3 = Instantiate(_NoswingStorm[index]) as GameObject;
//				EventManager.ActiveObjects.Add(temp3);
//				break;
//			}
//			case Category.DodgeShoot:
//			{
//				GameObject temp4 = Instantiate(_DodgeShoot[index])as GameObject;
//				EventManager.ActiveObjects.Add(temp4);
//				break;
//			}
//		}
//	}
//	public Category GetCategory()
//	{
//		return(_cateGory);
//	}
//	
	public void ChangeballPosition()
	{
		GameObject ball= _inputManager.GetRefOfBall();
		Vector3 ballPos = ball.transform.position;
		ballPos.x = -4;
		ballPos.y= -18.57f;
		ballPos.z= 95;
		ball.transform.position=ballPos;
	
	}
	
	public void ChangePosInZone()
	{
		GameObject ball= _inputManager.GetRefOfBall();
		Vector3  ballPos = ball.transform.position;
		ballPos.x=  Random.Range(-10,3);
		ball.transform.position=ballPos;
		_cameraMovement.ChangeCameraDirection(ball);
		_ballHandler.SetballPos(ballPos);
		
	}
}
