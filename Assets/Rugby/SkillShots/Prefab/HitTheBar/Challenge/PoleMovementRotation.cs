using UnityEngine;
using System.Collections;

public class PoleMovementRotation : MonoBehaviour {
	 public enum MovementType
	 {
		right=0,left
	 };
      public bool _isRotation;
	  public bool _ismovement;
	  public float movingBarSpeed;
	  public float rotaionSpeed;
	  public GameObject _pole;
	  private MovementType movementType;
	  public static PoleMovementRotation instance;
	// Use this for initialization
	void Start () {
		_pole=GameObject.Find("Poles");
		instance = this;
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_isRotation)
		{
			//print ("rotation");
			_pole.transform.Rotate(Vector3.forward * Time.deltaTime*rotaionSpeed);
		}
		if(_ismovement)
		{
			switch(movementType)
			{
				case MovementType.right:
				{
					_pole.transform.Translate(Vector3.right * Time.deltaTime*movingBarSpeed);
					break;
				}
				case MovementType.left:
				{
					_pole.transform.Translate(-Vector3.right * Time.deltaTime*movingBarSpeed);
					break;
				}
			}
		
		}
		if(_pole.transform.position.x>=7)
			movementType=MovementType.left;
		if(_pole.transform.position.x<=-15)
			movementType=MovementType.right;
	}
	
	public void ReSet()
	{
		_pole.transform.eulerAngles = new Vector3(270,0,0);
	}
	
	public void Enable()
	{
		this.enabled = true;
	}
	
}
