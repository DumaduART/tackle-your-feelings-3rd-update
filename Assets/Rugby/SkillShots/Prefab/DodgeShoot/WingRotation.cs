using UnityEngine;
using System.Collections;

public class WingRotation : MonoBehaviour {
	
	public GameObject _wing;
	public float rotationSpeed ;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	   
		_wing.transform.RotateAround(Vector3.forward ,rotationSpeed*Time.deltaTime);
	}
}
