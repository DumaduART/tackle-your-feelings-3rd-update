﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum eONLINE_REQ_TYPE
{
	LOGIN = 0,
	LOGOUT,
	INVITE_FRIENDS,
	SHARE_ON_WALL,
	SHARE_SCREEN_SHOT_ON_WALL,
	UPDATE_VALUE_TO_SERVER,
	GET_VALUE_FOR_KEY_FROM_SERVER,
	GET_TOP_RANKING_LIST,	
	GET_TOP_N_RANKING_LIST,
	NO_INTERNET_CONNECTION,
	GET_CURRENT_USER_RANK,
	LOAD_USER_DETAILS,
	GET_TOP_RANKER
}

public class OnlineInterfaceHandler : MonoBehaviour 
{
	static OnlineInterfaceHandler mInstance = null;
	
	public delegate void ReceivedCallBack(string result,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null);
	public  ReceivedCallBack  OnCallBack = null;
	eONLINE_REQ_TYPE meRequestType;
	int Max_No_of_User_List = 0;
	
	public static OnlineInterfaceHandler Instance
	{
		get
		{
			if(mInstance == null)
			{
				GameObject obj = new GameObject();
				obj.name = "OnlineInterfaceHandler";
				mInstance = obj.AddComponent<OnlineInterfaceHandler>();
				DontDestroyOnLoad(obj);
			}
			return mInstance;
		}
	}
	void OnDestroy()
	{
		mInstance = null;
		OnCallBack = null;
	}
	public void SendRequest(eONLINE_REQ_TYPE eRequestType,string str = null,ReceivedCallBack callback = null,string strKeyValue = null)
	{
//		Debug.Log("SendRequest" +eRequestType.ToString());
		OnCallBack = callback;
		meRequestType = eRequestType;
		Max_No_of_User_List = 0;
		switch(eRequestType)
		{
			case eONLINE_REQ_TYPE.LOGIN:
				//Call Login
				OnlineSocialHandler.Instance.CheckInternet();
			break;
			
			case eONLINE_REQ_TYPE.LOGOUT:
				//Call LogOut
			//	if(OnlineSocialHandler.Instance.GetIsLoggedIn())
				//	OnlineSocialHandler.Instance.CallLogOut();
			break;		
			
			case eONLINE_REQ_TYPE.INVITE_FRIENDS:
				//Call Invite Random Friends
				//if(OnlineSocialHandler.Instance.GetIsLoggedIn())
				//	OnlineSocialHandler.Instance.InviteBtnClick(str);
			break;
			
			case eONLINE_REQ_TYPE.SHARE_ON_WALL:
				//Call Share on wall
				//if(OnlineSocialHandler.Instance.GetIsLoggedIn())
					//OnlineSocialHandler.Instance.ShareBtnClick(str);
			break;
			
			case eONLINE_REQ_TYPE.SHARE_SCREEN_SHOT_ON_WALL:
				//Call Take Screen shot and share
				//if(OnlineSocialHandler.Instance.GetIsLoggedIn())
					//OnlineSocialHandler.Instance.ShareScreenShot();
			break;
			
			case eONLINE_REQ_TYPE.UPDATE_VALUE_TO_SERVER:
				//if(OnlineSocialHandler.Instance.GetIsLoggedIn())
//				{
//					OnlineDataHandler.Instance.UpdateValueToServer(int.Parse(str),strKeyValue,OnlineSocialHandler.Instance.GetCurrentUserId());
//				}
//				else
//				{
//					Receiver("====Check ur Internet Connection====",eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION);
//				}
			break;
			case eONLINE_REQ_TYPE.GET_VALUE_FOR_KEY_FROM_SERVER:
				//if(OnlineSocialHandler.Instance.GetIsLoggedIn())
					//OnlineDataHandler.Instance.GetValueFromServer(str,strKeyValue);
			break;			
			case eONLINE_REQ_TYPE.GET_TOP_RANKING_LIST:
				//if(OnlineSocialHandler.Instance.GetIsLoggedIn())
					//OnlineDataHandler.Instance.GetScoresFromList(OnlineSocialHandler.Instance.GetAppFriendsIdsWithCurrentUserId(),strKeyValue);
			break;
			case eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST:
//				if(OnlineSocialHandler.Instance.GetIsLoggedIn())
//				{
//					Max_No_of_User_List = int.Parse(str);
//					Debug.Log("Max_No_of_User_List"+Max_No_of_User_List);
//					OnlineDataHandler.Instance.GetScoresFromList(OnlineSocialHandler.Instance.GetAppFriendsIdsWithCurrentUserId(),strKeyValue);
//				}
//				else
//				{
//					Receiver("====Check ur Internet Connection====",eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION);
//				}
			break;
			case eONLINE_REQ_TYPE.GET_CURRENT_USER_RANK:
//				if(OnlineSocialHandler.Instance.GetIsLoggedIn())
//				{
//					OnlineDataHandler.Instance.GetScoresFromList(OnlineSocialHandler.Instance.GetAppFriendsIdsWithCurrentUserId(),strKeyValue);
//				}
			break;
			case eONLINE_REQ_TYPE.LOAD_USER_DETAILS:
//				if(OnlineSocialHandler.Instance.GetIsLoggedIn())
//					OnlineSocialHandler.Instance.GetUserInfo();
			break;
			case eONLINE_REQ_TYPE.GET_TOP_RANKER:
//				if(OnlineSocialHandler.Instance.GetIsLoggedIn())
//				{
//					OnlineDataHandler.Instance.GetScoresFromList(OnlineSocialHandler.Instance.GetAppFriendsIdsWithCurrentUserId(),strKeyValue);
//				}
				
			break;
		}
	}
	
	public void Receiver(string receivedData , eONLINE_REQ_TYPE eRequestType ,IList<UserData> data = null)
	{
		if(OnCallBack != null)
			OnCallBack(receivedData,eRequestType,data);
		
		if(gameObject != null && eRequestType == eONLINE_REQ_TYPE.LOGOUT)
		{
			Destroy(gameObject);
		}
	}	
	public eONLINE_REQ_TYPE GetRequestType()
	{
		return meRequestType;
	}
	public int GetNoofUserListNeeded()
	{
		return Max_No_of_User_List;
	}
	
}
